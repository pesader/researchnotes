# Overview
 
Open ResearchNotes is an electronic lab notebook and data organizer written in Python. Unlike 
a classical handwritten lab book, which is ordered chronologically, data is 
gathered around samples. Samples are considered all entities subject to your 
research - it can be a sample, an experimental setup, a structure to simulate 
or a run of a sensor. Normally, such research entities undergo various procedures, processing or 
measurements that capture several aspects of its properties. Hence, they should 
be grouped together to facilitate to get an overview and not scattered in different 
places in a lab book (or some lose collection of papers).

Information connected to entries are index by a fulltext search engine and can be accessed over the
search bar.


## Samples, Measurements and other stuff

You start with defining your experiment/sample/simulation (E/S/S), which is characterized 
by an identifier (normally labs and groups come up with an internal system, how to name things 
to have an idea, what is what. If you do not have a system, it is time to develop 
one), a short description (some 50 characters) and a more detailed long description. 
We will automatically remember the creation date, the creator as well as the last 
update of the E/S/S. You can attach files to the entry, which will be stored on 
the server and can later be downloaded, e.g. if you have property sheets coming 
with it. In the E/S/S menu, you can give a location to the sample (for tracking) 
or share it with a colleague (if he has an account in the Open ResearchNotes installation).

The 2nd step is to carry out a procedure, process or measurement (P/P/M) with or within
the E/S/S - e.g. grow it in your MBE, make a measurement in the experimental setup, make a
simulation run with specific parameters for the model or do photo luminescence on your sample.
A P/P/M has a title and a long description. Again, we do some automatic bookkeeping for you. 
Files can be attached – here you want to store raw data coming from the P/P/M. This 
has the advantage that after obtaining the data, it will be stored and archived, and 
you can download it, if something goes wrong during evaluation.
  
Finally, you want to have a report on your P/P/M, e.g. fitting the data, plotting parts
of the measurements against each other a so on. You can make several reports for one P/P/M.  
Again, you are encouraged to archive any interesting file with the report, e.g. images of 
plots or code used for evaluation.

In the case, you want to do an analysis over several samples, e.g. to see a systematic change, you
simply create an E/S/S for this analysis (this would be considered as experiment in some way). The 
advantage is that specific measurement stays with the sample and just the meta analysis is gathered 
in a new place and maybe compared to a calculation or simulation. 

In the end, the structure is quite flexible and allows for different approaches to archive and 
systematize your data.

For passing on the entered information or to archive it, you can build a summery 
which will create a Markdown formatted file with the information related to the E/S/S,
i.e. E/S/S descriptions, P/P/M descriptions and reports. The file will be created in 
the directory of the E/S/S file and can be downloaded from the E/S/S overview.

With time, you will build a logical archive of your research, where you easily 
figure out, what has been done when with or within an E/S/S and what the result was.
Open ResearchNotes also help groups to track stuff of former members. If an account is 
deactivated, e.g. a student graduated and went to another place, all his E/S/S 
and with this all other data associated with will be shared with the supervisor 
of the group (account needed in the Open ResearchNotes installation). No more writing 
students a mail after 3 years, if they remember, where a sample or measurement is and if 
he remembers the details of the experimental implementation :-)

**Remarks**

_File names_ as well as the _E/S/S identifier_ will be sanitized using an internal
function to prevent against directory traversal, SQL injection or HTML code. _Image tagging_ 
and _"href" references_ will not work due to the dynamical nature of the web pages. Uploaded images 
can be depicted using the [Markdown](markdown) language.

_Passwords_ are stored as salted hashes using the internal flask function. They can be 
reset by the admin, but not send or restored.

**Long Descriptions**

To improve readability and information content, the long description 
text areas support formatting by the Markdown language as implemented 
in the [Markdown Module for Python](https://github.com/Python-Markdown/markdown). See the
[Markdown](markdown) commands available for the possibilities.

As standard, the table extension is activated. The areas are than rendered as HTML 
part in the page. The implementation should be safe against HTML injection and directory 
transversal.

## Documentation

Beside the E/S/S, we also have the chance to do longer documentation, e.g. for sample processing protocols,
machine documentation and so on. This part works like a Wiki: You have a starting page and make links to new documents, which 
contain your information. New documents are created by clicking on the link - they are restricted to
access by the group of the creator. They can be linked also from E/S/S, P/M/M or report long descriptions using
the Wiki links. They have to be created from a document side to ensure that they are linked and not become orphan.

## Instruments

The instrumentation tap allows tracking and logging information regarding your instruments and setups.
This can include daily notes about the state or remarks like maintenance procedures etc. The idea is
to collect information that is not related to an ESS but needs to be noted for future of the
specific setup.

**Instrument**

In a first step, you create an instrument that is available for the whole group. This instrument can be shared to 
users outside the group. Beside a description of the instrument, you can attach files like manuals etc. 
Maybe, you want to link a page in the documentation that contains more specific information or common
procedures. The documents are the place to place your own manual.

You can define default templates for the instrument for journal entries and ESS as well as PPM connected to the 
instrument. For each instrument, specific journal entry types can be defined and are linked to this instrument.

An instrument with entries will not be deleted but just deactivated. The supervisor is still able to access to 
all instrument logs and can activate the instrument again. 

Instruments can be shared with other user from other groups allowing them to see connected Journal Entries 
as well as ad information.

**Journal entry**
On clicking an instrument, you will be presented with a page for Journal entries for the instrument. Create a new entry, 
noting the machine status or other specific information of the date. This part will be ordered chronologically. You can
define entry types for quick search. Existing entries can be used to define templates. Further, templates can be created
in the template sub-menu.

If you hit new ESS, the ESS create tap will open using the default template of this instrument. That's practical, if you 
make a daily entry about the instrument and then start using it. Same is possible for a PPM but an ESS has to be chosen
first.


## Roles available in ResearchNotes

1. **Admin (admin)**:
    Able to configure the software; add, edit and remove users, and groups. Should not be used to work.

2. **Student**:
    Account for normal researchers. Able to create, delete samples, P/P/M and reports 
and can share (and revoke shares) of samples.

3. **StudentAdmin**: 
   Can do some administrative works like adding measurement types and locations for the group. Otherwise,
similar to a student account.

4. **Supervisor**: 
    Account for the senior researcher of the group. Can create locations and P/P/M types
as well as will inherit sample data form deactivated user accounts. Can see samples 
of the group, but only access them if shared (so, students can do things without the 
supervisor knowing). Able to manage users of his group (add, modify and deactivate).

5. **ExStudent**:
    As data should not be lost and the original creator not forgotten, we will 
deactivate the accounts, but leave the rest intact. The account can not log in 
anymore, but can be reactivated by the admin.

## Limitations


By design, we assume that one installation is used by a rather small number of groups, e.g.
all from one department/institute that work together closely (so, sample sharing makes sense 
and happens often). For large amounts of user, things might turn a bit impractical.

There is a limited access control implement - mainly for seeing samples, P/P/M entries or 
reports. Sample and measurement templates are restricted to the creator, locations
and P/P/M types to the group. But access control is rather basic assuming you are among 
friends.

All data except passwords are stored as is at the server and the database. Hence, 
the installation should be made on a place under control by the data owners. Using the
server of your competitor might be a bad idea.
