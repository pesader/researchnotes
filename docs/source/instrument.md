The "Instruments" menu let you keep track of instrument or setup changes. It provides the place to store information 
about your machinery used for research that are not directly related to the ESS and also are not general enough to be stored in Documents.

This will be mainly log entries like tracking the daily state of a setup or noting things done to the instrument like maintenance, 
break-downs or readjustments.

When clicked, the menu will present a list of the instruments of the group as well as instruments that are "shared"
to the user, e.g. from another group, and he has the rights to use it. Supervisors will also see a list of deactivated 
instruments (if the group has some).

The table entries allows to see the instrument information, edit the instrument information or deactivate it. The menu bar
has a point to create new instruments. On clicking the instrument name, the instrument journal will open presenting
the journal entries in a chronological order.

### New instrument

The new instrument menu item will open the instrument create form. You have to define an identifier (this will be 
sanitized to be suitable to be used as directory name), the location of the instrument as well as can have a long 
description. You can define templates for instruments, if you have a bunch of similar instruments. 

### Instrument view

Clicking on the i-symbol will open the instrument view for this specific instrumentation entry.

The instrument view displays all the information above as well as a table of all journal entries for this instrument. 
You can attach files to the instrument view, e.g. pdfs of manuals etc. It might be a good idea to make a wiki link [[]]
to documents or the main document that contains information or procedures specific for this instrument. Remember that
wiki document pages can just be created from other wiki documents.

In the instrumentation view, default templates for new journal entries, new ESS as well as for PPM entries called 
from this instrumentation view can be defined. Further, journal entry types can be defined and be associated with this 
instrument.

Finally, journal entries of this instrument are listed and files can be uploaded associated with the instrument.

### Editing instrumentation

Clicking on the pencil symbol allows to edit the instrumentation entry.

### Deactivating an instrument

Clicking on the power symbol will deactivate the instrument. Only supervisors can see the instrument and no new 
journal entries are possible. The supervisor can delete deactivated instruments but this is not recommended as any
associated information gets lost.

## Journal entry overview

Clicking on the instrumentation identifier in the instrumentation view will open the journal entry overview
of this instrument. Journal entries are shown in chronological order with the latest entry shown first. The 
entry can be displayed by clicking on its title.

The menu allows for creating new journal entries (also available at the bottom of the entries table), create a new
ESS (using the default template of the instrument) or display the instrument information.

### New Journal Entry

This menu allows to create from a new journal entry using the default template defined for the instrument.
Beside a title (or an identifier), a journal entry type as well as a long description can be provided.

### Journal Entry

Clicking on the title, the journal entry will be displayed. 

Here files can be associated with the entry and previewed.

The menu allows to edit the current entry or define it as a template.

