"""
All database tramsactions related to the instrumentattion and instrumentation journal go here.

Again, related templates for instruments and JEntres are inside this modul.
"""

from flask_sqlalchemy import SQLAlchemy

from werkzeug.utils import secure_filename

# from flask import g  # This should not be needed here and be always a parameter for the function

from ResearchNotes.database import (
    EntryType,
    TemplateInstrumentationJournalEntry,
    Instrument,
    InstrumentationJournalEntry,
    TemplateInstrument,
)

from .basics import Transaction

# =============================================================================
# entry.py - instrumentation journal entry management
# =============================================================================


def create_instrumentation_journal_entry(database: SQLAlchemy, entry_info: dict) -> int:
    """
    Create an instrumentation journal entry.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    entry_info: Dict
        Information needed to initialize an Instrumentation Journal Entry object

    Returns
    -------
    int
        ID of the newly created jentry.

    """
    new_entry = InstrumentationJournalEntry(
        identifier=secure_filename(entry_info["identifier"]),
        description=entry_info["description"],
        creator_id=entry_info["creator_id"],
        group_id=entry_info["group_id"],
        instrument_id=entry_info["instrument_id"],
    )

    if entry_info["etype"] != 0:
        new_entry.etype_id = entry_info["etype"]

    with Transaction(database) as db_session:
        db_session.add(new_entry)

    return new_entry.id


def update_instrumentation_journal_entry(
    database: SQLAlchemy, entry: InstrumentationJournalEntry, entry_info: dict
) -> None:
    """
    Update an instrumentation journal entry

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    entry_info: Dict
        Updated fields of an Instrumentation Journal Entry object

    Returns
    -------
    None.

    """

    with Transaction(database):
        entry.identifier = secure_filename(entry_info["identifier"])
        entry.description = entry_info["description"]

        # if entry_info["etype"] != 0:
        entry.etype_id = entry_info["etype"]


def create_etype(database: SQLAlchemy, etype_info: dict) -> None:
    """

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    etype_info: Dict
        Instrumentation journal entry type information

    Returns
    -------
    None

    """
    new_etype = EntryType(
        name=etype_info["name"],
        instrument_id=etype_info["instrument_id"],
    )

    with Transaction(database) as db_session:
        db_session.add(new_etype)


def update_etype(database: SQLAlchemy, etype: EntryType, etype_info: dict) -> None:
    """
    Updates existing entry types.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using

    etype: EntryType
        Instrumentation journal entry type to update

    etype_info: Dict
        Instrumentation journal entry type information

    Returns
    -------
    None

    """
    with Transaction(database):
        etype.name = etype_info["name"]


# =============================================================================
# instruments.py - instruments/machines management
# =============================================================================


def create_instrument(database: SQLAlchemy, instrument_info: dict) -> int:
    """
    Creates an instrument based on the data of a web form.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    instrument_info: Dict
        Information needed to initialize an Instrument object

    Returns
    -------
    int
        New instrument ID.

    """
    new_instrument = Instrument(
        identifier=secure_filename(instrument_info["identifier"]),
        description=instrument_info["description"],
        creator_id=instrument_info["creator_id"],
        group_id=instrument_info["group_id"],
        location_id=instrument_info["location_id"],
    )

    with Transaction(database) as db_session:
        db_session.add(new_instrument)

    return new_instrument.id


def update_instrument(database: SQLAlchemy, instrument: Instrument, instrument_info: dict) -> None:
    """
    Update the metadata associated with an instrument.

    Pass a dict with the needed information (identifier, description, and
    location_id).

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    sample: Sample
        Entry of Sample object to be updated.

    sample_data: dict
       Containing the updated fields.

    Returns
    -------
    None.

    """

    with Transaction(database):
        instrument.identifier: str = secure_filename(instrument_info["identifier"])
        instrument.description: str = instrument_info["description"]
        instrument.location_id: int = instrument_info["location_id"]


def deactivate_instrument(database: SQLAlchemy, instrument: Instrument) -> None:
    """
    Deactivate an instrument.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    instrument: Instrument
        Instrument to be deactivated.

    Returns
    -------
    None.

    """
    with Transaction(database):
        instrument.active: bool = False


def activate_instrument(database: SQLAlchemy, instrument: Instrument) -> None:
    """
    Activate an instrument.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    instrument: Instrument
        Instrument to be activated.

    Returns
    -------
    None.

    """
    with Transaction(database):
        instrument.active: bool = True


def update_default_instrument_templates(
    database: SQLAlchemy,
    instrument: Instrument,
    default_ess: None | int,
    default_ppm: None | int,
    default_jentry: None | int,
) -> None:
    """
    Update the default templates of the instrument.

    Update the default templates for ESS, PPM and JEntry. If set to 0 or None, we set things to None in the database.
    This should resolve the problems seen with more advanced databases and foreign key referring.

    Parameters
    ----------
    database
        SQLAlchemy Database object to update.
    instrument
        Table to update (here Instruments)
    default_ess : None|int
        ID of the default EES template or None
    default_ppm : None|int
        ID of the default PPM template or None
    default_jentry : None|int
        ID of the default JEntry template or None

    Returns
    -------

    """

    with Transaction(database):
        if default_ess != 0:
            instrument.default_ess_template_id = default_ess
        else:
            instrument.default_ess_template_id = None

        if default_ppm != 0:
            instrument.default_ppm_template_id = default_ppm
        else:
            instrument.default_ppm_template_id = None
        if default_jentry != 0:
            instrument.default_entry_template_id = default_jentry
        else:
            instrument.default_entry_template_id = None


# =============================================================================
# template.py - template management
# =============================================================================


def create_template_instrument(database: SQLAlchemy, template_info: dict) -> None:
    """
    Create a template instrument.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    template_info: Dict
        Information needed to create a TemplateInstrument object

    Returns
    -------
    None.
    """

    instrument_template = TemplateInstrument(
        tname=template_info["tname"],
        identifier=secure_filename(template_info["identifier"]),
        description=template_info["description"],
        # location_id=template_info["location_id"],
        creator_id=template_info["creator_id"],
        group_id=template_info["group_id"],
    )

    with Transaction(database) as db_session:
        db_session.add(instrument_template)


def update_template_instrument(
    database: SQLAlchemy, template: TemplateInstrument, template_info: dict
) -> None:
    """
    Update a  instrument template based on the data of a web form.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    template : TemplateSamples

    temp_info: Dict

    Returns
    -------
    None.

    """
    with Transaction(database):
        template.tname = template_info["tname"]
        template.identifier = template_info["identifier"]
        template.description = template_info["description"]
        # template.location_id = template_info["location_id"]


def create_template_entry(database: SQLAlchemy, template_info: dict) -> None:
    """
    Create a template instrumentation journal entry.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    template_info: Dict
        Information needed to create a TemplateInstrumentationJournalEntry object

    Returns
    -------
    None.
    """

    instrument_template = TemplateInstrumentationJournalEntry(
        tname=template_info["tname"],
        identifier=secure_filename(template_info["identifier"]),
        description=template_info["description"],
        creator_id=template_info["creator_id"],
        group_id=template_info["group_id"],
        etype_id=template_info.get("etype_id", None),
    )

    with Transaction(database) as db_session:
        db_session.add(instrument_template)


def update_template_entry(
    database: SQLAlchemy, template: TemplateInstrumentationJournalEntry, template_info: dict
) -> None:
    """
    Update a instrumentation journal entry template based on the data of a web form.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    template : TemplateInstrumentationJournalEntry

    template_info: Dict

    Returns
    -------
    None.

    """
    with Transaction(database):
        template.tname = template_info["tname"]
        template.identifier = template_info["identifier"]
        template.description = template_info["description"]
        template.etype_id = template_info["etype_id"]
