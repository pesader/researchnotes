from flask import Flask

from contextvars import ContextVar
from werkzeug.local import LocalProxy


class RNFlask(Flask):
    def __init__(self, name, **kwargs):
        super(RNFlask, self).__init__(name, **kwargs)
        self.elasticsearch = None
        self.meilisearch = None


_no_app_msg = """\
Working outside of application context.
This typically means that you attempted to use functionality that needed
the current application. To solve this, set up an application context
with app.app_context(). See the documentation for more information.\
"""
_cv_app: ContextVar["AppContext"] = ContextVar("flask.app_ctx")
current_app: RNFlask = LocalProxy(  # type: ignore[assignment]
    _cv_app, "app", unbound_message=_no_app_msg
)
