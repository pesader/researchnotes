from flask import url_for
import pytest

# from ResearchNotes import create_app, db
from ResearchNotes.database.basics import User
from werkzeug.utils import secure_filename

# import ResearchNotes.database_transactions as dbt

pytestmark = pytest.mark.app


@pytest.mark.nologin
class TestAppRedirectToLogin:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get("/", follow_redirects=True)

    def test_success(self, response):
        assert response.status_code == 200

    def test_redirect(self, app, response):
        with app.test_request_context():
            assert response.request.path == url_for("auth.login")

    def test_alert(self, html):
        assert "alert-secondary" in html
        assert "You need to login" in html


@pytest.mark.nologin
class TestAppLoginForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(
                url_for("auth.login"),
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Please sign in" in html
        assert "Username" in html
        assert "Password" in html


@pytest.mark.nologin
class TestAppFirstLogin:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, make_info):
        user_info = make_info(User)
        with app.test_request_context():
            return client.post(
                url_for("auth.login"),
                data={
                    "username": user_info["name"],
                    "password": user_info["password"],
                },
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_redirect(self, app, response):
        with app.test_request_context():
            assert response.request.path == url_for("conf.changepasswd")

    def test_static_content(self, html):
        assert "Change password" in html
        assert "Old password" in html
        assert "New password" in html


class TestAppLogout:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(
                url_for("auth.logout"),
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_redirect(self, app, response):
        with app.test_request_context():
            assert response.request.path == url_for("auth.login")


class TestAppLogin:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, user_info):
        with app.test_request_context():
            client.get(
                url_for("auth.logout"),
                follow_redirects=True,
            )
            return client.post(
                url_for("auth.login"),
                data={
                    "username": user_info["name"],
                    "password": user_info["password"],
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class", autouse=True)
    def user_info(self, make_info):
        return make_info(User)

    def test_success(self, response):
        assert response.status_code == 200

    def test_redirect(self, app, response):
        with app.test_request_context():
            assert response.request.path == url_for("main.index")

    def test_static_content(self, html):
        assert "Last updated Experiments/Samples/Simulations" in html
        assert "Other information" in html
        assert "Documents updated" in html
        assert "Instruments" in html
        assert "User Info" in html

    def test_dynamic_content(self, user_info, html):
        assert user_info["name"] in html
        assert user_info["UserName"] in html


@pytest.mark.nologin
class TestAppAboutPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("pages.fpages", path="about"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Open Source" in html
        assert "Getting involved" in html
        assert "Contributing" in html
        assert "GPLv3" in html


@pytest.mark.nologin
class TestAppHelpPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("pages.fpages", path="help"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Overview" in html
        assert "Samples, Measurements and other stuff" in html
        assert "Documentation" in html
        assert "Limitations" in html


class TestAppSharedSamplesPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, share_test_user, default_test_sample):
        with app.test_request_context():
            client.post(
                url_for("samples.sample_view", sid=default_test_sample.id),
                data={
                    "user": share_test_user.id,
                    "usershare": True,
                },
                follow_redirects=True,
            )
            return client.get(url_for("samples.allsharedsamples"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "All Experiments/Samples/Simulations you shared with someone" in html

    def test_dynamic_content(self, html, default_test_sample):
        assert secure_filename(default_test_sample.identifier) in html
        assert default_test_sample.short_dis in html
        assert default_test_sample.origin in html


class TestAppAllSamplesPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("samples.allsamples"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "All your Experiments/Samples/Simulations" in html

    def test_dynamic_content(self, html, default_test_sample):
        assert secure_filename(default_test_sample.identifier) in html
        assert default_test_sample.short_dis in html
        assert default_test_sample.origin in html


class TestAppGroupSamplesPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("samples.groupsamples"))

    def test_success(self, response):
        assert response.status_code == 200


class TestAppAllMeasurementsPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("measurements.allmeasurements"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "All your Procedures/Processes/Measurements" in html

    def test_dynamic_content(self, html, default_test_sample, default_test_measurement):
        assert default_test_measurement.short_dis in html
        assert default_test_sample.identifier in html


class TestAppTemplatesPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("template.index"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "New E/S/S template" in html
        assert "New P/P/M template" in html
        assert "Experiments/Samples/Simulations Templates" in html
        assert "Procedures/Processing/Measurements Templates" in html

    def test_dynamic_content(
        self, html, default_test_template_sample, default_test_template_measurement
    ):
        assert default_test_template_sample.tname in html
        assert default_test_template_measurement.tname in html


class TestAppDocumentsPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(
                url_for("documents.show", label="index"),
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Linked to (children)" in html


class TestAppSettingsPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("conf.index"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Change Password" in html
        assert "P/P/M types" in html
        assert "Locations" in html

    def test_dynamic_content(self, html, default_test_mtype, default_test_location):
        assert default_test_mtype.name in html
        assert default_test_location.name in html


class TestAppUsersPage:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.get(url_for("conf.user_info"))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "User Information" in html
        assert "User Name" in html
        assert "Mail" in html
        assert "Group" in html
        assert "Last seen" in html
        assert "IP" in html

    def test_dynamic_content(self, html, default_test_user):
        assert default_test_user.UserName in html
        assert default_test_user.name in html
