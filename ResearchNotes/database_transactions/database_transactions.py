# -*- coding: utf-8 -*-
"""
Module for database transactions.

Every complex database transaction like updating and creating records goes here.
Queries and "one-liner" transactions should be left out of it.

To avoid conflict with existing functions and make them easy to
identify this module will be imported completely as dbt.
"""
