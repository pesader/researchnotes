from flask import current_app, g, url_for
import pytest
from werkzeug.utils import secure_filename

from ResearchNotes import db
from ResearchNotes.database.basics import Locations
from ResearchNotes.database.ess_ppm_report import Samples
import ResearchNotes.database_transactions as dbt
from ResearchNotes.url_security import safe_url_for

pytestmark = pytest.mark.sample


class TestSampleCreateForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            response = client.get(url_for("samples.create", tid=0))
            return response

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert 'name="identifier"' in html
        assert 'name="origin"' in html
        assert 'name="creator"' in html
        assert 'name="short_dis"' in html
        assert 'name="long_dis"' in html


class TestSampleCreate:
    new_sample_info: dict[str, str] = {
        "identifier": "new sample identifier",
        "origin": "new sample origin",
        "short_dis": "new sample short description",
        "long_dis": "new sample long description",
        "creator": "new sample creator",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client):
        with app.test_request_context():
            return client.post(
                url_for("samples.create", tid=0),
                data={
                    "identifier": self.new_sample_info["identifier"],
                    "origin": self.new_sample_info["origin"],
                    "creator": self.new_sample_info["creator"],
                    "short_dis": self.new_sample_info["short_dis"],
                    "long_dis": self.new_sample_info["long_dis"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def new_sample(self, app):
        with app.test_request_context():
            return db.session.execute(
                db.select(Samples).filter_by(
                    identifier=secure_filename(self.new_sample_info["identifier"])
                )
            ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Long Description" in html

    def test_dynamic_content(self, html, new_sample):
        assert self.new_sample_info["origin"] in html
        assert self.new_sample_info["creator"] in html
        assert self.new_sample_info["short_dis"] in html
        assert self.new_sample_info["long_dis"] in html

    def test_sample_renaming(self, html):
        assert secure_filename(self.new_sample_info["identifier"]) in html

    def test_alert(self, html):
        new_sample_identifier = secure_filename(self.new_sample_info["identifier"])
        assert "alert-success" in html
        assert f"Created E/S/S {new_sample_identifier}" in html

    def test_database(self, new_sample):
        assert new_sample is not None


class TestSampleUpdateForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_sample):
        with app.test_request_context():
            return client.get(url_for("samples.update", sid=default_test_sample.id))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert 'name="identifier"' in html
        assert 'name="origin"' in html
        assert 'name="creator"' in html
        assert 'name="short_dis"' in html
        assert 'name="long_dis"' in html


class TestSampleUpdate:
    updated_sample_info: dict[str, str] = {
        "identifier": "updated_sample_identifier",
        "origin": "updated sample origin",
        "short_dis": "updated sample short description",
        "long_dis": "updated sample long description",
        "creator": "updated sample creator",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, make_info):
        sample_info = make_info(Samples)
        sample: Samples = db.session.execute(
            db.select(Samples).filter_by(identifier=sample_info["identifier"])
        ).scalar()
        with app.test_request_context():
            return client.post(
                url_for("samples.update", sid=sample.id),
                data={
                    "identifier": self.updated_sample_info["identifier"],
                    "origin": self.updated_sample_info["origin"],
                    "short_dis": self.updated_sample_info["short_dis"],
                    "long_dis": self.updated_sample_info["long_dis"],
                    "creator": self.updated_sample_info["creator"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def updated_sample(self):
        return db.session.execute(
            db.select(Samples).filter_by(identifier=self.updated_sample_info["identifier"])
        ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Long Description" in html

    def test_dynamic_content(self, html):
        assert self.updated_sample_info["identifier"] in html
        assert self.updated_sample_info["origin"] in html
        assert self.updated_sample_info["short_dis"] in html
        assert self.updated_sample_info["long_dis"] in html
        assert self.updated_sample_info["creator"] in html

    def test_alert(self, html):
        assert "alert-success" in html
        assert "E/S/S information updated" in html

    def test_database(self, updated_sample):
        assert updated_sample.identifier == self.updated_sample_info["identifier"]
        assert updated_sample.origin == self.updated_sample_info["origin"]
        assert updated_sample.short_dis == self.updated_sample_info["short_dis"]
        assert updated_sample.long_dis == self.updated_sample_info["long_dis"]
        assert updated_sample.creator == self.updated_sample_info["creator"]


class TestSampleForbiddenUpdate:
    new_sample_info: dict[str, str] = {
        "creator_id": "1",
        "group_id": "0",
        "identifier": "new_sample_identifier",
        "origin": "new sample origin",
        "short_dis": "new sample short description",
        "long_dis": "new sample long description",
        "creator": "new sample creator",
    }

    updated_sample_info: dict[str, str] = {
        "identifier": "updated_sample_identifier",
        "origin": "updated sample origin",
        "short_dis": "updated sample short description",
        "long_dis": "updated sample long description",
        "creator": "updated sample creator",
    }

    @pytest.fixture(scope="class", autouse=True)
    def sample(self, app):
        with app.app_context():
            dbt.create_sample(db, self.new_sample_info)

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, sample):
        sample = db.session.execute(
            db.select(Samples).filter_by(identifier=self.new_sample_info["identifier"])
        ).scalar()
        with app.test_request_context():
            return client.post(
                url_for("samples.update", sid=sample.id),
                data={
                    "identifier": self.updated_sample_info["identifier"],
                    "origin": self.updated_sample_info["origin"],
                    "short_dis": self.updated_sample_info["short_dis"],
                    "long_dis": self.updated_sample_info["long_dis"],
                    "creator": self.updated_sample_info["creator"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    def test_forbidden(self, response):
        assert response.status_code == 403


class TestSampleShare:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, share_test_user, default_test_sample):
        with app.test_request_context():
            return client.post(
                url_for("samples.sample_view", sid=default_test_sample.id),
                data={
                    "user": share_test_user.id,
                    "usershare": True,
                },
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_database(self, share_test_user, default_test_sample):
        assert share_test_user in default_test_sample.sharedsamples
        assert default_test_sample.is_shared

    def test_static_content(self, html, default_test_user):
        assert "Current shares" in html
        assert default_test_user.UserName in html

    def test_alert(self, html, share_test_user):
        assert "alert-success" in html
        assert f"Shared sample with {share_test_user.name}"


class TestSamplePreventDelete:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, share_test_user, default_test_sample):
        with app.test_request_context():
            # first, share sample
            client.post(
                url_for("samples.sample_view", sid=default_test_sample.id),
                data={
                    "user": share_test_user.id,
                    "usershare": True,
                },
                follow_redirects=True,
            )
            # then, try to remove shared sample
            url = safe_url_for(
                default_test_sample.id, "samples.delete", current_app.config["SEC_SESSION_KEY"], g.salt
            )
            return client.get(url, follow_redirects=True)

    def test_success(self, response):
        assert response.status_code == 200

    def test_database(self, make_info):
        sample_info = make_info(Samples)
        assert (
            db.session.execute(
                db.select(Samples).filter_by(identifier=sample_info["identifier"])
            ).scalar()
            is not None
        )

    def test_alert(self, html):
        assert "alert-warning" in html
        assert "Sample is shared with others and should not be deleted" in html


class TestSampleUnshare:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, share_test_user, default_test_sample):
        with app.test_request_context():
            # first, share sample
            client.post(
                url_for("samples.sample_view", sid=default_test_sample.id),
                data={
                    "user": share_test_user.id,
                    "usershare": True,
                },
                follow_redirects=True,
            )
            # then, unshare sample
            return client.get(
                url_for("samples.removeshare", ids=[default_test_sample.id, share_test_user.id]),
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_database(self, share_test_user, default_test_sample):
        assert share_test_user not in default_test_sample.sharedsamples

    def test_alert(self, share_test_user, html):
        assert "alert-info" in html
        assert f"Share with {share_test_user.name} was removed" in html


class TestSampleLocation:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_location, default_test_sample):
        with app.test_request_context():
            previous_location_id = default_test_sample.location_id
            return client.post(
                url_for("samples.sample_view", sid=default_test_sample.id),
                data={
                    "loc": default_test_location.id,
                    "location": True,
                },
                follow_redirects=True,
            )

    def test_success(self, response):
        assert response.status_code == 200

    def test_database(self, default_test_location, default_test_sample):
        assert default_test_sample.location_id == default_test_location.id

    def test_dynamic_content(self, default_test_location, html):
        assert f"stored at <em>{default_test_location.name}.</em>" in html

    def test_alert(self, make_info, html):
        location_info = make_info(Locations)
        assert f"Changed sample location to {location_info['name']}" in html
        assert "alert-success" in html


class TestSampleDelete:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_sample):
        with app.test_request_context():
            # NOTE: g.salt is only available on certain views
            client.post(
                url_for("samples.sample_view", sid=default_test_sample.id),
                follow_redirects=True,
            )
            url = safe_url_for(
                default_test_sample.id, "samples.delete", current_app.config["SEC_SESSION_KEY"], g.salt
            )
            return client.get(url, follow_redirects=True)

    def test_success(self, response):
        assert response.status_code == 200

    def test_database(self, default_test_sample):
        assert db.session.get(Samples, default_test_sample.id) is None

    def test_dynamic_content(self, app, default_test_sample, html):
        with app.test_request_context():
            assert (
                url_for(
                    "samples.sample_view",
                    sid=default_test_sample.id,
                )
                not in html
            )

    def test_alert(self, default_test_sample, html):
        assert f"{default_test_sample.identifier} was deleted" in html
        assert "alert-info" in html
