# -*- coding: utf-8 -*-
"""
App configuration module.

Define the configuration of the app for different operating systems
and usage scenarios.
"""
import os
import secrets


class BaseConfig:
    """Base configuration class."""

    DEBUG = False
    TESTING = False
    SECRET_KEY = "dev" or os.environ.get("SECRET_KEY")
    SEC_SESSION_KEY = "dev" or os.environ.get("SEC_SESSION_KEY")
    CONFIG_FILE = "config.py"
    DATABASE_MIGRATION = "migrations"

    ALLOWED_EXTENSIONS = ({".txt", ".pdf", ".png", ".jpg", ".jpeg", ".gif"},)
    FORBIDDEN_EXTENSIONS = {
        ".bat",
        ".exe",
        ".cmd",
        ".sh",
        ".php",
        ".pl",
        ".cgi",
        ".386",
        ".dll",
        ".com",
        ".torrent",
        ".js",
        ".app",
        ".jar",
        ".pif",
        ".vb",
        ".vbscript",
        ".asp",
        ".cer",
        ".csr",
        ".jsp",
        ".drv",
        ".sys",
        ".ade",
        ".adp",
        ".bas",
        ".chm",
        ".cpl",
        ".crt",
        ".csh",
        ".fxp",
        ".hlp",
        ".hta",
        ".inf",
        ".ins",
        ".isp",
        ".jse",
        ".htaccess",
        ".htpasswd",
        ".ksh",
        ".lnk",
        ".mdb",
        ".mde",
        ".mdt",
        ".mdw",
        ".msc",
        ".msi",
        ".msp",
        ".mst",
        ".ops",
        ".pcd",
        ".prg",
        ".reg",
        ".scr",
        ".sct",
        ".shb",
        ".shs",
        ".url",
        ".vbe",
        ".vbs",
        ".wsc",
        ".wsf",
        ".wsh",
    }

    MAX_CONTENT_LENGTH = 1024 * 1024 * 100

    # SQLALCHEMY_DATABASE_URI=r"sqlite:///"+os.path.join(UPLOAD_PATH , 'ResearchNotes.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    FLATPAGES_ENCODING = "cp1252"
    FLATPAGES_EXTENSION = ".md"

    LOG_FILE = True
    LOG_FILENAME = "researchnotes.log"

    LOG_SYSLOG = False
    LOG_SYSLOG_ADDR = ("localhost", 514)
    LOG_SYSLOG_FAC = "local3"

    ELASTICSEARCH_URL = os.environ.get("ELASTICSEARCH_URL")
    MEILISEARCH_URL = os.environ.get("MEILISEARCH_URL")
    MEILISEARCH_MKEY = None


class TestConfig(BaseConfig):
    """Testing configuration used in the moment of create_app."""

    # print("Development configuration")
    DEBUG = True
    TESTING = True
    SECRET_KEY = os.environ.get("SECRET_KEY", default="dev")
    SEC_SESSION_KEY = os.environ.get("SEC_SESSION_KEY", default="dev")
    # PERMANENT_SESSION_LIFETIME = 24 * 3600
    # SESSION_LIFETIME = 24 * 3600

    UPLOAD_PATH = ""
    SQLALCHEMY_DATABASE_URI = r"sqlite://"
    # SQLALCHEMY_DATABASE_URI = r"sqlite:///" + os.path.join(UPLOAD_PATH, "test.sqlite")

    LOG_FILE = False


class TestSearchConfig(TestConfig):
    """Search-specific testing configuration used in the moment of create_app."""

    ELASTICSEARCH_URL = os.environ.get("ELASTICSEARCH_URL", default="http://localhost:9200")


class DevConfig(BaseConfig):
    """Development configuration used in the moment of create_app."""

    # print("Development configuration")
    DEBUG = True
    SECRET_KEY = os.environ.get(
        "SECRET_KEY", default="dev"
    )  #: :meta hide-value: Uses os.environ.get("SECRET_KEY", default="dev")
    SEC_SESSION_KEY = os.environ.get(
        "SEC_SESSION_KEY", default="dev"
    )  #: :meta hide-value: Uses os.environ.get("SECRET_KEY", default="dev")
    # PERMANENT_SESSION_LIFETIME = 24 * 3600
    # SESSION_LIFETIME = 24 * 3600

    UPLOAD_PATH = r"D:\ResearchNotes"
    SQLALCHEMY_DATABASE_URI = r"sqlite:///" + os.path.join(
        UPLOAD_PATH, "ResearchNotes.sqlite"
    )  #: :meta hide-value:

    LOG_FILE = False
    LOG_PATH = os.path.join(UPLOAD_PATH, "logs")
    LOG_FILENAME = "researchnotes.log"

    ELASTICSEARCH_URL = os.environ.get("ELASTICSEARCH_URL")
    MEILISEARCH_URL = os.environ.get("MEILISEARCH_URL")


class ProConfig(BaseConfig):
    """Production configuration used in the moment of create_app."""

    # print("Production configuration")
    SECRET_KEY = os.environ.get(
        "SECRET_KEY", default=secrets.token_hex(32)
    )  #: :meta hide-value: os.environ.get("SEC_SESSION_KEY", default=secrets.token_hex(32))
    SEC_SESSION_KEY = os.environ.get(
        "SEC_SESSION_KEY", default=secrets.token_hex(32)
    )  #: :meta hide-value: Created using secrets.token_hex(32)
    # PERMANENT_SESSION_LIFETIME = 3600
    # SESSION_LIFETIME = 3600

    UPLOAD_PATH = ""
    CONFIG_FILE = "ResearchNotes_conf.py"

    LOG_PATH = "researchnotes_logs/"

    DATABASE_MIGRATION = UPLOAD_PATH
    SQLALCHEMY_DATABASE_URI = r"sqlite:///" + os.path.join(
        UPLOAD_PATH, "ResearchNotes.sqlite"
    )  #: :meta hide-value:
