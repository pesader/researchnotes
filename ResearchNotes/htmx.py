"""
Modul to have all htmx function that are not specific to a view or page and can be recycled for
different things.
"""

import os

from flask import Blueprint, render_template, current_app, request, abort, g

from ResearchNotes.files import uploaddir_path, make_image_url_list, make_file_list
from ResearchNotes.auth import login_required

bp = Blueprint("htmx", __name__, url_prefix="/htmx")


@bp.route("/<list:info>/htmx_image_files", methods=["GET"])
@login_required
def htmx_image_files(info: list) -> str:
    """
    Htmx function that renders and returns the modal dialog to show associated files.

    Parameters
    ----------
    info : list
        Info dict for the directory to show.

    Returns
    -------
    str
        Rendered content of the modal dialog.
    """
    current_app.logger.debug(" htmx : Create file list")

    pre = uploaddir_path(info, only_sub_dir=True)
    path = os.path.join(current_app.config["UPLOAD_PATH"], pre)

    current_app.logger.debug(f" htmx : Looking in {path}")

    urls = []
    if os.path.exists(path):
        urls = make_image_url_list(path, pre)
        current_app.logger.debug(f" htmx : Render {len(urls)} files")

    return render_template("htmx/file_show_modal.html", urls=urls)


@bp.route("/htmx_markdown_help", methods=["GET"])
@login_required
def htmx_markdown_help() -> str:
    """
    Renders the Markdown help modal.

    We read the markdown help from the markdown_help.txt file. This can/should contain a Markdown text
    that we want to show. We use our markdown|safe filter to display.

    Returns
    -------
    str
        Rendered content of the modal (markdown).

    """

    current_app.logger.debug(" htmx : Modal help")
    with current_app.open_resource("markdown_help.txt", mode="r") as list_file:
        help_str = list_file.read()

    return render_template("htmx/markdown_help_modal.html", help_string=help_str)


@bp.route("/<list:info>/htmx_redo_files", methods=["GET"])
@login_required
def htmx_redo_files(info: list) -> str:
    """
    Recreate file list in associated files area.

    Parameters
    ----------
    info : dict
        Information of the directory

    Returns
    -------
    str
        Rendered file list to be put into the html document body.
    """
    current_app.logger.debug(" htmx : Refresh file list")
    files = make_file_list(uploaddir_path(info))

    return render_template("htmx/file_list.html", files=files, info=info)


@bp.route("/<list:info>/htmx_delete_file", methods=["DELETE"])
@login_required
def htmx_delete_file(info):
    """
    Use htmx to delete an uploaded file.

    Parameters
    ----------
    info: dict
        Information of the path for file

    Returns
    -------
    str
        Rendered new file list to be replaced in body.

    """
    current_app.logger.debug(" htmx : Delete file")
    file = request.args.get("file")
    path = uploaddir_path(info)

    if file is None:
        abort(404, description=f"htmx : delete_file : File None call to function for user {g.user}")

    try:
        os.remove(os.path.join(path, file))
        current_app.logger.info(f" htmx : Deleted {os.path.join(path, file)}")
        # flash(f"Deleted {file}", "alert-success")
    except OSError as error:
        abort(
            500,
            description=f"htmx : delete_file : Failed to delete file {file} in {path}."
            + f" Exception: {str(error)}",
        )
    files = make_file_list(uploaddir_path(info))

    return render_template("htmx/file_list.html", files=files, info=info)
