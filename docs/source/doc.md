Part of our research knowledge includes information that does not fit into the ESS, PPM and report scheme. These are more general instructions or protocols that should 
be known or followed in the group. Therefore, a dedicated documentation area was created.

This area works more like a wiki. An index document is created for each group and then sub-pages are defined by creating links using the form \[\[ \]\]. These wiki links 
will open on clicking a form to create a new document. If the document label already exists for this group, the page will be opened (and marked as a child page of the current page).

### Document creation form

On creation or editing a form will open asking for a label (what is in the \[\[\]\]), the title of the document and its long description. Guess what, Markdown all over the place for the long description part.

As documents are created over links, no specific create document icon exists. In the document view or page, you find the following menu items:

### Edit

Loads the current page for editing into the document creation form. The initial label of the index page of the group is protected but all other labels can be changed later.

### Download

The current document is downloaded as a Markdown file. It can be opened by any text editor or converted with a Markdown converter to PDF, LaTeX or similar.

### Delete

Delete the current page. Just possible, if it has no child pages connected to it.

### Overview

Shows a tree of the pages in a hierarchical structure that you know, where is which information.

Finally, the document page shows on top the parent page(s) of the document and on the bottom the linked child pages (if they have been visited once from this document). 
Child links will never be forgotten, if they are once used to avoid making orphans. If you want to explicitly unlink a document from the current document use the eraser icon.



