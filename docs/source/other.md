# Additional menu points

## Templates 

The template menu offers a view with a table of the existing ESS and PMM templates. Using the action bottom, you can edit or delta templates.

Furthermore, the menu offers an icon for creation of ESS and PPM templates. The used form is very similar to the ESS or PPM form, respectively. Only a template name has been 
added and some information assumed to change every time is not in the template.


------------------

## Markdown in text areas

We make heavy use of the Markdown format language as already [defined](https://daringfireball.net/projects/markdown/). Indeed, we use an existing Python implementation of 
the language - the [Python Markdown](https://python-markdown.github.io/) module. As implementations differ, there are different flowers 
around e.g. from GitHub or StackOverflow. Hence, sometimes the result from our preview of the markdown toolbar and final rendering might differ a bit.

We have a [documentation](markdown) describing the main commands.

------------------

## Settings and User info

The Setting menu is not very interesting. Basically, you can change your password here. You can reach this also over the user
info icon left to the username after log-in and right to the sign-out link. 

The user information will provide some basic information of you (Name, login handle, no. of samples, etc.) as well as the 
version string of Open ResearchNotes and the OS it is running on.

For supervisors, there are some additional entries in the setting menu:

### New PPM type (supervisor)

Allows the definition of a new PPM type for the group. Existing types are provided in a table on the setting view. The type Other is predefined and cannot be deleted.

### New location (supervisor)

Add a new location, where to find the physical entry of your ESS (e.g. room, sample storage place, computer etc.). Existing places for the group are listed in a table on
the setting view. The place “not defined” is predefined and cannot be deleted.

### New user (supervisor and admin)

__Supervisor__ can add new students to their group as well as deactivate students. For user registration a unique login name has to be provided as well as an unique e-mail. 
Further, we ask for full name and a password. The password should be changed by the user (student) at next login. Supervisor can also reset the password for their
students (and themselves). **The password reset link is sendable and accessable without login. **

__Administrators__ can  also register new supervisors and new administrators.

### Groups (admins)

Administrators can register groups, change the name of the group as well as register new users for the group (see menu above).

For the administrator several other commands are available:

### App info (admins)

This will display the configuration variables of the web application including the secret keys for session encoding and safe url creation.



