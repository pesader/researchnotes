# -*- coding: utf-8 -*-
"""
All helpful utils go here. Mainly carrying for the markdown stuff.

- Define um url converter for lists
- Define Markdown filter and handling of flatpages
- Define markdown template filter
"""

from typing import Any, List
from secrets import choice

import bleach

from markdown import markdown
from markdown.extensions.wikilinks import WikiLinkExtension

from werkzeug.routing import BaseConverter

from flask import Blueprint, render_template, url_for, g, current_app  # , abort
from flask.json import load  # , JSONEncoder

from markupsafe import escape

from . import pages


class ListConverter(BaseConverter):
    """
    Class for passing a list when calling a page.

    Used to pass a lot of the parameters.
    """

    def to_python(self, value: str) -> Any:
        """
        Convert the list into url parameters passed to the view.

        Parameters
        ----------
        value : str
            URL save string of list.

        Returns
        -------
        list
            Decoded list from input string.
        """
        return value.split("+")

    def to_url(self, values: List) -> str:
        """
         Convert or list from url back to python list.

        Parameters
        ----------
        values : List
            Obj to be encoded.

        Returns
        -------
        str
            Encoded list in URL save str form.
        """
        base_to_url = super().to_url
        return "+".join(base_to_url(value) for value in values)


def markdown_render(markdown_text: str) -> str:
    """
    Markdown-renderer for Flatpages plugin.

    Defines a markdown render function to be used to show part
    of the text. Used by samples, measurements and reports html pages


    Parameters
    ----------
    markdown_text : String
        Input text in Markdown format.

    Returns
    -------
    str
        Renders the Markdown text into HTML with table extension enabled.

    """
    return markdown(
        escape(markdown_text),
        extensions=[
            "tables",
            "fenced_code",
        ],
    )


bp = Blueprint("pages", __name__, url_prefix="/pages")


@bp.route("/<path:path>")
def fpages(path: str) -> str:
    """
    Call the static pages over flask-flatpages.

    Parameters
    ----------
    path : str
        path to flat page.

    Returns
    -------
    str
        Renders the static page in the page directory in the flatpages template.
    """
    page = pages.get_or_404(str(path))

    return render_template("flatpages.html", page=page)


def doc_url(label: str, base, end) -> str:
    """
    Define our own url handler for the wiki pages.

    The function has to have this signature with in our case two unused keywords.

    Parameters
    ----------
    label : str
        Document label.
    base : str
        Not use but required by func signature.
    end : str
        Not use but required by func signature.

    Returns
    -------
    str
        Url for document view of a Wiki link depending on from where it comes.

    """
    try:
        return url_for("documents.show", label=label, p_id=g.pid, _external=False)
    except AttributeError:
        return url_for("documents.show", label=label, _external=False)


@bp.app_template_filter("markdown")
def markdown_filter(markdown_text: str) -> str:
    """
    Define our markdown filter to show things.

    These needs our defined url_doc handler

    Filter is used in all moduls to show the markdown (ESS, PPM, reports and documents)

    Parameters
    ----------
    markdown_text : str
        Markdown text - normally coming from the database and containing the long description of the item.

    Returns
    -------
    str
        HTML rendered text from the markdown input. Cleaned by bleach as this is user provided input.

    """
    html_str = markdown(
        escape(markdown_text),
        extensions=["tables", "fenced_code", "attr_list", WikiLinkExtension(build_url=doc_url)],
        output_format="html",
    )
    return bleach.clean(
        html_str,
        tags=[
            "a",
            "abbr",
            "acronym",
            "b",
            "blockquote",
            "code",
            "em",
            "i",
            "li",
            "ol",
            "strong",
            "ul",
            "p",
            "img",
            "table",
            "thead",
            "tr",
            "th",
            "td",
            "tbody",
            "hr",
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "pre",
        ],
        attributes={
            "img": {"width", "height", "src"},
            "a": ["href", "alt"],
            "abbr": ["title"],
            "acronym": ["title"],
        },
    )


def suggest_passwd() -> str:
    """Suggested a strong password randomly picking 4 words from a word list with more than 7000 entries."""
    with current_app.open_resource("wordlist.json", mode="r") as list_file:
        word_list = load(list_file)

    suggest = "_".join(choice(word_list) for _ in range(4))

    # print(suggest)

    return suggest
