from flask import g

from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    TextAreaField,
    validators,
)

# from dataclasses import dataclass

from ResearchNotes.database import db, Documents


# =============================================================================
# Forms related to the documentation
# =============================================================================


class DocCreateForm(FlaskForm):
    """Create or update a Documentation page."""

    label = StringField(
        "Label",
        [validators.DataRequired(), validators.Length(min=1, max=70)],
    )  #: :meta hide-value:
    title = StringField(
        "Title", [validators.DataRequired(), validators.Length(min=1, max=70)]
    )  #: :meta hide-value:
    body = TextAreaField("Long Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit Document")  #: :meta hide-value:

    def __init__(self, original_label, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.original_label = original_label
        # self.label.default = original_label

    def validate_label(self, label):
        """Validate that label is unique and not use the name index."""
        # print(label.data, self.original_label)
        if label.data != self.original_label:
            if self.original_label.endswith("_index"):
                raise validators.ValidationError("Document label 'index' cannot be changed")
            doc = db.session.execute(
                db.select(Documents).filter_by(label=label.data, group_id=g.user.group_id)
            ).scalar()
            if doc is not None:
                raise validators.ValidationError(
                    "Document label already used. Please use a different document label."
                )
