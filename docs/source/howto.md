# How to use

## Experiment/Sample/Simulation entry

```{include} ess.md
```
----------------

## Processing/Procedure/Measurement entry

```{include} ppm.md
```
----------------

## Report entry

```{include} report.md
```

-----------------

## Documents

```{include} doc.md 
```

## Instruments

```{include} instrument.md 
```