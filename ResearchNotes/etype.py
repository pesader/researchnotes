"""
Module dealing with the entry types - might be merged to entry.py
"""


import typing

from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    url_for,
    current_app,
    abort,
)

from werkzeug.wrappers.response import Response

import ResearchNotes.database_transactions as dbt

from ResearchNotes.auth import role_required
from ResearchNotes.database import EntryType, db

from ResearchNotes.form import ConfigCreateMType
from ResearchNotes.url_security import token_decode

# from ResearchNotes.instruments import check_permission


bp = Blueprint("etype", __name__, url_prefix="/etype")


@bp.route("/<int:iid>/create", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def create(iid: int) -> typing.Union[str, Response]:
    """
    Register a new instrumentation journal entry type.

    Parameters
    ----------
    iid: int
        ID of the instrument that the journal entry type is associated with

    Returns
    -------
    str|Response

    """
    if not (
        iid in [i.id for i in g.user.group_member.owned_instruments]
        or iid in [i.id for i in g.user.shared_instruments]
    ):
        abort(
            403,
            description=f"instrument.get_instrument: Authorization failure. {g.user} "
            + f"tried to load instrument {iid}",
        )
    else:
        existing_etypes = db.session.execute(
            db.select(EntryType).order_by("name").filter_by(instrument_id=iid)
        ).scalars()

        form = ConfigCreateMType()

        if form.validate_on_submit():
            name = form.mtype.data

            if (
                db.session.execute(db.select(EntryType).filter_by(name=name, instrument_id=iid)).scalar()
                is not None
            ):
                flash(f"Entry type {name} is already registered.", "alert-warning")
            else:
                dbt.create_etype(
                    db,
                    {
                        "name": name,
                        "instrument_id": iid,
                    },
                )
                flash("Entry type created.", "alert-success")
                return redirect(url_for("instruments.instrument_view", iid=iid))
        return render_template(
            "setup/registertype.html", form=form, existing=existing_etypes, what="Entry types"
        )


@bp.route("/<int:etid>/update", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def update(etid: int) -> typing.Union[str, Response]:
    """
    Update an existing instrumentation journal entry type.

    Parameters
    ----------
    etid : int
        EntryType ID to update.

    Returns
    -------
    str|Response

    """
    etype = db.get_or_404(EntryType, etid)

    if not (
        etype.instrument_id in [i.id for i in g.user.group_member.owned_instruments]
        or etype.instrument_id in [i.id for i in g.user.shared_instruments]
    ):
        abort(
            403,
            description=f"instrument.get_instrument: Authorization failure. {g.user} "
            + f"tried to load instrument {etype.instrument_id}",
        )
    else:
        existing_etypes = db.session.execute(
            db.select(EntryType).order_by("name").filter_by(instrument_id=etype.instrument_id)
        ).scalars()

        form = ConfigCreateMType(data={"mtype": etype.name})

        if form.validate_on_submit():
            name = form.mtype.data

            if (
                db.session.execute(
                    db.select(EntryType).filter_by(name=name, instrument_id=etype.instrument_id)
                ).scalar()
                is not None
            ):
                flash(f"Entry type {name} is already registered.", "alert-warning")
            else:
                dbt.update_etype(
                    db,
                    etype,
                    {
                        "name": name,
                    },
                )
                flash(f'Entry type {name} updated', "alert-info")
                return redirect(url_for("instruments.instrument_view", iid=etype.instrument_id))
        return render_template(
            "setup/registertype.html", form=form, existing=existing_etypes, what="Entry types"
        )


@bp.route("/delete/<string:token>")
@role_required(["Supervisor", "StudentAdmin"])
def delete(token: str) -> Response:
    """
    Delete a location where samples are stored.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of entry type to delete


    Returns
    -------
    Flask.Response
        Redirect to instrument view

    """
    etid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    etype = db.session.get(EntryType, etid)
    for entry in etype.entries:
        flash(f'{entry.identifier} assigned to "None" type', "alert-info")
        with dbt.Transaction(db):
            entry.etype_id = None

    with dbt.Transaction(db) as db_session:
        db_session.delete(etype)
    flash(f'{etype.name} successfully deleted', "alert-success")

    return redirect(url_for("instruments.instrument_view", iid=etype.instrument_id))
