# -*- coding: utf-8 -*-
"""
User and app configuration module.

Everything for setup, configuration and user handling is here. Big parts are only for
the admin.

We do creation of groups, places etc. here as well (for supervisors and admins).
"""
import platform
import sys
import typing

from flask import (
    Blueprint,
    redirect,
    url_for,
    flash,
    render_template,
    current_app,
    g,
)

from werkzeug.security import check_password_hash, generate_password_hash
from werkzeug.wrappers.response import Response

from ResearchNotes.database import db, User, Groups, MeasurementType, Role, Locations

import ResearchNotes.database_transactions as dbt
from ResearchNotes.auth import role_required, login_required

from ResearchNotes.form import (
    UserCreateForm,
    UserUpdateForm,
    ConfigChangePasswdForm,
    ConfigCreateMType,
    ResetPassword,
)

from ResearchNotes.url_security import token_decode
from ResearchNotes.util import suggest_passwd

bp = Blueprint(
    "conf",
    __name__,
    url_prefix="/config",
)


@bp.route("/reset_password/<string:token>", methods=("GET", "POST"))
def resetpassword(token: str) -> typing.Union[str, Response]:
    """
    Reset the password of a user.

    Parameters
    ----------
    uid : int
        User ID.

    Returns
    -------
    Website URL
        Either the form or redirect to the config main page.

    """
    uid = token_decode(token, current_app.config["SEC_SESSION_KEY"], "reset_passwd")

    user = db.get_or_404(User, uid)
    s_passwd = suggest_passwd()
    form = ResetPassword()

    if form.validate_on_submit():
        flash(f"Changed password for {user.name}", "alert-success")
        with dbt.Transaction(db):
            user.password = generate_password_hash(form.password.data)
        # db.session.commit()

        if not user.active:
            dbt.activate_user(db, user)
        return redirect(url_for("conf.user_mangement"))
    return render_template("setup/resetpass.html", form=form, user=user, s_passwd=s_passwd)


@bp.route("/index")
@login_required
def index() -> str:
    """Overview of configuration - mainly for admins."""
    mtypes = db.session.execute(
        db.select(MeasurementType).order_by("name").filter_by(group_id=g.user.group_id)
    ).scalars()
    locations = db.session.execute(
        db.select(Locations).order_by("name").filter_by(group_id=g.user.group_id)
    ).scalars()

    return render_template("setup/index.html", mtyps=mtypes, locations=locations)


@bp.route("/user_info")
@login_required
def user_info() -> str:
    """
    Return view to show some user info and statistic.

    Returns
    -------
    View
        Returns flask view to user info.
    """
    system = platform.uname()
    return render_template(
        "setup/userinfo.html", system=str(system.node) + " (" + str(system.system) + ")"
    )


@bp.route("/change_passwd", methods=("GET", "POST"))
@login_required
def changepasswd() -> typing.Union[str, Response]:
    """Change your password."""
    form = ConfigChangePasswdForm()

    s_passwd = suggest_passwd()

    if form.validate_on_submit():
        old = form.old.data
        password = form.password.data

        if check_password_hash(g.user.password, old):
            flash(f"Changed password for {g.user.name}", "alert-info")
            with dbt.Transaction(db):
                g.user.password = generate_password_hash(password)
            return redirect(url_for("conf.index"))
        #
        flash(f"Wrong old password for {g.user.name}", "alert-warning")
        return redirect(url_for("conf.index"))
    return render_template("setup/changepass.html", form=form, s_passwd=s_passwd)


#######################################################################################################
# Admin functions
#######################################################################################################


@bp.route("/app_info")
@role_required(["admin"])
def app_info() -> str:
    """
    Return view to show some user info and statistic.

    Returns
    -------
    View
        Returns flask view to user info.
    """
    system = platform.uname()
    py_version = sys.version
    return render_template(
        "setup/app_info.html",
        system=str(system.node) + " (" + str(system.system) + ")",
        pyversion=py_version,
    )


@bp.route("/user_mangement")
@role_required(["admin", "Supervisor"])
def user_mangement() -> str:
    """User management view."""
    if g.user.role_member.name == "admin":
        users = db.session.execute(db.select(User).order_by("group_id", "UserName")).scalars()
    else:
        users = db.session.execute(
            db.select(User).order_by("group_id", "UserName").filter_by(group_id=g.user.group_id)
        ).scalars()
    return render_template("setup/users.html", users=users)


@bp.route("/group_mangement")
@role_required(["admin"])
def group_mangement() -> str:
    """Group management view."""
    groups = db.session.execute(db.select(Groups).order_by("id")).scalars()
    return render_template("setup/groups.html", groups=groups)


@bp.route("/register", methods=("GET", "POST"))
@role_required(["admin", "Supervisor"])
def register() -> typing.Union[str, Response]:
    """
    Register a new user.

    Currently, only admin or supervisors can register or edit users.

    Returns
    -------
    View
        Returns view to 'conf.index' or for form.
    """
    form = UserCreateForm(
        "",
        "",
    )
    if g.user.role_member.name == "admin":
        form.role_id.choices = [
            (cg.id, cg.name) for cg in db.session.execute(db.select(Role).order_by("name")).scalars()
        ]
        form.group_id.choices = [
            (cg.id, cg.name) for cg in db.session.execute(db.select(Groups).order_by("name")).scalars()
        ]
    else:
        form.role_id.choices = [
            (cg.id, cg.name)
            for cg in db.session.execute(db.select(Role).order_by("name")).scalars()
            if cg.name not in ["admin", "Supervisor"]
        ]

        form.group_id.choices = [
            (cg.id, cg.name)
            for cg in db.session.execute(
                db.select(Groups).order_by("name").filter_by(id=g.user.group_id)
            ).scalars()
        ]

    if form.validate_on_submit():
        dbt.create_user(
            db,
            {
                "name": form.name.data,
                "UserName": form.UserName.data,
                "email": form.email.data,
                "password": form.password.data,
                "role_id": int(form.role_id.data),
                "group_id": int(form.group_id.data),
                "active": True,
            },
        )
        return redirect(url_for("conf.user_mangement"))
    #
    return render_template("setup/user_register.html", form=form)


@bp.route("<int:uid>/update", methods=("GET", "POST"))
@role_required(["admin", "Supervisor"])
def update(uid) -> typing.Union[str, Response]:
    """
    Update a user record.

    We get the user record from the database and fill the
    UserEdit form with it.

    Needs admin rights.

    Parameters
    ----------
    uid : int
        User ID of user to update.

    Returns
    -------
    str|Response
        Render the update page or redirect to user view.

    """
    user = db.session.get(User, uid)

    form = UserUpdateForm(user.name, user.email, obj=user)
    if g.user.role_member.name == "admin":
        form.role_id.choices = [
            (cg.id, cg.name) for cg in db.session.execute(db.select(Role).order_by("name")).scalars()
        ]
        form.group_id.choices = [
            (cg.id, cg.name) for cg in db.session.execute(db.select(Groups).order_by("name")).scalars()
        ]
    else:
        form.role_id.choices = [
            (cg.id, cg.name)
            for cg in db.session.execute(db.select(Role).order_by("name")).scalars()
            if cg.name not in ["admin", "Supervisor"]
        ]

        form.group_id.choices = [
            (cg.id, cg.name)
            for cg in db.session.execute(
                db.select(Groups).order_by("name").filter_by(id=g.user.group_id)
            ).scalars()
        ]

    if form.validate_on_submit():
        if user.id != g.user.id:
            dbt.update_user(
                db,
                user,
                {
                    "name": form.name.data,
                    "UserName": form.UserName.data,
                    "email": form.email.data,
                    "role_id": form.role_id.data,
                    "group_id": form.group_id.data,
                },
            )
            return redirect(url_for("conf.user_mangement"))

        dbt.update_user(
            db,
            user,
            {
                "name": form.name.data,
                "UserName": form.UserName.data,
                "email": form.email.data,
                "role_id": g.user.role_id,
                "group_id": form.group_id.data,
            },
        )
        if form.role_id.data != g.user.role_id:
            flash("You cannot change your own role", "alert-warning")
        return redirect(url_for("conf.user_mangement"))

    return render_template("setup/user_update.html", form=form)


@bp.route("/<string:token>/deactivate")
@role_required(["admin", "Supervisor"])
def deactivate_user(token: str) -> Response:
    """
    Deactivates the user and shares all his samples with the Supervisor(s) of the group.

    If no supervisor exists, we will share samples with user id 1, which is an admin.

    Parameters
    ----------
    token : str
        Token for User to deactivate.

    Returns
    -------
    Response
        DESCRIPTION.

    """
    uid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    if uid != g.user.id:
        user = db.session.get(User, uid)
        g_members = db.session.get(Groups, user.group_id).members
        supervisors = [member for member in g_members if member.role_member.name == "Supervisor"]

        if supervisors is None:
            supervisors = db.session.get(User, 1)
        for s_v in supervisors:
            for sample in user.samples:
                flash(f"Sharing sample {sample.identifier} to {s_v.name}", "alert-info")
                with dbt.Transaction(db):
                    sample.sharedsamples.append(s_v)
        flash(f'User: {user.name} will be deactivated and become a "ExStudent"', "alert-info")
        current_app.logger.info(f"User: {user.name} will be deactivated.")
        dbt.deactivate_user(db, user)

    else:
        flash("You can not deactivate yourself", "alert-warning")

    return redirect(url_for("conf.user_mangement"))


@bp.route("/register_group", methods=("GET", "POST"))
@role_required(["admin"])
def register_group() -> typing.Union[str, Response]:
    """Register new research group."""
    exists = db.session.execute(db.select(Groups)).scalars()

    form = ConfigCreateMType()

    if form.validate_on_submit():
        typename = form.mtype.data
        error = None

        if db.session.execute(db.select(Groups).filter_by(name=typename)).scalar() is not None:
            error = f"Group {typename} is already registered."
        if error is None:
            with dbt.Transaction(db) as db_session:
                db_session.add(Groups(name=typename))
            return redirect(url_for("conf.group_mangement"))
        flash(error, "alert-warning")
    return render_template("setup/registertype.html", form=form, existing=exists, what="Groups")


@bp.route("/update_group/<int:gid>", methods=("GET", "POST"))
@role_required(["admin"])
def update_group(gid: int) -> typing.Union[str, Response]:
    """Update  research group name.

    Parameters
    ----------
    gid : int
        ID ofd group to update.

    Returns
    -------
    str|Response
        Render template or redirect to index.
    """
    exists = db.session.execute(db.select(Groups)).scalars()
    up_group = db.get_or_404(Groups, gid)

    form = ConfigCreateMType(data={"mtype": up_group.name})

    if form.validate_on_submit():
        gname = form.mtype.data
        error = None

        if db.session.execute(db.select(Groups).filter_by(name=gname)).scalar() is not None:
            error = f"Group {gname} is already registered."
        if error is None:
            dbt.rename_group(db, up_group, gname)
            flash(f"Changed group name to {gname} and relabelled documents accordingly", "alert-info")

            return redirect(url_for("conf.group_mangement"))
        flash(error, "alert-warning")
    return render_template("setup/registertype.html", form=form, existing=exists, what="Groups")


@bp.route("/delete_group/<string:token>")
@role_required(["admin"])
def delete_group(token: str) -> Response:
    """
    Delete a group.

    Does not delete the users within, only the group itself. hence, all ESS, PPM and report now belong
    to orphan group_id.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the group to delete


    Returns
    -------
    Flask.Response
        Redirect to admin settings page

    """
    gid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)
    group = db.get_or_404(Groups, gid)
    dbt.delete_group(db, group)
    flash(
        f"Members, samples, documents, mtypes, and locations that belonged to group {group.name}"
        + " were moved to group None",
        "alert-info",
    )
    flash(f"{group.name} was deleted", "alert-success")
    return redirect(url_for("conf.group_mangement"))


########################################################################################################
# Supervisor functions
########################################################################################################
@bp.route("/register_type", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def register_type() -> typing.Union[str, Response]:
    """
    Register a new measurement type.

    Returns
    -------
    str|Response
    """
    exists = db.session.execute(
        db.select(MeasurementType).order_by("name").filter_by(group_id=g.user.group_id)
    ).scalars()

    form = ConfigCreateMType()

    if form.validate_on_submit():
        error = None

        if db.session.execute(
            db.select(MeasurementType).filter_by(name=form.mtype.data, group_id=g.user.group_id)
        ).scalar():
            error = f"MeasurementType {form.mtype.data} is already registered."
        else:
            dbt.create_mtype(db, {"name": form.mtype.data, "group_id": g.user.group_id})
            flash("Measurement type created.", "alert-success")
            return redirect(url_for("conf.index"))
        flash(error, "alert-warning")

    return render_template(
        "setup/registertype.html", form=form, existing=exists, what="Measurement Types"
    )


@bp.route("/update_type/<int:mtype_id>", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def update_type(mtype_id: int) -> typing.Union[str, Response]:
    """
    Update a measurement type name.

    Parameters
    ----------
    mtype_id : int
        MeasurementType ID to change name for.

    Returns
    -------
    str|Response
    """
    exists = db.session.execute(
        db.select(MeasurementType).order_by("name").filter_by(group_id=g.user.group_id)
    ).scalars()
    mtype = db.get_or_404(MeasurementType, mtype_id)

    form = ConfigCreateMType(data={"mtype": mtype.name})

    if form.validate_on_submit():
        typename = form.mtype.data
        error = None

        if (
            db.session.execute(
                db.select(MeasurementType).filter_by(name=typename, group_id=g.user.group_id)
            ).scalar()
            is not None
        ):
            error = f"MeasurementType {typename} is already registered."
        if error is None:
            with dbt.Transaction(db):
                mtype.name = typename

            flash("Measurement type updated.", "alert-success")
            return redirect(url_for("conf.index"))
        flash(error, "alert-warning")
    return render_template(
        "setup/registertype.html", form=form, existing=exists, what="Measurement Types"
    )


@bp.route("/register_location", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def register_location() -> typing.Union[str, Response]:
    """
    Register a new location.

    Returns
    -------
    str|Response
    """
    exists = db.session.execute(
        db.select(Locations).order_by("name").filter_by(group_id=g.user.group_id)
    ).scalars()

    form = ConfigCreateMType()

    if form.validate_on_submit():
        typename = form.mtype.data
        error = None

        if (
            db.session.execute(
                db.select(Locations).filter_by(name=typename, group_id=g.user.group_id)
            ).scalar()
            is not None
        ):
            error = f"Location {typename} is already registered."
        if error is None:
            dbt.create_location(
                db,
                {
                    "name": typename,
                    "group_id": g.user.group_id,
                },
            )
            flash("Location created.", "alert-success")
            return redirect(url_for("conf.index"))
        flash(error, "alert-warning")
    return render_template("setup/registertype.html", form=form, existing=exists, what="Locations")


@bp.route("/update_location/<int:lid>", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def update_location(lid: int) -> typing.Union[str, Response]:
    """
    Update a location name.

    Parameters
    ----------
    lid : int
        Location ID to change name for.

    Returns
    -------
    str|Response
    """
    exists = db.session.execute(
        db.select(Locations).order_by("name").filter_by(group_id=g.user.group_id)
    ).scalars()
    location = db.get_or_404(Locations, lid)

    form = ConfigCreateMType(data={"mtype": location.name})

    if form.validate_on_submit():
        typename = form.mtype.data
        error = None

        if (
            db.session.execute(
                db.select(Locations).filter_by(name=typename, group_id=g.user.group_id)
            ).scalar()
            is not None
        ):
            error = f"Location {typename} is already registered."
        if error is None:
            with dbt.Transaction(db):
                location.name = typename

            flash("Location updated.", "alert-success")
            return redirect(url_for("conf.index"))
        flash(error, "alert-warning")
    return render_template("setup/registertype.html", form=form, existing=exists, what="Locations")


@bp.route("/delete_location/<string:token>")
@role_required(["Supervisor", "StudentAdmin"])
def delete_location(token: str) -> Response:
    """
    Delete a location where samples are stored.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the location to delete


    Returns
    -------
    Flask.Response
        Redirect to admin settings page

    """
    lid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    location = db.session.get(Locations, lid)
    for member in location.samples:
        flash(f"{member.identifier} will move to location id 1", "alert-info")
        with dbt.Transaction(db):
            member.location_id = 1

    for instrument in location.instruments:
        flash(f"{instrument.identifier} will move to location id 1", "alert-info")
        with dbt.Transaction(db):
            instrument.location_id = 1

    with dbt.Transaction(db) as db_session:
        db_session.delete(location)
    flash(f"{location.name} was deleted", "alert-success")

    return redirect(url_for("conf.index"))


@bp.route("/delete_type/<string:token>")
@role_required(["Supervisor", "StudentAdmin"])
def delete_type(token: str) -> Response:
    """
    Delete a type of measurement.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the type of measurement to delete


    Returns
    -------
    Flask.Response
        Redirect to admin settings page

    """
    gid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    mtype = db.session.get(MeasurementType, gid)
    for member in mtype.measurements:
        flash(f"P/P/M type {member.mtype_id} will become id 1", "alert-info")
        with dbt.Transaction(db):
            member.mtype_id = 1

    with dbt.Transaction(db) as db_session:
        db_session.delete(mtype)
    flash(f"{mtype.name} was deleted", "alert-success")

    return redirect(url_for("conf.index"))
