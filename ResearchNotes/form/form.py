# -*- coding: utf-8 -*-
"""
Here we define all form classes to keep the other files a bit more clean.

They are used by Flask - WTForm to render the input formulas.
"""
