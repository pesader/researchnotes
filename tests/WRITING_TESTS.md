# Writing tests

This is a step-by-step guide for writing tests for new classes.

## Create a default object of that class

### Define the object in `_make_info`

Every class that is relevant for testing has one default instance that is made
available to all tests. Some of these classes are not tested directly, but are
relevant for testing other classes (e.g. the `MeasurementType` class is
important for testing the `Measurement` class).

To add the default instance of a newly implemented class, head over to
[conftest.py](./conftest.py) and modify the `_make_info` function inside the
`make_info` fixture so that it conditionally returns information needed to
instantiate your class, like so:

```python
elif model == YourClass:
    info = {
        "generic_field": "test your class generic field",
        "another_field": "test your class another field",
        # ...
    }
```

It is important that this information is enough for a database_transaction
function to create an object of that class.

### Add the object to the testing database `populate_db`

Your next step is to modify the `populate_db` function with one additional
argument and one additional `dbt.*` function call so that it can create
objects of the new class:

```diff
def populate_db(
    # ...
+   your_class_info,
):
    # ...
+   dbt.create_your_class(db, your_class_info)
```

Then, update the call to the `populate_db` in the `app` fixture to account for
your additions. You should use an output of `make_info` as the value of the
argument:

```diff
@pytest.fixture(scope="class")
def app(request, make_info):
    # ...
    with app.app_context():
        # ...
        populate_db(
            # ...
+           your_class_info=make_info(YouClass),
        )
```

### Write a fixture for easily accessing the object

Finally, add a fixture to easily access the default object:

```python
@pytest.fixture(scope="class")
def default_test_your_class(app, make_info) -> YourClass:
    your_class_info = make_info(YourClass)
    return db.session.execute(
        db.select(YourClass).filter_by(generic_field=your_class_info["generic_field"])
    ).scalar()
```

## Write the actual tests

### Create the testing file

Create a new file under the `tests/` directory. Its name must be "`test_`" +
"`name.py`" (e.g. `test_sample.py`, `test_supervisor.py`, etc).

### Mark the file with `pytestmark`

Head over to [pyproject.toml](./pyproject.toml) and add a mark for your file
to the list of `markers`. Then, a line like that to your file:

```python
pytestmark = pytest.mark.name
```

This makes it easier to select and skip tests using the pytest CLI:

```bash
pytest -m "mark"     # selecting
pytest -m "not mark" # skipping
```

### Decide on what you want to test

If possible, you should test every feature of your class. Here are some
that come up frequently:

- Forms
- Create
- Update
- Remove
- Share/Unshare

Encapsulate the tests of each of these features in a *test class*, for
example, `TestSampleCreate`, `TestMeasurementUpdate`, etc. Keep in mind that
these classes are completely independent from each other, so nothing is shared
between them.

In most classes, the needed setup is done using a class-scoped
(`scope="class"`) auto-using (`autouse=True`) fixture called `response`, which
takes care of making the request(s) necessary to get all *test functions*
within it up and running.

### Write the *test functions*

For each *test class*, you may write multiple *test functions*. Your
implementation should follow the project's conventions below:

- `test_success`

> Succeeds if code 200 was returned

- `test_forbidden`

> Succeeds if code 403 was returned. Useful to ensure access control is being
> working properly.

- `test_static_content`

> Succeeds if all hardcoded strings are in the response's html

- `test_dynamic_content`

> Succeeds if all dynamic (i.e. database dependant) strings are in the
> response's html

- `test_database`

> Succeeds if an expected database transactions (e.g. creation, update,
> removal) are completed successfully

- `test_alert`

> Succeeds if both content and the type of an expected alert are correct

There are many fixture at your disposal at [conftest.py](./conftest.py), but
feel free to write your own as well.

## Run the tests

All you to do now is open a terminal in the root of the project and run:

```bash
pytest
```

This will run all tests (except the ones on search, in case you haven't
enabled that feature in your local development environment).
