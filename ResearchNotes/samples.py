# -*- coding: utf-8 -*-
"""
E/S/S or sample submodule.

Everything regarding the samples, experiments or simulations is done here.
"""

import os
import shutil
import copy
import typing

from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    url_for,
    current_app,
    abort,
)

# from werkzeug.exceptions import abort
from werkzeug.utils import secure_filename
from werkzeug.wrappers.response import Response

import ResearchNotes.database_transactions as dbt

# from ResearchNotes.rn_class import current_app

from ResearchNotes.auth import login_required, role_required
from ResearchNotes.database import db, Samples, User, Locations, TemplateSamples, Groups

from ResearchNotes.form import SamplesShare, SamplesCreateForm, SamplesLocation, UseTemplate
from ResearchNotes.files import uploaddir_path, make_file_list
from ResearchNotes.report import _delete_report
from ResearchNotes.measurement import _delete_measurements
from ResearchNotes.url_security import token_decode

bp = Blueprint("samples", __name__, url_prefix="/sample")


@bp.route("/")
@login_required
def index() -> str:
    """
    Index view for the E/S/S part.

    Base function showing the last 5 updated samples, last 5 updated PPM and
    last 5 reports

    Returns
    -------
    str
        Render index template.

    """
    samples_list = g.user.samples[:10] + g.user.sharedsamples[:10]
    # print(len(samples_list))
    return render_template("samples/index.html", samples_list=samples_list)


@bp.route("/group_samples", methods=("GET", "POST"))
@role_required(["Supervisor"])
def groupsamples() -> str:
    """
    Show all ESS of the group.

    Returns
    -------
    str
        Renders template of the group view.

    """
    group_samples = db.session.execute(db.select(Samples).filter_by(group_id=g.user.group_id)).scalars()

    return render_template("samples/groupsamples.html", samples=group_samples)


@bp.route("/allsamples", methods=("GET", "POST"))
@login_required
def allsamples() -> str:
    """
    Show all samples of user.

    Returns
    -------
    str
        Calls render_template for the all sample view (which returns a str).

    """
    return render_template("samples/allsamples.html")


@bp.route("/allsharedsamples", methods=("GET", "POST"))
@login_required
def allsharedsamples() -> str:
    """
    Show all samples shared with and by user.

    Returns
    -------
    str
        Render allsharedsamples template.

    """
    return render_template("samples/allsharedsamples.html")


def create_choose_form(user_id: int):
    """Creates the choose form and returns.

    Parameters
    ----------
    user_id :int
        User ID who's templates we want to use

    Returns
    -------
    Form : WTFForm
        Form to be rendered.
    """

    choose_form = UseTemplate()
    choose_form.template.choices = [
        (cg.id, cg.tname)
        for cg in db.session.execute(
            db.select(TemplateSamples).order_by("tname").filter_by(creator_id=user_id)
        ).scalars()
    ]
    choose_form.template.choices.insert(0, (0, "None"))

    return choose_form


@bp.route("/htmx_fill_template", methods=["PUT"])
@login_required
def htmx_fill_template():
    """htmx route that puts the chosen template into the form.

    We get the content of the choose_form and load the according template. Then the main form is rendered
    again and htmx will put the newly rendered form into the main form.

    We use a put command and therefore have not anymore to POST commands in the form. Also the form is not
    reloaded anymore.

    Returns
    -------
    str
        Newly filled form that is put into the place of the old form.
    """
    current_app.logger.debug(" htmx : Change template")
    choose_form = UseTemplate()

    current_app.logger.debug(f" htmx :Check submit {choose_form.is_submitted()}")

    tobj = None
    if choose_form.template.data != 0:
        tobj = db.session.get(TemplateSamples, choose_form.template.data)

        current_app.logger.debug(f" htmx : New template is {tobj.tname}")

    form = SamplesCreateForm(obj=tobj)

    return render_template("samples/inner_form.html", form=form)


@bp.route("/<int:tid>/create", methods=("GET", "POST"))
@login_required
def create(tid: int) -> typing.Union[str, Response]:
    """
    Create new sample entry. One can choose a template from the top menu.

    Returns
    -------
    str|Response
        Renders create sample template or redirects to sample_view view.

    """
    tobj = None
    if tid != 0:
        tobj = db.session.get(TemplateSamples, tid)

    form = SamplesCreateForm(obj=tobj)

    choose_form = create_choose_form(g.user.id)

    # if choose_form.validate_on_submit():
    #     # print("Hey")
    #     if choose_form.template.data is not None:
    #         return redirect(url_for("samples.create", tid=choose_form.template.data))

    if form.validate_on_submit():
        new_sample = {
            "creator_id": g.user.id,
            "group_id": g.user.group_id,
            "identifier": secure_filename(form.identifier.data),
            "origin": form.origin.data,
            "short_dis": form.short_dis.data,
            "long_dis": form.long_dis.data,
            "creator": form.creator.data,
        }

        s_id = dbt.create_sample(db, new_sample)
        flash(f"Created E/S/S {new_sample['identifier']}", "alert-success")

        return redirect(url_for("samples.sample_view", sid=s_id))
    return render_template("samples/create.html", form=form, chose_form=choose_form, sample=None)


def get_sample(sid: int, check_author: bool = True) -> Samples:
    """
    Get sample id and check permission to act on sample.

    Parameters
    ----------
    sid : Integer
        Sample ID.
    check_author : bol, optional
        Whether to check for permission or not. The default is True.

    Returns
    -------
    sample : ResearchNotes.Sample
        Entry for Sample in database.

    """
    sample = db.get_or_404(
        Samples,
        sid,
        description=f" sample.get_sample : {g.user} tried to load sample {sid}"
        + " which does not exist",
    )

    if check_author and int(sample.creator_id) != g.user.id and g.user.id not in sample.sharedsamplelist:
        abort(
            403,
            description=f"  sample.get_sample : Authorization failure. {g.user} "
            + f"tried to load sample {sid}",
        )
    return sample


@bp.route("/<int:sid>/update", methods=("GET", "POST"))
@login_required
def update(sid: int) -> typing.Union[str, Response]:
    """
    Update sample information.

    We also have to rename the sample directory,
    if it already exits.

    Parameters
    ----------
    sid : Integer
        Sample ID to update.

    Returns
    -------
    str|Flask.Response
        Renders either form or redirects to the sample entry.

    """
    sample = get_sample(sid)

    old_identifier = copy.copy(sample.identifier)

    form = SamplesCreateForm(obj=sample)

    if form.validate_on_submit():
        dbt.update_sample(
            db,
            sample,
            {
                "short_dis": form.short_dis.data,
                "long_dis": form.long_dis.data,
                "identifier": secure_filename(form.identifier.data),
                "origin": form.origin.data,
                "creator": form.creator.data,
            },
        )

        if str(old_identifier) != secure_filename(sample.identifier):

            old_path = uploaddir_path(["s", sample.id, old_identifier])
            new_path = uploaddir_path(["s", sample.id, secure_filename(sample.identifier)])

            if os.path.exists(old_path):
                os.rename(old_path, new_path)
                flash("E/S/S path updated", "alert-success")
            #
        flash("E/S/S information updated", "alert-success")
        return redirect(url_for("samples.sample_view", sid=sid))
    return render_template("samples/create.html", form=form, sample=sample)


@bp.route("/<int:sid>/sample", methods=["GET", "POST"])
@login_required
def sample_view(sid: int) -> typing.Union[str, Response]:
    """
    Show single sample entry.

    Parameters
    ----------
    sid : int
        Sample id to show.

    Returns
    -------
    str|Flask.Response
        Flask view of the sample.

    """
    sample = get_sample(sid)

    files = make_file_list(uploaddir_path(["s", sample.id, sample.identifier]))

    share_form = SamplesShare()
    share_form.user.choices = {
        str(group.name): [
            (u.id, u.UserName)
            for u in group.members
            if u.active and g.user.id != u.id and u.id not in sample.sharedsamplelist
        ]
        for group in db.session.execute(db.select(Groups).order_by(Groups.name)).scalars()
    }

    if share_form.validate_on_submit():
        # print("Hey")
        if share_form.usershare.data:
            with dbt.Transaction(db):
                user = db.session.get(User, share_form.user.data)
                sample.sharedsamples.append(user)
            flash(f"Shared sample with {user.name}", "alert-success")
        return redirect(url_for("samples.sample_view", sid=sid))

    location_form = SamplesLocation()
    location_form.loc.choices = [
        (loc.id, loc.name)
        for loc in db.session.execute(
            db.select(Locations).order_by("name").filter_by(group_id=g.user.group_id)
        ).scalars()
        if loc.id != sample.location_id
    ]
    location_form.loc.choices.insert(
        0, (db.session.get(Locations, 1).id, db.session.get(Locations, 1).name)
    )

    if location_form.validate_on_submit():
        # print("Ho")
        if location_form.location.data:
            with dbt.Transaction(db):
                location = db.session.get(Locations, location_form.loc.data)
                sample.location_id = location_form.loc.data
            flash(f"Changed sample location to {location.name}", "alert-success")
        return redirect(url_for("samples.sample_view", sid=sid))

    return render_template(
        "samples/sample.html",
        sample=sample,
        files=files,
        share_form=share_form,
        location_form=location_form,
    )


@bp.route("/<list:ids>/rmshare")
@login_required
def removeshare(ids: typing.List) -> Response:
    """
    Remove the share between sample and user (only possible for owner).

    Parameters
    ----------
    ids : list[int]
        List of sample and user id.

    Returns
    -------
    Flask.Response
        Flask redirect.

    """
    s_id, u_id = ids
    sample = get_sample(s_id)

    if g.user.id == sample.creator_id:
        s_user = db.session.get(User, u_id)
        with dbt.Transaction(db):
            sample.sharedsamples.remove(s_user)
        flash(f"Share with {s_user.name} was removed", "alert-info")
    else:
        flash("Only sample owner can remove shares", "alert-warning")
    return redirect(url_for("samples.sample_view", sid=s_id))


def _delete_sample(sample: Samples) -> None:
    """
    Sub-function to actually delete the sample and all files associated with it.

    Parameters
    ----------
    sample : ResearchNotes.Sample
        Database entry or record of the report

    Returns
    -------
    None
        None or aborts operation

    """
    # error = None
    path = uploaddir_path(["s", sample.id, sample.identifier])

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except shutil.Error as error:
            # print(error)
            abort(500, description=f"Failed to delete {path}. Exception {error}")
            #
    with dbt.Transaction(db) as db_session:
        db_session.delete(sample)
        flash(f"{sample.identifier} was deleted", "alert-info")


@bp.route("/<string:token>/delete")
@login_required
def delete(token: str) -> Response:
    """
    Delete a sample.

    We need to define cascade of deleting things in the right order. Hence, we will first delete all
    reports (and files associated), then all measurements and finally the sample (and all its files).


    Parameters
    ----------
    token : str
        Signed string that encodes the id of the sample to delete

    Returns
    -------
    Response
        Redirects to the index vies of the ESS module.

    """
    sid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    sample = get_sample(sid)

    if g.user.id == sample.creator_id:
        if not sample.is_shared:
            for report in sample.reports:
                flash(f"Deleting report: {report.title}", "alert-info")
                error = _delete_report(report)
                if error:
                    flash(f"Something went wrong: {error}", "alert-warning")
            for measurement in sample.measurements:
                flash(f"Deleting measurement: {measurement.short_dis}", "alert-info")
                error = _delete_measurements(measurement)
                if error:
                    flash(f"Something went wrong: {error}", "alert-warning")

            _delete_sample(sample)
            # flash(f"Sample {sample.short_dis} was deleted", "alert-info")
        else:
            flash(
                "Sample is shared with others and should not be deleted",
                "alert-warning",
            )
    else:
        flash("Only sample owner can delete samples", "alert-warning")
        abort(
            403,
            description=f" sample.delete : Authorization failure. {g.user} tried"
            + f" to delete sample {sid}.",
        )
    return redirect(url_for("samples.allsamples"))


def sample_summery(sample: Samples) -> str:
    """
    Create a summery Markdown string for all information in the database.

    Parameters
    ----------
    sample : ResearchNotes.Samples
        Entry of sample to summarize

    Returns
    -------
        str
        Markdown formatted string of sample entry.

    """
    summery_text = f"# Sample {sample.identifier}" + "\n"
    summery_text += (
        f'by *{sample.creator}* inserted *{sample.created.strftime("%Y-%m-%d")}* coming'
        + f" from *{sample.origin}* by *{sample.creator}* stored at *{sample.sample_location.name}*"
        + ("\n" * 2)
    )
    summery_text += "## Short description \n"
    summery_text += f"{sample.short_dis}" + ("\n" * 2)
    summery_text += "## Long description\n"
    summery_text += sample.long_dis.replace("\r\n", "\n")
    summery_text += "\n \n ---------------------------------------------------------------\n\n"

    for measurement in sample.measurements:
        summery_text += f"## Measurement: {measurement.short_dis}" + "\n"
        summery_text += (
            f"*{measurement.measurement_type.name}* by *{measurement.creator}* "
            + f'updated *{measurement.updated.strftime("%Y-%m-%d")}*'
            + ("\n" * 2)
        )
        summery_text += "### Description" + "\n"
        summery_text += measurement.long_dis.replace("\r\n", "\n") + "\n\n"

        for report in measurement.reports:
            summery_text += f"## Report: {report.title}" + "\n"
            summery_text += f'by *{report.creator}* written *{report.updated.strftime("%Y-%m-%d")}*' + (
                "\n" * 2
            )
            summery_text += "###  Description \n"
            summery_text += report.long_dis.replace("\r\n", "\n") + "\n \n"
        summery_text += "\n \n ---------------------------------------------------------------\n\n"
    # if summery_text is None:
    #     current_app.logger.debug(f"{sample}")

    return summery_text


@bp.route("/<int:sid>/summery")
@login_required
def summery(sid: int) -> Response:
    """
    Make a .md file of a sample entry.

    Create a database dump of all entries for a given sample as well as attached
    measurements and reports. Everything is written into a markdown file, which
    can then be downloaded to create a backup of the information.

    Parameters
    ----------
    sid : int
        Sample id of sample to be exported.

    Returns
    -------
    Flask.Response
        Redirect to sample view

    """
    sample = get_sample(sid)

    summery_text = sample_summery(sample)

    path = uploaddir_path(["s", sample.id, sample.identifier])
    if not os.path.exists(path):
        os.makedirs(path)

    with open(
        os.path.join(path, secure_filename("SampleSummery-" + str(sample.identifier) + ".md")),
        "w",
        encoding="utf-8",
    ) as out_file:
        out_file.write(summery_text)

    flash(
        f"Summery created in file {secure_filename('SampleSummery-' + str(sample.identifier) + '.md')}",
        "alert-info",
    )

    return redirect(url_for("samples.sample_view", sid=sid))
