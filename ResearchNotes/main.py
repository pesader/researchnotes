# -*- coding: utf-8 -*-
"""
Main module of the program.

Basically just contains the index html view that it
does not be defined in the __init__ part. Also, we can later put other views here
that are not related to any sub-menu or other module.
"""

import os
import typing

# from typing import Union

from werkzeug.utils import secure_filename
from werkzeug.wrappers.response import Response

from flask import (
    Blueprint,
    render_template,
    current_app,
    send_from_directory,
    g,
    redirect,
    url_for,
    flash,
)

from ResearchNotes.auth import login_required
from ResearchNotes.documents import last_updates, export_doc_text
from ResearchNotes.samples import sample_summery
from ResearchNotes.database import Samples, Measurements, Reports, Documents


bp = Blueprint("main", __name__, url_prefix="")


@bp.route("/")
@login_required
def index() -> str:
    """
    View showing the last samples and main page.

    Returns
    -------
    str
        Renders the index page.
    """
    docs = last_updates()

    return render_template("index.html", docs=docs)


@bp.route("/favicon.ico")
def favicon() -> Response:
    """
    Fix favicon 403 error.

    Returns the favicon for browser that ignore the template or if back bottom is hit.
    Otherwise, we will have a lot of 404 errors over the log filers, if someone hits the
    back bottom.

    Returns
    -------
    Flask.Response
        Favicon file to be shown.
    """
    return send_from_directory(os.path.join(current_app.root_path, "static/img"), "favicon-16x16.png")


@bp.route("/lab_book")
@login_required
def lab_book() -> Response:
    """
    Create a "lab book" of all entries of user and download as Markdown file.

    The file will be created in the upload directory for the user specific name. We might need to
    do some cleaning form time to time.

    Returns
    -------
    Response
        Markdown-file to download
    """
    all_text = ""
    for sample in g.user.samples:
        # assert sample is not None
        all_text += "\n\n -------------- \n\n"
        all_text += str(sample_summery(sample))

    all_text.encode("utf-8")

    for doc in g.user.group_member.documents:
        all_text += export_doc_text(doc)

    file_name = secure_filename("All_samples_" + f"{g.user.UserName}" + ".md")

    with open(
        os.path.join(current_app.config["UPLOAD_PATH"], file_name),
        "w",
        encoding="utf-8",
    ) as out_file:
        out_file.write(all_text)

    return send_from_directory(
        current_app.config["UPLOAD_PATH"],
        file_name,
        as_attachment=True,
        download_name=secure_filename(file_name),
    )


@bp.route("/search")
@login_required
def search() -> typing.Union[str, Response]:
    """
    Display the result of the search.

    Returns
    -------
    str|Response
    """
    if not g.search_form.validate():
        return redirect(url_for("samples.index"))

    if not current_app.elasticsearch and not current_app.meilisearch:
        flash("No search engine defined!", "alert-warning")

    results, _ = Samples.search(g.search_form.q.data)
    current_app.logger.debug(f"Found: {results}")
    if results:
        samples: typing.Optional[typing.List] = [
            s for s in results if g.user.id in s.sharedsamplelist or s.creator_id == g.user.id
        ]
    else:
        samples = None

    results, _ = Measurements.search(g.search_form.q.data)
    current_app.logger.debug(f"Found: {results}")
    if results:
        measurement: typing.Optional[typing.List] = [
            s
            for s in results
            if g.user.id in s.measurement_sample.sharedsamplelist
            or s.measurement_sample.creator_id == g.user.id
        ]
    else:
        measurement = None

    results, _ = Reports.search(g.search_form.q.data)
    current_app.logger.debug(f"Found: {results}")
    if results:
        reports: typing.Optional[typing.List] = [
            s
            for s in results
            if g.user.id in s.reports_sample.sharedsamplelist or s.reports_sample.creator_id == g.user.id
        ]
    else:
        reports = None

    results, _ = Documents.search(g.search_form.q.data)
    current_app.logger.debug(f"Found: {results}")
    if results:
        docs: typing.Optional[typing.List] = [s for s in results if s.group_id == g.user.group_id]
    else:
        docs = None

    current_app.logger.debug(f"Sample list : {samples}")
    current_app.logger.debug(f"Measurement list : {measurement}")
    current_app.logger.debug(f"Report list : {reports}")
    current_app.logger.debug(f"Documents list : {docs}")

    return render_template(
        "search/search.html",
        title="Search",
        samples=samples,
        measurements=measurement,
        reports=reports,
        docs=docs,
    )
