from flask import current_app, g, url_for
import pytest
from werkzeug.utils import secure_filename

from ResearchNotes import db
from ResearchNotes.database.basics import Locations
from ResearchNotes.database.ess_ppm_report import Samples
from ResearchNotes.database.instrument_journal import EntryType
import ResearchNotes.database_transactions as dbt
from ResearchNotes.url_security import safe_url_for

pytestmark = pytest.mark.etype

class TestEntryTypeCreateFormView:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_instrument):
        with app.test_request_context():
            response = client.get(url_for("etype.create", iid=default_test_instrument.id))
            return response

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible
        assert 'Name' in html
        assert 'Existing Entry types' in html
        # source
        # NOTE: to create etypes, we use ConfigCreateMType() that's why mtype comes up
        assert 'name="mtype"' in html

    def test_dynamic_content(self, html, default_test_instrument):
        for etype in default_test_instrument.etypes:
            assert etype.name in html

class TestEntryTypeCreateFormTransaction:
    new_etype_info: dict[str, str] = {
        "name": "new etype name",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_instrument):
        with app.test_request_context():
            return client.post(
                url_for("etype.create", iid=default_test_instrument.id),
                data={
                    # NOTE: to create etypes, we use ConfigCreateMType() that's why "mtype" comes up
                    "mtype": self.new_etype_info["name"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def new_etype(self, app):
        with app.test_request_context():
            return db.session.execute(
                db.select(EntryType).filter_by(
                    name=self.new_etype_info["name"]
                )
            ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible
        assert 'Instrument:' in html
        assert 'Associated Instrumentation Journal Entry types' in html

    def test_dynamic_content(self, html):
        assert "<td>test entry type name</td>" in html

    def test_alert(self, html):
        assert "alert-success" in html
        assert f"Entry type created" in html

    def test_database(self, new_etype):
        assert new_etype is not None


class TestEntryTypeUpdateFormView:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_etype):
        with app.test_request_context():
            response = client.get(url_for("etype.update", etid=default_test_etype.id))
            return response

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible
        assert 'Name' in html
        assert 'Existing Entry types' in html
        # source
        # NOTE: to update etypes, we use ConfigCreateMType() that's why "mtype" comes up
        assert 'name="mtype"' in html

    def test_dynamic_content(self, html, default_test_instrument):
        for etype in default_test_instrument.etypes:
            assert etype.name in html


class TestEntryTypeUpdateFormTransaction:
    updated_etype_info: dict[str, str] = {
        "name": "updated etype name",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_etype):
        with app.test_request_context():
            return client.post(
                url_for("etype.update", etid=default_test_etype.id),
                data={
                    # NOTE: to update etypes, we use ConfigCreateMType() that's why "mtype" comes up
                    "mtype": self.updated_etype_info["name"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def updated_etype(self, app):
        with app.test_request_context():
            return db.session.execute(
                db.select(EntryType).filter_by(
                    name=self.updated_etype_info["name"]
                )
            ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert 'Instrument:' in html
        assert 'Associated Instrumentation Journal Entry types' in html

    def test_dynamic_content(self, html):
        assert self.updated_etype_info["name"] in html

    def test_alert(self, html):
        assert "alert-info" in html
        assert f"Entry type {self.updated_etype_info['name']} updated" in html

    def test_database(self, default_test_etype):
        assert default_test_etype.name == self.updated_etype_info["name"]


class TestEntryTypeDelete:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_etype):
        with app.test_request_context():
            # NOTE: g.salt is only available on certain views
            client.post(
                url_for("instruments.instrument_view", iid=default_test_etype.instrument.id),
                follow_redirects=True,
            )
            url = safe_url_for(
                default_test_etype.id, "etype.delete", current_app.config["SEC_SESSION_KEY"], g.salt
            )
            return client.get(url, follow_redirects=True)

    def test_success(self, response):
        assert response.status_code == 200

    def test_database(self, default_test_etype):
        assert db.session.get(EntryType, default_test_etype.id) is None

    def test_dynamic_content(self, app, html, default_test_etype):
        assert f"<td>{default_test_etype.name}</td>" not in html

    def test_alert(self, default_test_etype, html):
        assert f"{default_test_etype.name} successfully deleted" in html
        assert "alert-success" in html
