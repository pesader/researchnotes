Finally, you can create reports for your PPMs. The number of reports to a PPM is not limited as single PPMs are analyzed sometimes more than once 
or in a different way. If you have to analyze data from different ESS doing the same PPM, it might be worth it to create an ESS for this analysis.

### Create report

Opens a form that allows you to create a report to a PPM. It contains basically two fields:

**Title:** Give a title to your report

**Long Description:** Basically, a free text field containing your report. Again, you can take advantage of Markdown formatting as provided by the toolbar or 
by typing the Markdown command as explained in the [Markdown help](markdown).

After submission, you will enter the report view or page that allows some further operation:

### Return ESS

Gets you back to the EES parent entry

### Return PPM

Gets you back to the parent PPM entry

### Edit

Edit the information in the report form.

### Associated Files 

Same functionality as before. You can upload, preview and store different files. A Dropzone makes uploading much easier.
