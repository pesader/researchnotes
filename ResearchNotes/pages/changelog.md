title: Change log Open ResearchNotes
published: 2022-05-25


Changelog of Open ResearchNotes
==============================
**Version 1.3.1**

- Introduced breadcrumb menu to Documents, ESS-PPM-Report and Instrument chain.
- Page does not reload on adding/deleting file anymore (thanks to htmx).
- Modal for Markdown help and Image (for update) using htmx.
- Frontend gets more inactive thanks to _hyperscript.
- Fix of templates editing and creating to have all fields available.
- Some UI improvement and standardization. Now functions are either buttons or links but clearly visible.
  Icons are more standardized.
- Bug fix: Make sure that default templates are set to None.
- Instrument and JEntry templates can be edited by group and not only by creator.
- Bug fixing: Checking ids of m_type and e_type for measurement and jentry create.
- Code clean up and finished migration to new query schema (issue #115).

**Version 1.3.0.1**
- Fix bug in delete_measurement_template.

**Version 1.3.0**

- Code clean-up.
- Ensure, user cannot share sample with himself (issue #135).
- Instrumentation log with Journal entries (issue #50).
- Small changes to admin and general UI.

**Version 1.2.6**

- Enabled master-key for meilisearch. Meilisearch is now preferred search engine.
- Created a StudentAdmin role that can modify mtypes and locations (issue #106).
- Ensure that sample identifier is going through secure_file name (issue #117).
- Made sure that admins/supervisors cannot deactivate themselves or change their role (issue #100).
- Included more test and rewrote part of the test infrastructure.
- Started to change database query scheme to follow the new scheme of Flask-SQLAlchemy (issue #115).
- Updated Bootstrap and other packages.
- Included bandit into test results.
- After sample delete, return to all sample view and not home view.
- Fix sample creator entrance.
- Fix some issue with datatables.js (issue #133).
- Preview for unknown file type as text (issue #144).
- Update some of the documentation.
- Move create sample and other stuff to database_transaction.
- Improvement of alert msgs and UI msgs.
- Include template select in sample create form (issue #118).

**Version 1.2.5**

- Fixes to UI like duplicated bottoms, errors on template choice etc (issues #110, #103, #104).
- More code clean up and deleting not needed code.
- Update doc, docstrings and Readme.
- Fix IP and login date registration (issue #99).
- Fixing user registration and update (issue #104).
- Internal improvements (clean up and moving stuff to database transaction modul). Split create/update function for location and mtype.
- Fixed inconsistency in admin rights. Admin lost right to edit location and measurements types (issue #102)
- Fixed issues related to Flask 2.2.x. Mainly json dump and send_form_directory (issue #96 and #98).

**Version 1.2.4**
(20.07.2022)

- Make sessions permanent again.
- Some internal code cleaning and optimization (as always).
- Started to add role, alt and arial-labels to most of the SVG icons.
- Location and MType names are now changeable.
- Supervisors now can add, deactivate and reactivate students to their group.

**Version 1.2.3**
(20.06.2022)

- Include bleach to prevent XSS attacks from Markdown formatted  texts.
- Small improvements to the UI like order of tables and lists as well as demonstrated fields.
- Login page more password manager friendly.
- Update change password dialog.
- optgroup for share user selection.
- Collapse for long description.
- Update to admin interface.

**Version 1.2.2**
(10.06.2022)

- Close issues #77, #76, #75 and #72.
- Small updates to admin area.
- Small updates to UI.
- Search function implemented for elastic search and meilisearch.

**Version 1.2.1**
(25.05.2022)

- Download for files in preview.
- Improved UI for ESS entrance view.
- Export of whole lab book.
- A lot of code cleaning and typing.

**Version 1.2.0**
(10.05.2022)

**Version changes database. Need to run flask db migrate.**

- Fixed security issue #30 and defined a send_preview function.
- Closed issue #47 - using save_url to make part of the URL not guessable.
- Implemented one index per group and delete link to child (issue #51, #53 and #68)
- Implemented app overview for admin (issue #49).

**Version 1.1.6**
(03.05.2022)

- Fixed database issue #66 (measurement template creates copy of itself and does not update).
- Small updates to about.md, author.md and help.md. Added a markdown help.
- Introduced a markdown toolbar (issue #64).
- Fixed mtype order (issue #58) as well as visibility of measurement types and locations on create dialog (issue #63).
- Fix order of tables (disable to order after action).
- Changed groupview from sample.creator to sample.sample_owner.UserName (issue #59).
- Order files in sample, measurement and report view after name and extension (issue #62).

**Version 1.1.5**
(25.04.2022)

- Added back bottom to preview as well as new preview functions
- Added datatabel.js in table view.
- Updated menu bar (corrected typo) and added user_info view.
- Cli function to clean doc database.
- Documents labels are now changeable (issue #53). Documents not longer become child of themselves on editing.
- Fix new created document to remember parent (issue #35).
- Solved back bottom for child and parent link outside text body (issue #42).
- Include a preview for .txt, .dat and .csv files. Improved image preview.
- Include a back bottom to sample and measurement/report into file preview (issue #34).
- Created central .toml file closing issue #52. Now flit is used to build packages.

**Version 1.1.4**
(11.04.2022)

- Fix role_required to avoid error on session expire or with g.user is None (issue #38 reopened).
- Fix small database bugs.
- Made change to work with flask 2.1.1.
- Import typing List for compatibility with python 3.8.

**Version 1.1**
(06.04.2022)

- Added function to load database from json. That should end issue #39 and allow database migration from one engine to the other.
- Updated sample summery to be written into .md file.
- Fixed typo in form.py and corrected docstrings.
- All database entries are now @dataclass and all relevant variables should be typed.
- All database transaction goes now into the database_transaction.py module. Still working on issue #24.
- Issue #39: Updated csv dump function in cli.py. Added dump to json function to cli.py.
- Fixed sample delete that shared samples cannot be deleted and only owner can delete sample (closed issue #44).
- Fixed issue #41 on windows. Needs a test on BSD.
- Started to fix issue #41 (needs testing again). Fixed small things in cli.py and database.py.
- Database Transaction implemented (issue #25). Started to write functions for database transactions.
- Fixed error with back bottom (issue #46).
- Removed delete bottom from allsharesample view - hence, a sample shared to you can not be delete without hand typing the URL (partly addresses issue #44)
- Modified shared sample retrieval - now samples shared by you can be sorted (issue #45).
- Fixed issue #45 partly - ordering now calls allsharedsamples, but the passed parameters are ignored.
- Implemented list in shared view that shows samples shared by the user to someone. Closes issue #21.
- Implemented is_shared attribute in sample to check, if the sample is shared to another user.
- Updated format string for syslog logging

**Version 1.0.16**
(23.03.2022)

- Add configuration parameter for the syslog handler. Hopefully closes issue #32.
- Issue #39: Added database dump to csv that should allow to reconstruct the whole database and the dependencies. Does not include templates.
- Changed layout document show.
- Fixed issue #41 on create database.
- Fixed login screen by removing body instructions from sign.css. Closes #19
- Changed auth.logout to separately handle g.user is None to avoid passing an empty user to the function (or to have it wrapped into login function).
- Removed setup from static pages as it does not make much sense. It is better in the docs and git wiki.
- Replaced gif icons with Bootstrap svg icons.
- Fixed python code styling and preview. New dependency for to add into setup.py.

**Version 1.0.15**
(16. March 2022)

- Introduced login to syslog (issue #32). Introduced two config vars to enable/disable logging to file and syslog, respectively
- Moved index view to new module main to have no views defined in the create_app function
- Registering error blueprints only if we are not in debug mode
- Updated error messages for 500 error page.
- Fixed issue #38 by implementing our own time stamp in session cookie.
- Slightly updated log format.
- Included a parent part to page to show, which pages link to a given page (makes jumping back easier).

**Version 1.0.14.1**
(17. Feb. 2022)

- Removed file check in  make_uploaddir_path_str as we get an error, if there are no files attached to things

**Version 1.0.14**
(17. Feb. 2022)

- Changed the app_cofiguration. Now log_file will be made by os.path.join(LOG_PAT, LOG_FILENAME). LOG_FILE will be removed
- Implemented catch all exception page to handle non HTTPExceptions
- Improved logging output

**Version 1.0.13.1**
(16. Feb. 2022)

- Fixed wrong if-statement for logging in 1.0.13
- Implemented logging. All errors and exceptions are now logged. Exceptions call an aboard function (so we lose the module, they are thrown). Closes issue #22
- Fixed broad exception by shutil, os and SQLAlchemy. Exception handling is a bit untested.
- Renamed config module to setup.py to make it clearer that it not deals with the app config. Frees also the name config.py
  - Blueprint name stays unchanged to not mess up with the internal URL generation. Template folder has been moved to templates/setup/
- Created a docs directory and started documentation using sphinx with myst-parser.
- Standardized variable names fo Sample,m Measurement and Report database entry
- Changed names of samples.sample, measurement.measurement and report.report as suggested in issue #29 to clean name space
- Sample.py, Measurements.py and Report.py now use file.make_uploaddir_path_str
- Started to make abort louder and print passed error message (good for future logging).
- Stated python preview - but the markdown needs configuration of CodeHilite extension. This requires install
  of Pygments
- Solved issue of HTML injection in markdown block (#28)
- All function and classes should have now docstrings (as checked by code analysis). Closes issue #7
- Renamed db.py to database.py to separate from the internal variable db
- Moved template_filter("markdown") and doc_url to util.py and use blueprint to register template filter
- Moved markdown_redner of flatpages to util.py (now joined with fpages function displaying .md files)
- Moved error handling into error.py blueprint
- Introduced the file.make_uploaddir_path_str and preview.make_subdir_prefix_str to generate the needed path to ESS, PMM or report
- Write user.UserName instead of user.name into report.creator on creation.
- Fixed issue #27. Changed add-validators to Register and UserEdit form
- Renamed init-db to init_db for command line to follow PEP8
- Updated view (html template of measurement and sample.index to show report updates)
- Creating a report sets sample.updated and measurement.updated to datetime.datetime.utcnow(). That should close issue #6
- Updated form.py doc-strings
- Cleaned code from using reserved words - mainly id and type - solving issue #25. Also resolved most of the pylind warnings.
- On create measurement updates sample.updated to datetime.datetime.utcnow()

**Version 1.0.12.1**
(14. Dec. 2022)

- On update measurements and report put time and date into sample.updated (measurement.updated for reports)
- Updated some of the abort(404) errors with get_or_404 and first_or_404 function
- Update jquery to 3.6 (slim).
- Update bootstrap to 4.5.3 (.css and .js).
- Update dropzone.js and .css to 5.9.3. Newer version are not anymore available as
  distribution.
- Introduced back_populates into User.sharedsamples and Samples.sharedsamples. Closes
  issue #14 but changes code behavior. Needs more testing. (02.01.2022)
- Defined a flask shell context function to use flask shell to test database.
- Changed backref in Groups database to solve issue #20 (28.12.2021)
- Update documentation of db schemes
- Define VERSION_STRING which is read from version.txt in root (and written by setup.py)
- Fixed issue #17 - rename sample directory (17.12.2021)
- Fixed issue #18 - error for .css at tree.html (16.12.2021)
- Slightly changed menu layout (16.12.2021)
- Improve doc string of some functions and classes.
- Setup.py now copies now version string to version.txt in app root and CHANGELOG to static/changelog.md

**Version 1.0.11**
(18. Nov. 2021)

- Fixed issue #13 (Windows configuration)
- Update setup.md and help.md
- Test setup on windows using waitress

**Version 1.0.10**
(18. Aug. 2021)

- Fixed issue #9 (Dropzone was broken by error in base.html)

**Version 1.0.9**
(03. Aug. 2021)

- Cloned to git and started git version control 24.07.2021
- Fixed bugs found in 1.0.8 except large .zip file
- PDF preview - 18.09.20
- Security question before delete (except docs) - 18.09.2020


**Version 1.0.8**
(25. August. 2020)

- Fix app_configuration (Production) for CONFIG_FILE

**Known Bugs**

- **Bug:** E/S/S Group view for supervisor just shows first 10 results.
- **Bug:** Large files result in time-out by downloading .zip (takes to long to be created).
- **Bug:** Sub-directories are packed and file started to pack itself into archive, if you start download of all files in directory.
- **Bug:** Document tree enters endless loop, if child refers back to parent.
- **Bug:** Upper case file extensions not recognized - put .lower to string identifier in preview.


**Version 1.0.7**

- Fix Unix environment variable
- Output for configuration


**Version 1.0.6**

- Change program name to something not existing
- Code formatting

**ToDo**

- Replace id number by name in Document changes - 20.08
- Include index with latest worked Documentation (maybe do dashboard like index page) - 22.08
- Page deletion Documents; Build tree of pages - 22.08
- Templates management - 24.08


**Version 1.0.5**

- Create documentation index, if not exists
- Include preview for P/S/S and report view

**Version 1.0.4**

- Fixed markdown of the sample summery
- Did housekeeping and defined some Error pages.
- Included a Wiki like Documentation for depositing Setup descriptions, experimental protocols etc. Can be referred from E/S/S and P/M/M description.
- FlatPages starts having problems with the file encoding. Changed it to FLATPAGES_ENCODING = 'cp1252'
- Added style sheet for markdown tables to be more readable (bootstrap does not explicitly change table-tag)
- Change dropezone.css to views that need them (move dropezone.js to script block)
- Changed sample to E/S/S
- Sample returns to sample page after update.
- Introduced code formatting by black.
- Small change to meta tag in base.html.
- Ensure safefile name E/S/S identifier at creation.
- Increase field length short description sample to 50 characters.
- Mariadb keeps crashing, when constructing initial database. Might be a memory problem of the FreeBSD
  machine with only 2GB or the use of Pypy3. No clear solution except to use sqlite for now.

**Todo**

- Preview for files - Preview for markdown file and images - 18.08
- Overview P/P/M for easier search - 15.08
- Ensure that P/P/M keeps Type during update - seems not so trivial. Might have to define a property


**Version 1.0.3**


- Give all StringFields a length as requiered by SQL database (not sqlite). Limited
  length in all form fields.
- Fixed misspelled path in Unix configuration

**Version 1.0.2**
(11.08.2020)

- Repropose field *sample.creator* as the original idea can be replaced by sample.creator_id and relationship reference - 10.08.20
- Implemented basic sample and P/P/M template -11.082020
- Small front end improvements, updates to help and about
- Fixed access of shared sample. Now owner can access measurements of other made with his sample. 09.08
- Make sample shares removable by owner - 09.08
- Fixed ordering of shared samples and tables in allsamples-view - 08.08

**Todo**

- Update sign-in page - 08.08.2020
- Use the track data of user to generate some login and out record - 08.08.20
- Fix sharing out data when user is deactivated - 08.08.20
- Generate some sort of reporting to get sample/measurement/report data out - 08.08: A
  markdown dump of the database is created in the sample directory

- Implement data structure for templates to freeze database structure - 09.08
- Write a startup script for pypy3 and waitress for windows 10 - 08.08.20
- Change config for 'NT' to values useful for everyone (make directory in user\.appdata) - done 08.08

**Version 1.0.1**

- Changed config to work
- Updated setup.py to include all dependencies

**Version 1.0**
(07. August 2020)

**Todo for Version 1.0**

- Rename the tutorial app to LabSpace - done 27.07
- Use SQLAlchemy  as database back end (we can move to a real database later) - done 28.07
- Additionally include flask_migrate, which might come handy at some point - done 28.07
- Use WF-forms to make all the forms - done 29.07 for register and login.
- Implement some user registry and changes - partly done 29.07;
- Change the logic from Blog post to samples - done 29.07
- Implement creating/deleting and updating samples - done 29.07
- Implementing attaching new measurements to a sample - done 30.07
- Implement a report per measurement - done 31.07
- Long description excepts formatting of field in markdown. Dangerous as we do not check for nasty links! - done 02.08
- Associate files with sample, measurement and reports - done 03.08, including download using a zip file
- Implement a delete cascade from sample, measurement and report as well as files for this - done 04.08
- Implement role_model for different functions. Also only owner can edit or change sample - done 04.08
- Implement user reset password and user delete/deactivate  - done 04.08
- Change index to show just 10 results and make page for all samples - done 5.08
- Download and delete single file - done 5.08
- sample sharing  - done 05.08
- Look into flash-FlatPages - implementation of Markup language for static pages and documentation - done 06.08
- Use some bootstrap design to make a better front end - see flask-bootstrap -done 06.08
- Move config to production - Done 07.08
- Implement deleting of types and groups - done 07.08
- Make setup file for package - done 07.08
