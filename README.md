# Open ResearchNotes 

Open ResearchNotes is an electronic lab book (ELN) to keep track of your experiments/samples/simulations,
procedures/processing/measurements and results. Unlike your handwritten lab book, it is organized by 
samples - which are understood of the entity you are investigated. All procedures, measurements and 
results are connected to the sample keeping things in a logical order and not distributed over
various places.

## Features

- Organization of the research data by samples (ESS entries), to which measurements (PPM entries) and reports are connected.

- ESS entries have identifiers and description to provide metadata to attached files and to physical samples. We also have extra fields to mark for ESS origin, creator and creation place. We track creation and 
update times for you. These entries can be ordered and searched to find data or samples information.

- Indexing against a full text search engine (ElasticSearch or Meilisearch) allowing to search the long description and other relevant fields.

- Users are organized by research groups and can have either the role of a student or supervisors. On leaving the group, the student account is converted into a former student account and data shared with the supervisor.

- Easy upload of files. For various file types, a preview function is implemented.

- Long description text for the ESS, PPM and report entries can be formatted and translated to HTML using Markdown. A Markdown toolbar is implemented to help to learn the commands. The Markdown can refer to uploaded images.

- A documentation wiki using Markdown as to format and creation of new documents containing information not directly connected to samples (e.g. experimental protocols, common procedures etc.). Documents are shared and 
editable on the group bases.

- ESS entries can be shared  between persons with an account in the software allowing collaboration and collection of research data form different sources in one entry.

- ESS location can be marked and used to track the location of a sample.

- ESS entries are exportable to Markdown files for storage and backup outside the software.

- A dedicated instrumentation journal exist that allows tracking the status, changes or information of setups used for measurements that are not directly related to ESS. Instruments are managed at the group level and can be shared to other users to keep information coherent.

## Installation

See the documentation for a detailed installation instructions for different OSs.
But the base idea is:

`pip install ResearchNotes-1.xx.zz-py3-none-any.whl`

Write and change the parameter in a config.py to serve your needs and OS, mainly adjusting the UPLOAD directory and
database engine:

~~~~
UPLOAD_PATH = r"c:\ResearchNotes"
SQLALCHEMY_DATABASE_URI = r"sqlite:///c:\ResearchNotes\ResearchNotes.sqlite"
# SQLALCHEMY_DATABASE_URI = "mysql+pymysql://ResearchNotes@127.0.0.1/ResearchNotes?charset=utf8"
~~~~

You will have to install a python package appropriate for your database engine. 

Create a wsgi app.

~~~~
import sys
from werkzeug.middleware.proxy_fix import ProxyFix

from ResearchNotes import create_app

app_wsgi = create_app("/usr/local/www/researchnotes/config.py")

app = ProxyFix(app_wsgi, x_for=1, x_host=1)

if __name__ == "__main__":
    app.run(host='0.0.0.0')

~~~~

Run a WSGI server like Gunicorn (together with supervisord) to serve the app:

`gunicorn  wsgi:app`

Finally, use a proxy like nginx to serve the app to your users.

## Contribute

We are happy to get new contributors to the project. Please contact the project maintainer first before starting coding.

See our wiki, how to setup a development environment and see the code rules. We also have some kind of workflow established.

For more details, see our CONTRIBUTING file or the wiki.

## Support

If you run into issues, have questions, found a bug or want to help, you can open an issue in the issue tracker.

Also see the gitlab  wiki and the [documentation](https://open-researchnotes.readthedocs.io/en/latest/) for help.

## License and dependencies

The software is published under GPLv3 license as formulated in 29 June 2007.

Open ResearchNotes uses:

- Python 3.10 or better
- flask
- flask SQLAlachemy
- flask migrate
- flask FlatPages (including the markdown python module)
- flask WTForms
- Bootstrap, dropzone.js simpleMDE.js, htmx, _hyperscript and DataTable.js (front end)

and the packages, they depend on. By default, **sqlite** is used as database but code is tested also against **mariadb**.
