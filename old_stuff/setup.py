# -*- coding: utf-8 -*-
"""
File is outdated.

We switched nto a different build system and do not use anymore setup tools. 
Even for setuptools, we should move the information to the .tmol file or create
a setup.cfg file.

Setup.py to create the ResearchNote package.

This will create the wheel package for ResearchNotes as well as copy some stuff
like changelog around. Also updates the version number in the package
diurectory.

Created on Fri Aug  7 14:41:32 2020

@author: Christoph
"""
from setuptools import find_packages, setup
from shutil import copyfile

copyfile("CHANGELOG", "ResearchNotes/pages/changelog.md")

rn_version = "1.1.4"

with open("ResearchNotes/version.txt", "w") as f:
    f.write(rn_version)
    #
setup(
    name="ResearchNotes",
    version=rn_version,
    packages=find_packages(),
    #    package_data={"": ["version.txt"]},
    include_package_data=True,
    zip_safe=False,
    author="Christoph Deneke",
    author_email="christoph@deneke.net",
    description="An electronic labbook and data archiver",
    install_requires=[
        "flask",
        "flask_sqlalchemy",
        "flask_migrate",
        "flask_flatpages",
        "flask-wtf",
        "pygments",
        "python-dotenv",
    ],
)
