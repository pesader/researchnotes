from typing import Final, Generator

from flask import current_app
import pytest

from ResearchNotes import db
from ResearchNotes.database.ess_ppm_report import Samples
import ResearchNotes.database_transactions as dbt
from ResearchNotes.search import add_to_index, query_index, delete_index

pytestmark = pytest.mark.search


@pytest.mark.slow
@pytest.mark.backend
@pytest.mark.skip
class TestSeachInitialized:
    def test_elasticsearch(self):
        assert current_app.elasticsearch is not None


@pytest.mark.slow
@pytest.mark.backend
@pytest.mark.skip
class TestSearchResults:
    PREFIX: Final = "search"

    @pytest.fixture(scope="class")
    def index(self, app, client) -> Generator:
        for n in range(1, 11):
            new_sample_info: dict[str, str] = {
                "creator_id": "1",
                "group_id": "1",
                "identifier": f"{self.PREFIX} sample identifier " + str(n),
                "origin": f"{self.PREFIX} sample origin " + str(n),
                "short_dis": f"{self.PREFIX} sample short_dis " + str(n),
                "long_dis": f"{self.PREFIX} sample long_dis " + str(n),
                "creator": f"{self.PREFIX} sample creator " + str(n),
                "location_id": "1",
            }

            dbt.create_sample(db, new_sample_info)
        samples = db.session.execute(db.select(Samples)).scalars()

        idx = "test"
        for s in samples:
            add_to_index(idx, s)
        yield idx
        delete_index(idx)

    @pytest.fixture(scope="class")
    def hits(self, index):
        hits, _ = query_index(index, f"{self.PREFIX} sample")
        return hits

    @pytest.fixture(scope="class")
    def times(self, index):
        _, times = query_index(index, f"{self.PREFIX} sample")
        return times

    def test_hits(self, hits):
        assert hits

    def test_times(self, times):
        assert times
