from flask import g, request
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    SelectField,
    TextAreaField,
    validators,
)


# =============================================================================
# Forms related to E/S/S (samples)
# =============================================================================


class SamplesCreateForm(FlaskForm):
    """Form for E/S/S creation and updating."""

    identifier = StringField(
        "Identifier",
        [validators.DataRequired(), validators.Length(min=4, max=25)],
    )  #: :meta hide-value:
    origin = StringField(
        "Origin", [validators.DataRequired(), validators.Length(min=3, max=25)]
    )  #: :meta hide-value:
    creator = StringField(
        "Produced by",
        [validators.DataRequired(), validators.Length(min=1, max=25)],
    )  #: :meta hide-value:
    short_dis = StringField(
        "Short Description",
        [validators.DataRequired(), validators.Length(min=1, max=50)],
    )  #: :meta hide-value:
    long_dis = TextAreaField("Long Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit E/S/S")  #: :meta hide-value:


class SamplesShare(FlaskForm):
    """Helper form to share sample with other users."""

    user = SelectField("User", coerce=int)  #: :meta hide-value:
    usershare = SubmitField("Share E/S/S with")  #: :meta hide-value:


class SamplesLocation(FlaskForm):
    """Helper form to change sample location."""

    loc = SelectField("Location", coerce=int)  #: :meta hide-value:
    location = SubmitField("E/S/S is at ")  #: :meta hide-value:


# =============================================================================
# Forms related to P/M/M (measurements)
# =============================================================================


class ConfigCreateMType(FlaskForm):
    """Define a new measurement type."""

    mtype = StringField(
        "Name", [validators.DataRequired(), validators.Length(min=3, max=49)]
    )  #: :meta hide-value:
    submit = SubmitField("Submit")  #: :meta hide-value:


class MeasurementCreateForm(FlaskForm):
    """Create and update a measurement."""

    short_dis = StringField(
        "Title", [validators.DataRequired(), validators.Length(min=1, max=50)]
    )  #: :meta hide-value:
    creator = StringField(
        "Made by", [validators.DataRequired(), validators.Length(min=1, max=25)]
    )  #: :meta hide-value:
    mtype = SelectField("P/P/M Type", coerce=int)  #: :meta hide-value:
    instrument_id = SelectField("Instrument", coerce=int)  #: :meta hide-value:
    long_dis = TextAreaField("Long Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit P/P/M")  #: :meta hide-value:


# =============================================================================
# Forms related to Reports
# =============================================================================


class ReportCreateForm(FlaskForm):
    """Create and update a report form."""

    title = StringField(
        "Title", [validators.DataRequired(), validators.Length(min=1, max=50)]
    )  #: :meta hide-value:
    long_dis = TextAreaField("Long Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit Report")  #: :meta hide-value:
