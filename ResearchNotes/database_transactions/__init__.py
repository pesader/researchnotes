# -*- coding: utf-8 -*-
"""
Module for database transactions split into different files following the naming convention of the
database module.

Every complex database transaction like updating and creating records goes here.
Queries and "one-liner" transactions should be left out of it.

To avoid conflict with existing functions and make them easy to
identify this module will be imported completely as dbt.

We will import all functions to __initi__ to maker we them import to the other sub-modules without changes.
"""

from .basics import (
    Transaction,
    activate_user,
    deactivate_user,
    create_user,
    create_admin,
    create_location,
    create_mtype,
    create_index,
    create_roles,
    update_user,
    update_session_metadata,
    rename_group,
    delete_group,
)

from .documents import create_document, update_document

from .ess_ppm_report import (
    create_sample,
    update_sample,
    create_report,
    update_report,
    create_measurement,
    update_measurement,
    create_template_sample,
    create_template_measurement,
    update_template_sample,
    update_template_measurement,
)

from .instrument_journal import (
    create_instrument,
    create_etype,
    create_instrumentation_journal_entry,
    update_instrument,
    update_etype,
    create_template_instrument,
    create_template_entry,
    activate_instrument,
    deactivate_instrument,
    update_default_instrument_templates,
    update_template_instrument,
    update_template_entry,
    update_instrumentation_journal_entry,
)
