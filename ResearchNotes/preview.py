# -*- coding: utf-8 -*-
"""
Preview module and functions.

All preview functionality should go here. Deals with identifying file type and
deliver a static preview of files in the upload directory.

To send files, the function file.send_preview should be used to check if user signed in.

Attention, this blueprint will deliver back material that users upload.

"""
import os

import csv

from typing import Union

import textwrap

from flask import (
    Blueprint,
    request,
    render_template,
    redirect,
    url_for,
    abort,
    current_app,
    g,
    flash,
    session as login_session,
)
from werkzeug.utils import safe_join, secure_filename
from werkzeug.wrappers.response import Response

from markdown import markdown

# from markdown.extensions.codehilite import CodeHilite

from ResearchNotes.auth import login_required

from ResearchNotes.files import uploaddir_path, make_image_url_list
from ResearchNotes.database import db, Documents
from ResearchNotes.samples import get_sample
from ResearchNotes.measurement import get_measurement
from ResearchNotes.report import get_report
from ResearchNotes.instruments import get_instrument
from ResearchNotes.entry import get_entry

bp = Blueprint("preview", __name__, url_prefix="/preview")


def make_info_dict(info: list) -> dict:
    """
    Create information dict to make preview menu.

    Parameters
    ----------
    info : list
        Information list with ids and dir_type information.

    Returns
    -------
    info_dict : dict
        Dict with the ids or None for returning.

    """
    dir_type = info[0]
    info_dict = {}

    if dir_type == "s":
        info_dict = {
            "type": "s",
            "sid": int(info[1]),
            "s_identifier": info[2],
            "mid": None,
            "rid": None,
            "iid": None,
            "eid": None,
        }
    elif dir_type == "m":
        info_dict = {
            "type": "m",
            "sid": int(info[2]),
            "s_identifier": info[3],
            "mid": int(info[1]),
            "rid": None,
            "iid": None,
            "eid": None,
        }
    elif dir_type == "r":
        info_dict = {
            "type": "r",
            "sid": int(info[2]),
            "s_identifier": info[3],
            "mid": None,
            "rid": int(info[1]),
            "iid": None,
            "eid": None,
        }
    elif dir_type == "i":
        info_dict = {
            "type": "i",
            "sid": None,
            "i_identifier": info[2],
            "iid": int(info[1]),
            "eid": None,
        }
    elif dir_type == "e":
        info_dict = {
            "type": "e",
            "sid": None,
            "i_identifier": info[3],
            "iid": int(info[2]),
            "eid": int(info[1]),
        }
    elif dir_type == "d":
        dlabel = db.session.get(Documents, info[1])
        info_dict = {
            "type": "d",
            "sid": None,
            "i_identifier": None,
            "dlabel": dlabel.label,
            "did": info[1],
        }
    else:
        abort(400, description="Tried to load unknown directory type")

    return info_dict


def redirect_home(info_dict: dict) -> Response:
    """
    Make the correct return link depending on the preview.

    This is mainly used at UniCode Exception to return to the original view.

    Parameters
    ----------
    info_dict: Dict

    Returns
    -------
    Response

    """
    if info_dict["type"] == "m":
        return redirect(url_for("measurements.measurement_view", mid=info_dict["mid"]))
    if info_dict["type"] == "r":
        return redirect(url_for("report.report_view", rid=info_dict["rid"]))
    if info_dict["type"] == "s":
        return redirect(url_for("samples.sample_view", sid=info_dict["sid"]))
    if info_dict["type"] == "i":
        return redirect(url_for("instruments.instrument_view", iid=info_dict["iid"]))
    if info_dict["type"] == "e":
        return redirect(url_for("entry.entry_view", eid=info_dict["eid"]))
    if info_dict["type"] == "d":
        return redirect(url_for("documents.show", label=info_dict["dlabel"]))

    return abort(400, description="Tried to load unknown directory type")


def make_breadcrumb_list(info_dict: dict) -> list[str]:

    url_list = []
    label = []

    if info_dict["type"] == "m":
        entry = get_measurement(info_dict["mid"])
        label = [entry.measurement_sample.identifier, entry.short_dis]
        url_list = [
            url_for("samples.sample_view", sid=info_dict["sid"]),
            url_for("measurements.measurement_view", mid=info_dict["mid"]),
        ]

    elif info_dict["type"] == "r":
        entry = get_report(info_dict["rid"])
        label = [entry.reports_sample.identifier, entry.report_measurement.short_dis, entry.title]
        url_list = [
            url_for("samples.sample_view", sid=info_dict["sid"]),
            url_for("measurements.measurement_view", mid=entry.measurement_id),
            url_for("report.report_view", rid=info_dict["rid"]),
        ]

    elif info_dict["type"] == "s":
        entry = get_sample(info_dict["sid"])
        label = [entry.identifier]
        url_list = [url_for("samples.sample_view", sid=info_dict["sid"])]

    elif info_dict["type"] == "i":
        entry = get_instrument(info_dict["iid"])
        label = ["Index", entry.identifier]
        url_list = [
            url_for("instruments.index"),
            url_for("instruments.instrument_view", iid=info_dict["iid"]),
        ]

    elif info_dict["type"] == "e":
        entry = get_entry(info_dict["eid"])
        label = ["Index", entry.instrument.identifier, entry.identifier]
        url_list = [
            url_for("instruments.index"),
            url_for("instruments.instrument_view", iid=info_dict["iid"]),
            url_for("entry.entry_view", eid=info_dict["eid"]),
        ]
    elif info_dict["type"] == "d":
        label = login_session["urls"]
        url_list = [url_for("documents.show", label=url) for url in login_session["urls"]]

    return zip(url_list, label)


@bp.app_template_filter("preview")
def preview_filter(file: str, info: list) -> str:
    """
    Filter to create the link to the right preview function.

    Parameters
    ----------
    file: str
        Filename with ending to create preview for.
    info: list.
        List of dir type and sample number, identifier as well as m_id or r_id.

    Returns
    -------
    url : str
        Link to the preview view.

    """
    match os.path.splitext(file)[1].lower():

        case ".md":
            url = url_for("preview.markdown_preview", info=info, file=file)
        case ".pdf":
            url = url_for("preview.pdf", info=info, file=file)
        case ".py":
            url = url_for("preview.python_code", info=info, file=file)
        case ".txt" | ".dat" | ".xy":
            url = url_for("preview.text_preview", info=info, file=file)
        case ".csv":
            url = url_for("preview.csv_preview", info=info, file=file)
        case ".gif" | ".jpg" | ".png" | ".svg" | ".jpeg":
            url = url_for("preview.image", info=info, file=file)
        case _:
            url = url_for("preview.nopreview", info=info, file=file)

    return url


@bp.route("/<list:info>/nopreview")
@login_required
def nopreview(info: list) -> str:
    """
    No preview view.

    View to show, when no preview is defined for the data type

    Returns
    -------
    str
        Renders the "no preview defined" template.

    """
    file = request.args.get("file")
    info_dict = make_info_dict(info)
    if file is None:
        abort(404, description=f"preview : nopreview : File None call to function from user {g.user}")

    return render_template(
        "preview/nopreview.html",
        url_list=make_breadcrumb_list(info_dict),
        info_dict=info_dict,
        file=secure_filename(file),
    )


@bp.route("/<list:info>/pdf")
@login_required
def pdf(info: list) -> str:
    """
    PDF Preview.

    Preview for pdf files mainly relying on the functionality of modern browsers.
    Be aware that we have defined a new static content directory for the files to
    be delivered to the user.

    Parameters
    ----------
    info : list
        Information list with ids and dir_type information..

    Returns
    -------
    str
        Renders the pdf preview template.

    """
    file = request.args.get("file")
    info_dict = make_info_dict(info)
    if file is None:
        abort(404, description=f"preview : pdf : File None call to function from user {g.user}")

    pre = uploaddir_path(info, only_sub_dir=True)
    pdf_file = url_for("files.send_preview", filename=safe_join(pre, file))

    return render_template(
        "preview/pdf.html",
        pdf=pdf_file,
        url_list=make_breadcrumb_list(info_dict),
        file=secure_filename(file),
        info_dict=info_dict,
    )


@bp.route("/<list:info>/markdown")
@login_required
def markdown_preview(info: list) -> Union[str, Response]:
    """
    Preview for all Markdown files.

    Parameters
    ----------
    info : list
        Information list with ids and dir_type information.

    Returns
    -------
    str|Response
        Renders the preview for Markdown text files.

    """
    file = secure_filename(request.args.get("file"))
    info_dict = make_info_dict(info)
    if file is None:
        abort(
            404,
            description=f"preview : markdown_preview : Call to function from user {g.user} invalid file arg",
        )

    path = uploaddir_path(info)

    try:
        with open(os.path.join(path, file), "r", encoding="utf-8") as input_file:
            body = input_file.read()
    except FileNotFoundError:
        abort(
            404,
            description=f" : markdown_preview : {g.user} tried to load {os.path.join(path, file)}",
        )
    except UnicodeError as error:
        flash(f"Reading the txt file gave an  error: {str(error)}", "alert-danger")
        return redirect_home(info_dict)
    return render_template(
        "preview/markdown.html",
        url_list=make_breadcrumb_list(info_dict),
        body=body,
        title=file,
        info_dict=info_dict,
        file=file,
    )


@bp.app_template_filter("markdown_python")
def python_markdown_filter(markdown_text: str) -> str:
    """
    Markdown python code filter.

    WE use the CodeHilite function of the Markdown module. It is just activated for this preview (we can
    debate, if we want to activate for everything) to make the Python code more readable. In theory, pygiments
    used by the CodeHilite can also be used for other language (if needed).


    Parameters
    ----------
    markdown_text : str
        Python code to be rendered to HTML.

    Returns
    -------
    str
        Python code set as HTML.

    """
    return markdown(
        markdown_text,
        extensions=["codehilite"],
        output_format="html",
    )


@bp.route("/<list:info>/python_code")
@login_required
def python_code(info: list) -> Union[str, Response]:
    """
    Preview for all python files.

    Parameters
    ----------
    info : List
        List of ids and dir_type.

    Returns
    -------
    str|Response
        Renders the python preview template.

    """
    file = secure_filename(request.args.get("file"))
    info_dict = make_info_dict(info)
    if file is None:
        abort(
            404,
            description=f"preview : python_code : Call to function from user {g.user} invalid file arg",
        )

    path = uploaddir_path(info)

    try:
        with open(os.path.join(path, file), "r", encoding="utf-8") as input_file:
            rbody = input_file.read()
    except FileNotFoundError:
        abort(
            404,
            description=f" : python_preview : {g.user} tried to load {os.path.join(path, file)}",
        )
    except UnicodeError as error:
        flash(f"Reading the txt file gave an  error: {str(error)}", "alert-danger")
        return redirect_home(info_dict)
    body = textwrap.indent(":::python \n" + rbody, "    ")
    return render_template(
        "preview/python.html",
        url_list=make_breadcrumb_list(info_dict),
        body=body,
        title=file,
        info_dict=info_dict,
        file=file,
    )


@bp.route("/<list:info>/text_preview")
@login_required
def text_preview(info: list) -> str | Response:
    """
    Preview for text files.

    We will cut the text after 2000 characters in the view. We read the first
    3000 characters or bytes of the file. The view will ensure that we have a
    sting and do a truncate at this size.

    Parameters
    ----------
    info : list
        List of ids and dir_type. To be passed to uploaddir_path.

    Returns
    -------
    str|Response
        Renders the text preview template.

    """
    file = secure_filename(request.args.get("file"))
    info_dict = make_info_dict(info)
    if file is None:
        abort(
            404,
            description=f"preview : text_preview : Call to function from user {g.user} invalid file arg",
        )

    path = uploaddir_path(info)

    try:
        with open(os.path.join(path, file), "r", encoding="utf-8") as input_file:
            body = input_file.read(3000)
    except FileNotFoundError:
        abort(
            404,
            description=f" : text_preview : {g.user} tried to load {safe_join(path, file)}",
        )
    except UnicodeError as error:
        flash(f"Reading the txt file gave an  error: {str(error)}", "alert-danger")
        return redirect_home(info_dict)
    return render_template(
        "preview/text.html",
        url_list=make_breadcrumb_list(info_dict),
        body=body,
        title=file,
        info_dict=info_dict,
        file=file,
    )


@bp.route("/<list:info>/image")
@login_required
def image(info: list) -> str:
    """
    Image preview.

    Preview for all image files making using a Bootstrap Carousel to display all
    images in the folder. We get a specific file, but we display all images not
    starting with the ask file (one would need to
    do an ordering of the list - I guess)

    Parameters
    ----------
    info : list
        List of ids and dir_type.

    Returns
    -------
    str
        Renders the preview template for images.

    """
    file = request.args.get("file")

    pre = uploaddir_path(info, only_sub_dir=True)
    info_dict = make_info_dict(info)
    if file is None:
        abort(404, description=f"preview : image : Call to function from user {g.user} without file arg")

    path = os.path.join(current_app.config["UPLOAD_PATH"], pre)

    urls = make_image_url_list(path, pre, file, zip_files=True)

    return render_template(
        "preview/image.html",
        imgs=urls,
        url_list=make_breadcrumb_list(info_dict),
        info=info,
        info_dict=info_dict,
        file=secure_filename(file),
    )


@bp.route("/<list:info>/csv_preview")
@login_required
def csv_preview(info: list) -> str | Response:
    """
    Preview for .csv (and maybe excel) files.

    We use the python csv liberal to guess first the type of csv and read it in a dict.
    The dict is transformed into a markdown string, and we render the sting with the
    markdown template.

    Parameters
    ----------
    info : list
        List of ids and dir_type.

    Returns
    -------
    str|Response
        Calls the csv preview template.

    """
    file = secure_filename(request.args.get("file"))
    path = uploaddir_path(info)
    info_dict = make_info_dict(info)
    if file is None:
        abort(
            404, description=f"preview : csv_preview : Call function for user {g.user} invalid file arg."
        )

    try:
        with open(os.path.join(path, file), "r", encoding="utf-8") as csvfile:
            try:
                dialect = csv.Sniffer().sniff(csvfile.read(1024))
            except csv.Error as error:
                flash(f"Reading the csv file gave an error: {str(error)}", "alert-danger")
                return redirect_home(info_dict)

            current_app.logger.debug(f"Detected dialect {dialect()} in csv file {file}")

            csvfile.seek(0)
            csv_dict = csv.DictReader(csvfile, dialect=dialect)

            list_of_rows = list(csv_dict)
    except FileNotFoundError:
        abort(
            404,
            description=f" : csv_preview : {g.user} tried to load {os.path.join(path, file)}",
        )
    except UnicodeError as error:
        flash(f"Reading the txt file gave an  error: {str(error)}", "alert-danger")
        return redirect_home(info_dict)

    headers = list(list_of_rows[0].keys())

    md_string = " | "
    for header in headers:
        md_string += header + " |"
    md_string += "\n |"
    for _ in range(len(headers)):
        md_string += "--- | "
    md_string += "\n"
    for row in list_of_rows:
        md_string += " | "
        for header in headers:
            md_string += row[header] + " | "
        md_string += "\n"
    #
    # Render the template with the md_string as body
    #
    return render_template(
        "preview/csv.html",
        url_list=make_breadcrumb_list(info_dict),
        body=md_string,
        title=file,
        info_dict=info_dict,
        file=file,
    )
