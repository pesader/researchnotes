# -*- coding: utf-8 -*-
"""
All template related functions/views go here.

Templates are easy ways to create new E/S/S, P/P/M or report entries following
a certain style (e.g. having pre-defined tables for data insertion etc.).
"""
# from typing import List

import typing

from werkzeug.wrappers.response import Response

from flask import (
    Blueprint,
    flash,
    redirect,
    url_for,
    abort,
    g,
    render_template,
    current_app,
)

from ResearchNotes.database import (
    TemplateInstrumentationJournalEntry,
    db,
    MeasurementType,
    TemplateSamples,
    TemplateMeasurements,
    TemplateInstrument,
    EntryType,
)

from ResearchNotes.form import (
    TemplateInstrumentationJournalEntryCreateForm,
    UseTemplate,
    TemplateSamplesCreateForm,
    TemplateMeasurementCreateForm,
    TemplateInstrumentCreateForm,
)

from ResearchNotes.samples import get_sample
from ResearchNotes.measurement import get_measurement
from ResearchNotes.instruments import get_instrument
from ResearchNotes.entry import get_entry
from ResearchNotes.auth import login_required
import ResearchNotes.database_transactions as dbt

from ResearchNotes.url_security import token_decode

bp = Blueprint("template", __name__, url_prefix="/template")


@bp.route("/index")
@login_required
def index() -> str:
    """
    Index page of the templates for ESSs or PMMs.

    List all templates available for user.

    Returns
    -------
    str
        Renders index template.

    """
    sample_temp = db.session.execute(
        db.select(TemplateSamples).filter_by(creator_id=g.user.id).order_by("tname")
    ).scalars()

    measurement_temp = db.session.execute(
        db.select(TemplateMeasurements).filter_by(creator_id=g.user.id).order_by("tname")
    ).scalars()

    instrument_temp = [
        t
        for t in db.session.execute(db.select(TemplateInstrument).order_by("tname")).scalars()
        if (t.group_id != 1 and t.group_id == g.user.group_id) or (t.creator_id == g.user.id)
    ]

    entry_temp = [
        t
        for t in db.session.execute(
            db.select(TemplateInstrumentationJournalEntry).order_by("tname")
        ).scalars()
        if (t.group_id != 1 and t.group_id == g.user.group_id) or (t.creator_id == g.user.id)
    ]

    return render_template(
        "template/index.html",
        sample=sample_temp,
        measurement=measurement_temp,
        instrument=instrument_temp,
        entry=entry_temp,
    )


#######################################################################################################
# Sample templates
#######################################################################################################


@bp.route("/template_from_sample/<int:s_id>")
@login_required
def create_template_from_sample(s_id: int) -> Response:
    """
    Create a tem,plate from an existing database entry.

    Either Sample or Measurement depending on what was given.

    Parameters
    ----------
    s_id : int
        Sample ID to be used as template.

    Returns
    -------
    Flask.Response
        Redirects to template creation form using the information provided to
        prefill form.

    """
    sample = get_sample(s_id)

    sample_template = TemplateSamples(
        tname=str(sample.identifier + " - Template"),
        identifier=sample.identifier,
        origin=sample.origin,
        short_dis=sample.short_dis,
        long_dis=sample.long_dis,
        creator_id=g.user.id,
        group_id=g.user.group_id,
    )

    with dbt.Transaction(db) as db_session:
        db_session.add(sample_template)
        flash(f"Created sample template based on {sample.identifier}", "alert-success")

    return redirect(url_for("samples.sample_view", sid=s_id))


@bp.route("/use_sample_template", methods=["POST"])
@login_required
def use_sample_temp() -> typing.Union[str, Response]:
    """
    Use ESS template for creation of new ESS.

    Returns
    -------
    str,Flask Response
        Renders template selection form or redirects to the creation form.

    """
    form = UseTemplate()
    form.template.choices = [
        (cg.id, cg.tname)
        for cg in db.session.execute(
            db.select(TemplateSamples).order_by("tname").filter_by(creator_id=g.user.id)
        ).scalars()
    ]
    what = "E/S/S"

    if form.validate_on_submit():
        return redirect(url_for("samples.create", tid=form.template.data))

    return render_template("template/chose.html", form=form, what=what)


@bp.route("/create_sample_template/<int:tid>", methods=("GET", "POST"))
@login_required
def create_sample_template(tid: int) -> typing.Union[str, Response]:
    """
    Create new template entry, if not exists. If exits use it to edit it.

    Parameters
    ----------
    tid : int
        Information what kind of template to create and ids.

    Returns
    -------
    str/ Flask.Response
        Render template create form or redirect to template view.

    """
    new = False
    temp = db.session.get(TemplateSamples, tid)
    #
    if temp is None:
        form = TemplateSamplesCreateForm()
        new = True
    else:
        if int(temp.creator_id) != g.user.id:
            abort(
                403,
                description=f"  template.create_sample_template : Authorization failure. {g.user} "
                + "tried to edit template {tid}",
            )
        form = TemplateSamplesCreateForm(obj=temp)

    #
    if form.validate_on_submit():
        if new:
            temp_info = {
                "tname": form.tname.data,
                "identifier": form.identifier.data,
                "origin": form.origin.data,
                "short_dis": form.short_dis.data,
                "long_dis": form.long_dis.data,
                "creator_id": g.user.id,
                "group_id": g.user.group_id,
            }
            dbt.create_template_sample(db, temp_info)
            flash("Template created", "alert-info")
        else:
            dbt.update_template_sample(
                db,
                temp,
                {
                    "tname": form.tname.data,
                    "short_dis": form.short_dis.data,
                    "long_dis": form.long_dis.data,
                    "identifier": form.identifier.data,
                    "origin": form.origin.data,
                },
            )
            flash(f"Template {temp.tname} updated", "alert-info")
        return redirect(url_for("template.index"))
    #
    return render_template("template/screate.html", form=form, temp=temp)


@bp.route("/delete_sample_template/<string:token>/")
@login_required
def delete_sample_template(token: str) -> Response:
    """
    Delete a template from the template database.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the sample template to delete

    Returns
    -------
    Flask.Response
        Redirect to template.index.

    """
    tid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    temp = db.get_or_404(
        TemplateSamples,
        tid,
        description=f" template.delete_sample_template : {g.user} tried to load sample template {tid}",
    )

    if int(temp.creator_id) != g.user.id:
        abort(
            403,
            description=f"  template.delete_sample : Authorization failure. {g.user} "
            + "tried to delete sample template {tid}",
        )

    if len(list(temp.instruments_default)) == 0:
        with dbt.Transaction(db) as db_session:
            db_session.delete(temp)
        flash(f"Template {temp.identifier} deleted", "alert-info")
    else:
        flash("Can not delete Template as it is default for one or more instruments.", "alert-warning")

    return redirect(url_for("template.index"))


#######################################################################################################
# Measurement templates
#######################################################################################################
@bp.route("/template_from_measurement/<int:m_id>")
@login_required
def create_template_from_measurement(m_id: int) -> Response:
    """
    Create a template from an existing database entry.

    Parameters
    ----------
    m_id : int
        Measurement ID to create template from.

    Returns
    -------
    Flask.Response
        Redirects to template creation form using the information provided to
        prefill form.

    """
    measurement = get_measurement(m_id)
    temp_info = {
        "tname": str(measurement.short_dis + "- Template"),
        "short_dis": measurement.short_dis,
        "long_dis": measurement.long_dis,
        "creator_id": g.user.id,
        "creator": measurement.creator,
        "mtype_id": measurement.mtype_id,
    }
    dbt.create_template_measurement(db, temp_info)
    flash(f"Created sample template based on {measurement.short_dis}", "alert-success")
    return redirect(url_for("measurements.measurement_view", mid=m_id))


@bp.route("/use_measurement_template/<int:s_id>", methods=["POST"])
@login_required
def use_measurement_temp(s_id: int) -> typing.Union[str, Response]:
    """
    Use template for creation of new entity.

    Function to let you choose the template to be used to create a PPM.

    Parameters
    ----------
    s_id : int
       Sample ID to base PPM template on (needed for redirect to PPM view back).

    Returns
    -------
    str,Flask Response
        Renders template selection form or redirects to the creation form.

    """
    form = UseTemplate()
    form.template.choices = [
        (cg.id, cg.tname)
        for cg in db.session.execute(
            db.select(TemplateMeasurements).order_by("tname").filter_by(creator_id=g.user.id)
        ).scalars()
    ]
    what = "P/P/M"
    if form.validate_on_submit():
        return redirect(url_for("measurements.create", ids=[s_id, form.template.data]))
    return render_template("template/chose.html", form=form, what=what)


@bp.route("/delete_measurement_template/<string:token>")
@login_required
def delete_measurement_template(token: str) -> Response:
    """
    Delete a template from the template database.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the measurement template to delete

    Returns
    -------
    Flask.Response
        Redirect to template.index.

    """
    tid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    temp = db.get_or_404(
        TemplateMeasurements,
        tid,
        description=f" template.delete_measurement : {g.user} tried to load not existing "
        + f"measurement template {tid}",
    )

    if int(temp.creator_id) != g.user.id:
        abort(
            403,
            description=f"  template.delete_measurement : Authorization failure. {g.user} "
            + "tried to delete template {tid}",
        )

    if len(list(temp.instruments_default)) == 0:
        with dbt.Transaction(db) as db_session:
            db_session.delete(temp)
        flash(f"Template {temp.tname} deleted", "alert-info")
    else:
        flash("Can not delete Template as it is default for one or more instruments.", "alert-warning")

    return redirect(url_for("template.index"))


@bp.route("/create_measurement_template/<int:tid>", methods=("GET", "POST"))
@login_required
def create_measurement_template(tid: int) -> typing.Union[str, Response]:
    """
    Create new template entry, if not exists. If exits use it to edit it.

    Parameters
    ----------
    tid : int
        Information what kind of template to create and ids.

    Returns
    -------
    str/ Flask.Response
        Render template create form or redirect to template view.

    """
    new = False
    temp = db.session(TemplateMeasurements, tid)

    if temp is None:
        form = TemplateMeasurementCreateForm()
        new = True
    else:
        if int(temp.creator_id) != g.user.id:
            abort(
                403,
                description=f"  template.create_measurement_template : Authorization failure. {g.user} "
                + "tried to edit template {tid}",
            )
        form = TemplateMeasurementCreateForm(obj=temp)

    form.mtype.choices = [
        (cg.id, cg.name)
        for cg in db.session.execute(
            db.select(MeasurementType).order_by("name").filter_by(group_id=g.user.group_id)
        ).scalars()
    ]
    form.mtype.choices.insert(
        0, (db.session.get(MeasurementType, 1).id, db.session.get(MeasurementType, 1).name)
    )
    form.instrument_id.choices = [
        (instrument.id, instrument.identifier)
        for instrument in (
            [i for i in g.user.group_member.owned_instruments if i.active]
            + [i for i in g.user.shared_instruments if i.active]
        )
    ]
    form.instrument_id.choices.insert(0, (0, "None/Other"))

    if form.validate_on_submit():
        if new:
            dbt.create_template_measurement(
                db,
                {
                    "tname": form.tname.data,
                    "short_dis": form.short_dis.data,
                    "creator": form.creator.data,
                    "mtype_id": form.mtype.data,
                    "long_dis": form.long_dis.data,
                    "creator_id": g.user.id,
                },
            )
            flash("Template created", "alert-info")
        else:
            dbt.update_template_measurement(
                db,
                temp,
                {
                    "tname": form.tname.data,
                    "creator": form.creator.data,
                    "mtype_id": form.mtype.data,
                    "long_dis": form.long_dis.data,
                    "short_dis": form.short_dis.data,
                },
            )
            flash(f"Template {temp.tname} updated", "alert-info")
        return redirect(url_for("template.index"))
    #
    return render_template("template/mcreate.html", form=form, temp=temp)


#######################################################################################################
# Instrument templates
#######################################################################################################


@bp.route("/template_from_instrument/<int:iid>")
@login_required
def create_template_from_instrument(iid: int) -> Response:
    """
    Create a template from an existing database entry.

    Parameters
    ----------
    iid : int
        Instrument ID to be used as template.

    Returns
    -------
    Flask.Response
        Redirects to template creation form using the information provided to
        prefill form.

    """
    instrument = get_instrument(iid)

    template_info = {
        "tname": str(instrument.identifier + " - Template"),
        "identifier": instrument.identifier,
        "description": instrument.description,
        "location_id": instrument.location_id,
        "group_id": g.user.group_id,
        "creator_id": g.user.id,
    }

    dbt.create_template_instrument(db, template_info)
    flash(f"Created instrument template based on {instrument.identifier}", "alert-success")
    return redirect(url_for("instruments.instrument_view", iid=iid))


@bp.route("/delete_instrument_template/<string:token>/")
@login_required
def delete_instrument_template(token: str) -> Response:
    """
    Delete an instrument template from the template database.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the instrument template to delete

    Returns
    -------
    Flask.Response
        Redirect to template.index.

    """
    tid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    template = db.get_or_404(
        TemplateInstrument,
        tid,
        description=f" template.delete_instrument_template : {g.user} "
        + f"tried to load instrument template {tid}",
    )

    if int(template.creator_id) != g.user.id:
        abort(
            403,
            description=f" template.delete_instrument_template : Authorization failure. {g.user} "
            + f"tried to delete instrument template {tid}",
        )

    with dbt.Transaction(db) as db_session:
        db_session.delete(template)
    flash(f"Template {template.identifier} deleted", "alert-info")
    return redirect(url_for("template.index"))


@bp.route("/create_instrument_template/<int:tid>", methods=("GET", "POST"))
@login_required
def create_instrument_template(tid: int) -> str | Response:
    """
    Create new template entry if it doesn't exist or updated if it does.

    Parameters
    ----------
    tid : int
        Information what kind of template to create and ids.

    Returns
    -------
    str/ Flask.Response
        Render template create form or redirect to template view.

    """
    new = False
    template = None

    if tid != 0:
        template = db.get_or_404(
            TemplateInstrument,
            tid,
            description=f" template.create_instrument_template : {g.user} "
            + f"tried to load instrument template {tid}",
        )

    if template is None:
        form = TemplateInstrumentCreateForm()
        new = True
    else:
        if int(template.group_id) != g.user.group_id:
            abort(
                403,
                description=" template.create_instrument_template: "
                + f"Authorization failure. {g.user} "
                + f"tried to edit template {tid}",
            )
        form = TemplateInstrumentCreateForm(obj=template)

    # form.location_id.choices = [
    #     (loc.id, loc.name)
    #     for loc in db.session.execute(
    #         db.select(Locations).order_by("name").filter_by(group_id=g.user.group_id)
    #     ).scalars()
    # ]

    if form.validate_on_submit():
        if new:
            dbt.create_template_instrument(
                db,
                {
                    "tname": form.tname.data,
                    "identifier": form.identifier.data,
                    "description": form.description.data,
                    # "location_id": form.location_id.data,
                    "group_id": g.user.group_id,
                    "creator_id": g.user.id,
                },
            )
            flash("Template created", "alert-info")
        else:
            dbt.update_template_instrument(
                db,
                template,
                {
                    "tname": form.tname.data,
                    "identifier": form.identifier.data,
                    "description": form.description.data,
                    # "location_id": form.location_id.data,
                },
            )
            flash(f"Template {template.identifier} updated", "alert-info")
        return redirect(url_for("template.index"))
    return render_template("template/icreate.html", form=form, template=template)


#######################################################################################################
# InstrumentationJournalEntry templates
#######################################################################################################


@bp.route("/template_from_entry/<int:eid>")
@login_required
def create_template_from_entry(eid: int) -> Response:
    """
    Create a template from an existing instrumentation journal entry

    Parameters
    ----------
    eid : int
        ID of Instrumentation Journal Entry to be used as template

    Returns
    -------
    Flask.Response
        Redirects to template creation form using the information provided to
        prefill form

    """
    entry = get_entry(eid)

    template_info = {
        "tname": str(entry.identifier + " - Template"),
        "identifier": entry.identifier,
        "description": entry.description,
        "creator_id": g.user.id,
        "group_id": g.user.group_id,
        "etype_id": entry.etype_id,
    }

    dbt.create_template_entry(db, template_info)
    flash(
        f"Created instrumentation journal entry template based on {entry.identifier}",
        "alert-success",
    )
    return redirect(url_for("entry.entry_view", eid=eid))


@bp.route("/delete_entry_template/<string:token>/")
@login_required
def delete_entry_template(token: str) -> Response:
    """
    Delete an instrumentation journal entry template

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the instrumentation journal entry
        template to delete

    Returns
    -------
    Flask.Response
        Redirect to template.index.

    """
    tid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)

    template = db.get_or_404(
        TemplateInstrumentationJournalEntry,
        tid,
        description=f" template.delete_entry_template : {g.user} "
        + f"tried to load instrumentation journal entry template {tid}",
    )

    if int(template.creator_id) != g.user.id:
        abort(
            403,
            description=f" template.delete_entry_template : Authorization failure. {g.user} "
            + f"tried to delete instrumentation journal entry template {tid}",
        )
    if len(list(template.instruments_default)) == 0:
        with dbt.Transaction(db) as db_session:
            db_session.delete(template)
        flash(f"Template {template.identifier} deleted", "alert-info")
    else:
        flash("Can not delete Template as it is default for one or more instruments.", "alert-warning")

    return redirect(url_for("template.index"))


@bp.route("/create_entry_template/<int:tid>", methods=("GET", "POST"))
@login_required
def create_entry_template(tid: int) -> str | Response:
    """
    Create new template entry if it doesn't exist or updated if it does.

    Parameters
    ----------
    tid : int
        Information what kind of template to create and ids.

    Returns
    -------
    str/ Flask.Response
        Render template create form or redirect to template view.

    """

    new = False
    template = None

    if tid != 0:
        template = db.get_or_404(
            TemplateInstrumentationJournalEntry,
            tid,
            description=f" template.create_entry_template : {g.user} "
            + f"tried to load instrumentation journal entry template {tid}",
        )
        if int(template.group_id) != g.user.group_id:
            abort(
                403,
                description=" template.create_entry_template: "
                + f"Authorization failure. {g.user} "
                + f"tried to edit template {tid}",
            )

        form = TemplateInstrumentationJournalEntryCreateForm(obj=template)
        etypes = db.session.get(
            EntryType,
            int(template.etype_id),
        )

        form.etype.choices = [(0, "None/Other")]
        if etypes:
            form.etype.choices = [(etype.id, etype.name) for etype in etypes.instrument.etypes]
            form.etype.choices.insert(0, (0, "None/Other"))

    else:
        form = TemplateInstrumentationJournalEntryCreateForm()
        form.etype.choices = [(0, "None/Other")]
        new = True

    if form.validate_on_submit():
        if new:
            dbt.create_template_entry(
                db,
                {
                    "tname": form.tname.data,
                    "identifier": form.identifier.data,
                    "description": form.description.data,
                    "creator_id": g.user.id,
                    "group_id": g.user.group_id,
                    "etype_id": form.etype.data,
                },
            )
            flash("Template created", "alert-info")
        else:
            dbt.update_template_entry(
                db,
                template,
                {
                    "tname": form.tname.data,
                    "identifier": form.identifier.data,
                    "description": form.description.data,
                    "etype_id": form.etype.data,
                },
            )
            flash(f"Template {template.identifier} updated", "alert-info")
        return redirect(url_for("template.index"))
    return render_template("template/ecreate.html", form=form, template=template)
