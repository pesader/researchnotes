The Experiment/Sample/Simulation (ESS) entry is the base of the organization scheme for Open ResearchNotes. 
Beside the documents, this is the place you will create and first start to include data. Unlike the documentation, which is 
intended for general information relevant for a larger audience (like your group), the ESS entry is specific for
one sample or experiment in your research.

### Create a ESS entry

The first step to work with Open ResearchNotes is to define or create an ESS entry - hence define your first experiment, sample or simulation. Basically, this is 
your base order entity and what you will actually do with this entry will become your next PPM entries. It is good to do a little thinking in before, what you 
want to put there and how you want to identify it later - the best is that the group defines some rules for this (** this is a supervisor task **).

New ESS entries are created by the new E/S/S icon that is found on the index page or the E/S/S menu page. Clicking it, will open a form that will ask for some information:

**Identifier:** A short group of letters and numerals to give an identification to the sample. It might depend on the project, the kind of sample (e.g. where it is been grown), 
the related simulation (e.g. the type of simulation) and so on. It is not a short description of the sample, but more a tag, ID or serial number that goes onto a sample box. 
It is more for internal use and identification then for publication. Special characters will be stripped form this for security reasons.

**Origin:** Marks the place the ESS comes from or where it is produced. It can be a vendor name, a collaborator or the machine of the group, the group name etc. This might be 
more important for a physical entity than for simulations or experiments.

**Produced by:** The name of the person who produced the ESS. The ESS entry will be owned by the person making the entry but the ESS might have a group or a 
different person who produced the actual physical object.

**Short description:** A 50 character string giving a short information about the ESS. This information will be later visible in the overview 
tables and so on and might be the main search item (beside the identifier).

**Long description:** A detailed description of the ESS. This part will accept formatting to the text and text size is some 50 kB (long).  It is the
field to store detailed information that might be relevant on the ESS level. The Markdown language (see toolbar and [help](markdown) allows for heading, tables, list and other formations.

On submitting the form, the ESS entry is created and shown to the user. Beside the information (now formatted into HTML), the ESS view covers some possibilities.

It should be pointed out that part of the fields are sensitized to prevent security breaches like SQL and HTML injection. Form the sample identifier all non alpha-numerical characters are removed (e.g. “+”, “-”, space etc).
By default, the long description is escaped and all HTML code will be converted to letters. Hence, HTML commands inside the long description will not work to preferred injection of JavaScript or other things.

### Editing EES entry

In the ESS view that is shown after sample creation or by clicking the sample identifier in one of the tables available on the index page, the all ESS page or the ESS index page 
allows editing the information allocated in the original form.

Hence, information can be updated or new information added along the sample life. Once files are uploaded - especially images are connected to the sample entry, they can be referred 
in the long description (see [Markdown](markdown) and files section).

### Template icon

You can use an existing ESS to create a template by clicking on this icon. Basically, all information except the creator will be transferred into a template. The template can
be edited in the template menu and adapted to your needs. On sample creation, there is a template bottom on the upper left corner that lets you pre-fill the ESS form with the information of the chosen template.

### Summarize (export your ESS information)

The summarize icon will create a Markdown file with all ESS, PPM and report information of this entry and make it available for preview and download. As Markdown 
files are text files with some formation hints, they can be easily converted into HTML, pdf or LaTeX files. Or they can be imported into a work processor.

### New P/P/M

A new Procedure, processing or measurement entry can be created by the P/P/M icon. It will be attached to the current ESS entry.

### Share E/S/S with

By choosing the name from the drop box you make the ESS entry and all its sub-entries (PPM and reports) available to this person. You can share ESS entries with anyone inside the 
Open ResearchNotes installation making information sharing easy between people. Shared persons can edit existing entries and  add new PPM or reports to the sample. Hence, some one else can carry out a 
PPM on the existing sample and add the information back

### E/S/S is at

Define the location the physical object (or your computer program) is found. In this way, you can keep track of the ESS location. _Entries_ for this drop down box can only be created by a _supervisor_ of the group.

### Dropzone

At the bottom of the ESS view is a Dropzone for files. Basically all files of interest can be attached there and will be uploaded to the server. We will do some filtering for certain 
file types. The maximum size of the file depends on the configuration of your webserver and the WebApp. The default is 100 MB.
The upload does not check for existing files and will overwrite files with the same file name. File names will be sanitized against directory transversal etc. 


### Associated Procedures/Processings/Measurements

Once you define a Procedures/Processing/Measurements (PPM), it will show up at this table. By clicking on the title, the information of the PPM entry will be shown (also available in the all PPM view of the ESS menu).

Furthermore, in the action menu, you can directly edit the PPM entry or delta it. **There is no undelete!**  Once deleted, the information and attached files are gone.

### Associated Files

Uploaded files will show up in the “Associated Files” table. There is a download bottom, which will create a zip of all files and offer it to download (this might timeout with large directories). 
Individual files can be downloaded, previewed or deleted. We have previews for images, python code, Markdown files, .dat and .txt files, and .csv files.

Finally, files can be **deleted and then are gone** from the server.

Image files will show in the preview there URL. With this, they can be included into the long description by using the `! []()` construction. Copy and paste the URL into the round 
brackets and give a description in the \[\]-brackets. 

A final code follows a pattern like: `![description](/file/send_preview/SampleXXX-identifier/MeasurementXXX/image.png)`

## ESS menu and view

The main part to work with ESS and related entries, is the ESS menu found in the menu bar. It will open a view, which shows the last five changed  ESS entries, the last five changed 
PPM and reports as well as the last five updated ESS shared with you. By default, this are ordered after update time with the latest change on top.

Furthermore, the menu offers some additional views:

### All E/S/S

View shows a table with all ESS of the current user. The table is orderable after Identifier, Short Description, Origin, Creation date, Update Date. It has a menu allowing you to 
edit and delete the ESS entry. Furthermore, the table has a search function allowing you to find entries more easily. 

### Shared ESS

View or page with two tables - the ESS you shared out to someone and ESS shared with you. Again, you can order and search these tables.

### Group ESS (Supervisor role only)

Supervisors can see all ESS entries by the group. The table will be showing identifier, Short Description, Origin, Owner (User, who created the entry), Creation and update date. The table 
is searchable and orderbal after these fields, but you cannot directly enter into the ESS entries.

### All P/P/M

View or page that shows all of the current users PPM entries in a searchable and ordable table. You can select to go to the entry, the ESS entry of the PPM or edit the PMM directly.

**Remark**
All tables are paginated with 25 entries and you can change this by clicking the menu on the left. Pagination marks are given at the bottom of the table.

## Sample series

Sometimes, you will have a series of samples that somehow belong together, e.g. you systematically changed a growth parameter and want to see a change of a sample's properties. Hence, 
it might make sense in this case to create a “Meta''-ESS, which is basically an experiment. In the P/M/M part, you would collect the changes from sample to sample regarding a behavior or analysis.
Maybe, you would add some simulation or calculator to describe the changes from sample to sample. Nevertheless, the original data should stay at the sample level as samples might be used later for 
a different study, which is independent from the current experiment/analysis you do.