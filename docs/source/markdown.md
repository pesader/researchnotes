# Markdown Guide

We follow the original Markdown implementation as provide by [Python-Markdown](https://python-markdown.github.io/). As implementations differ a bit from 
each other preview and end result might differ at some points. Also, commands are differing from implementation to implementation. As python module is close to
the original, some things made popular by GitHub or StackOverflow might not work. We try to be as close as possible to the most common implementations.

## Emphasis

~~~~
**bold**
*italics*
~~~~

## Headers

~~~~
# Big header
## Medium header
### Small header
#### Tiny header
~~~~

## Line

~~~~
--------
~~~~

## Lists
~~~~
* Generic list item
* Generic list item
* Generic list item

1. Numbered list item
2. Numbered list item
3. Numbered list item

~~~~
## Links
~~~~
[Text to display](http://www.example.com)
~~~~

## Images

To include images, you need to upload them and get their path. The easiest way is to look at them in the preview as
it displays the complete path to call. Copy that into the tag below.

~~~~
![tag_to_image](/file/send_preview/SampleXXXX-indentifier/MearurementXXXX/file_name)
~~~~
It is possible to change the size of the image by adding a \{\} part to the image.

~~~
![tag_to_image](/file/send_preview/SampleXXXX-indentifier/MearurementXXXX/file_name){width=50%}
~~~

will reduce (or stretch) the image to a relative width of 50% of the page.

## Quotes


> This is a quote.  
> It can span multiple lines!


## Tables

~~~~

| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| John     | Doe      | Male     |
| Mary     | Smith    | Female   |

~~~~
Or without aligning the columns...

~~~~

| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| John | Doe | Male |
| Mary | Smith | Female |

~~~~

## Displaying code

~~~~
`if ==1: pass`
~~~~

Or spanning multiple lines...

```
~~~~
def useless():
	pass
~~~~
```
