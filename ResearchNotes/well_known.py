# -*- coding: utf-8 -*-
"""
Well_known module of the program.

Defines some .well_known URLs and redirects to some other views.
"""

# from typing import Union

from werkzeug.wrappers.response import Response

from flask import (
    Blueprint,
    redirect,
    url_for,
)

from ResearchNotes.auth import login_required

bp = Blueprint("well_known", __name__, url_prefix="/.well-known")


@bp.route("/change-password")
@login_required
def change_password() -> Response:
    """Redirects to password change view."""
    return redirect(url_for("conf.changepasswd"))
