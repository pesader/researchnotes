from typing import Final, Generator
import pytest
from werkzeug.utils import secure_filename

from ResearchNotes import create_app, db
from ResearchNotes.database.basics import (
    User,
    Locations,
)

from ResearchNotes.database.ess_ppm_report import (
    Samples,
    TemplateSamples,
    Measurements,
    MeasurementType,
    TemplateMeasurements,
    Reports,
)

from ResearchNotes.database.instrument_journal import (
    EntryType,
    Instrument,
)

import ResearchNotes.database_transactions as dbt

TEST_USER_ID: Final[int] = 2
TEST_USER_ROLE_ID: Final[int] = 2 # Supervisor
TEST_USER_GROUP_ID: Final[int] = 1


def pytest_collection_modifyitems(items):
    for item in items:
        if "database" in item.nodeid:
            item.add_marker(pytest.mark.backend)
        elif "content" in item.nodeid:
            item.add_marker(pytest.mark.frontend)
        elif "alert" in item.nodeid:
            item.add_marker(pytest.mark.frontend)
        elif "success" in item.nodeid:
            item.add_marker(pytest.mark.backend)
            item.add_marker(pytest.mark.frontend)


@pytest.fixture(scope="class")
def make_info() -> Generator:
    def _make_info(model: db.Model, **kwargs):

        # default values for each model
        info: dict[str, str] = {}

        if model == User:
            info = {
                "name": "test user username",
                "UserName": "test user fullname",
                "email": "test user email",
                "password": "test user password",
                "role_id": f"{TEST_USER_ROLE_ID}",
                "group_id": f"{TEST_USER_GROUP_ID}",
            }

        elif model == Locations:
            info = {
                "name": "test location name",
                "group_id": f"{TEST_USER_GROUP_ID}",
            }

        elif model == MeasurementType:
            info = {
                "name": "test mtype name",
                "group_id": f"{TEST_USER_GROUP_ID}",
            }

        elif model == Samples:
            info = {
                "creator_id": f"{TEST_USER_ID}",
                "group_id": f"{TEST_USER_GROUP_ID}",
                "identifier": "test_sample_identifier",
                "origin": "test sample origin",
                "short_dis": "test sample short description",
                "long_dis": "test sample long description",
                "creator": "test sample creator",
            }

        elif model == Measurements:
            info = {
                "short_dis": "test short description",
                "long_dis": "test long description",
                "creator": "test creator",
                "creator_id": f"{TEST_USER_ID}",
                "instrument_id": "1",
                "sample_id": "1",
                "mtype_id": "1",
            }

        elif model == Reports:
            info = {
                "title": "test report title",
                "long_dis": "test report long_dis",
                "creator": "test report creator",
                "creator_id": f"{TEST_USER_ID}",
                "sample_id": "1",
                "measurement_id": "1",
            }

        elif model == TemplateSamples:
            info = {
                "tname": "test sample template name",
                "identifier": "test sample template identifier",
                "origin": "test sample template origin",
                "short_dis": "test sample template short_dis",
                "long_dis": "test sample template long_dis",
                "creator_id": f"{TEST_USER_ID}",
                "group_id": f"{TEST_USER_GROUP_ID}",
            }

        elif model == TemplateMeasurements:
            info = {
                "tname": "test measurement template tname",
                "short_dis": "test measurement template short_dis",
                "long_dis": "test measurement template long_dis",
                "creator": "test measurement creator",
                "creator_id": f"{TEST_USER_ID}",
                "mtype_id": 1,
            }

        elif model == Instrument:
            info = {
                "identifier": "test instrument identifier",
                "description": "test instrument description",
                "location_id": "1",
                "creator_id": f"{TEST_USER_ID}",
                "group_id": f"{TEST_USER_GROUP_ID}",
            }

        elif model == EntryType:
            info = {
                "name": "test entry type name",
                "instrument_id": 1,
            }

        # allows overriding the default key-value pairs
        for key, val in kwargs.items():
            if key in info:
                info[key] = val
        return info

    yield _make_info


@pytest.fixture(scope="class")
def app(request, make_info):
    if "search" in request.keywords:
        app = create_app(testing=True, testing_search=True)
    else:
        app = create_app(testing=True)

    app.config["WTF_CSRF_ENABLED"] = False
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate_db(
            user_info=make_info(User),
            location_info=make_info(Locations),
            mtype_info=make_info(MeasurementType),
            sample_info=make_info(Samples),
            measurement_info=make_info(Measurements),
            report_info=make_info(Reports),
            template_sample_info=make_info(TemplateSamples),
            template_measurement_info=make_info(TemplateMeasurements),
            instrument_info=make_info(Instrument),
            etype_info=make_info(EntryType),
        )
        yield app


def populate_db(
    user_info,
    location_info,
    mtype_info,
    sample_info,
    measurement_info,
    report_info,
    template_sample_info,
    template_measurement_info,
    instrument_info,
    etype_info,
):
    # database entries in common with real instance
    dbt.create_roles(db)
    dbt.create_admin(db)

    # database entries specific to testing instance
    dbt.create_user(db, user_info)
    dbt.create_location(db, location_info)
    dbt.create_mtype(db, mtype_info)
    dbt.create_sample(db, sample_info)
    dbt.create_instrument(db, instrument_info)
    dbt.create_measurement(db, measurement_info)
    dbt.create_report(db, report_info)
    dbt.create_template_sample(db, template_sample_info)
    dbt.create_template_measurement(db, template_measurement_info)
    dbt.create_etype(db, etype_info)

    # additional user to test sharing
    dbt.create_user(
        db,
        {
            "name": "share user username",
            "UserName": "share user fullname",
            "email": "share user email",
            "password": "share user password",
            "role_id": 3, # Student
            "group_id": f"{TEST_USER_GROUP_ID}",
        },
    )


@pytest.fixture(scope="class")
def client(app):
    yield app.test_client()


@pytest.fixture(scope="class", autouse=True)
def login(request, client, make_info):
    # skip tests marked with nologin
    if "nologin" in request.keywords:
        return
    user_info = make_info(User)
    client.post(
        "/auth/login",
        data={
            "username": user_info["name"],
            "password": user_info["password"],
        },
    )


@pytest.fixture(scope="class")
def default_test_user(make_info) -> User:
    user_info = make_info(User)
    return db.session.execute(db.select(User).filter_by(role_id=user_info["role_id"])).scalar()


@pytest.fixture(scope="class")
def share_test_user() -> User:
    return db.session.execute(db.select(User).filter_by(name="share user username")).scalar()


@pytest.fixture(scope="class")
def default_test_location(app, make_info) -> Locations:
    location_info = make_info(Locations)
    return db.session.execute(db.select(Locations).filter_by(name=location_info["name"])).scalar()


@pytest.fixture(scope="class")
def default_test_mtype(app, make_info) -> MeasurementType:
    mtype_info = make_info(MeasurementType)
    return db.session.execute(db.select(MeasurementType).filter_by(name=mtype_info["name"])).scalar()


@pytest.fixture(scope="class")
def default_test_sample(app, make_info) -> Samples:
    sample_info = make_info(Samples)
    return db.session.execute(
        db.select(Samples).filter_by(identifier=sample_info["identifier"])
    ).scalar()


@pytest.fixture(scope="class")
def default_test_measurement(app, make_info) -> Measurements:
    measurement_info = make_info(Measurements)
    return db.session.execute(
        db.select(Measurements).filter_by(short_dis=measurement_info["short_dis"])
    ).scalar()


@pytest.fixture(scope="class")
def default_test_report(app, make_info) -> Reports:
    measurement_info = make_info(Reports)
    return db.session.execute(db.select(Reports).filter_by(title=measurement_info["title"])).scalar()


@pytest.fixture(scope="class")
def default_test_template_sample(app, make_info) -> TemplateSamples:
    temp_info = make_info(TemplateSamples)
    return db.session.execute(db.select(TemplateSamples).filter_by(tname=temp_info["tname"])).scalar()


@pytest.fixture(scope="class")
def default_test_template_measurement(app, make_info) -> TemplateMeasurements:
    temp_info = make_info(TemplateMeasurements)
    return db.session.execute(
        db.select(TemplateMeasurements).filter_by(tname=temp_info["tname"])
    ).scalar()


@pytest.fixture(scope="class")
def default_test_instrument(app, make_info) -> Instrument:
    instrument_info = make_info(Instrument)
    return db.session.execute(
        db.select(Instrument).filter_by(identifier=secure_filename(instrument_info["identifier"]))
    ).scalar()


@pytest.fixture(scope="class")
def default_test_etype(app, make_info) -> EntryType:
    etype_info = make_info(EntryType)
    return db.session.execute(
        db.select(EntryType).filter_by(name=etype_info["name"])
    ).scalar()

@pytest.fixture(scope="class")
def html(response) -> str:
    return response.get_data(as_text=True)
