from flask import current_app, g, url_for
import pytest

from ResearchNotes import db
from ResearchNotes.database import (
    MeasurementType,
    Measurements,
    Instrument,
)
from ResearchNotes.url_security import safe_url_for

pytestmark = pytest.mark.measurement


class TestMeasurementCreateForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_sample):
        with app.test_request_context():
            return client.get(url_for("measurements.create", ids=[default_test_sample.id, 0]))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible content
        assert "New Measurement" in html
        assert "Title" in html
        assert "P/P/M Type" in html
        assert "Made by" in html
        assert "Long Description" in html
        # html tags
        assert 'name="short_dis"' in html
        assert 'name="long_dis"' in html
        assert 'name="mtype"' in html
        assert 'name="creator"' in html


class TestMeasurementCreate:
    new_measurement_info: dict[str, str] = {
        "mtype": "1",
        "instrument_id": "1",
        "short_dis": "new measurement short description",
        "long_dis": "new measurement long description",
        "creator": "new measurement creator",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_sample):
        with app.test_request_context():
            return client.post(
                url_for("measurements.create", ids=[default_test_sample.id, 0]),
                data={
                    "mtype": self.new_measurement_info["mtype"],
                    "short_dis": self.new_measurement_info["short_dis"],
                    "long_dis": self.new_measurement_info["long_dis"],
                    "creator": self.new_measurement_info["creator"],
                    "instrument_id": self.new_measurement_info["instrument_id"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def new_measurement(self):
        return db.session.execute(
            db.select(Measurements).filter_by(short_dis=self.new_measurement_info["short_dis"])
        ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Description" in html
        assert "Hide/Show" in html
        assert "inserted/updated" in html
        assert "Made with instrument" in html

    def test_dynamic_content(self, html, default_test_instrument):
        assert db.get_or_404(Instrument, self.new_measurement_info["instrument_id"]).identifier in html

    def test_alert(self, html):
        assert f"P/P/M {self.new_measurement_info['short_dis']} created" in html
        assert "alert-info" in html

    def test_database(self, new_measurement):
        assert new_measurement is not None


class TestMeasumentUpdateForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_measurement):
        with app.test_request_context():
            return client.get(url_for("measurements.update", mid=default_test_measurement.id))

    @pytest.fixture(scope="class")
    def mtype_name(self, default_test_measurement):
        return db.session.get(MeasurementType, default_test_measurement.mtype_id).name

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible content
        assert "Title" in html
        assert "P/P/M Type" in html
        assert "Made by" in html
        assert "Long Description" in html
        # html tags
        assert 'name="short_dis"' in html
        assert 'name="long_dis"' in html
        assert 'name="mtype"' in html
        assert 'name="creator"' in html

    def test_dynamic_content(self, html, default_test_measurement, mtype_name):
        assert f"Edit Measurement {default_test_measurement.short_dis}" in html
        assert default_test_measurement.long_dis in html
        assert default_test_measurement.creator in html
        assert mtype_name in html


class TestMeasumentUpdate:
    updated_measurement_info: dict[str, str] = {
        "mtype": "1",
        "short_dis": "updated short description",
        "long_dis": "updated long description",
        "creator": "updated creator",
        "instrument_id": "0",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_measurement):
        with app.test_request_context():
            return client.post(
                url_for("measurements.update", mid=default_test_measurement.id),
                data={
                    "creator": self.updated_measurement_info["creator"],
                    "short_dis": self.updated_measurement_info["short_dis"],
                    "long_dis": self.updated_measurement_info["long_dis"],
                    "mtype": self.updated_measurement_info["mtype"],
                    "instrument_id": self.updated_measurement_info["instrument_id"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def mtype_name(self):
        return db.session.get(MeasurementType, self.updated_measurement_info["mtype"]).name

    @pytest.fixture(scope="class")
    def updated_measurement(self):
        return db.session.execute(
            db.select(Measurements).filter_by(short_dis=self.updated_measurement_info["short_dis"])
        ).scalars()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Description" in html
        assert "Hide/Show" in html
        assert "inserted/updated" in html

    def test_dynamic_content(self, html, mtype_name):
        assert self.updated_measurement_info["creator"] in html
        assert self.updated_measurement_info["short_dis"] in html
        assert self.updated_measurement_info["long_dis"] in html
        assert mtype_name in html

    def test_alert(self, html):
        assert "P/P/M information updated" in html
        assert "alert-info" in html

    def test_database(self, updated_measurement):
        assert updated_measurement is not None


class TestMeasumentDelete:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_measurement):
        with app.test_request_context():
            # NOTE: g.salt is only available on certain views
            client.post(
                url_for("measurements.measurement_view", mid=default_test_measurement.id),
                follow_redirects=True,
            )
            url = safe_url_for(
                default_test_measurement.id,
                "measurements.delete",
                current_app.config["SEC_SESSION_KEY"],
                g.salt,
            )
            return client.get(url, follow_redirects=True)

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Long Description" in html

    def test_dynamic_content(self, app, html, default_test_measurement):
        with app.test_request_context():
            assert (
                url_for(
                    "measurements.measurement_view",
                    mid=default_test_measurement.id,
                )
                not in html
            )

    def test_alert(self, html, default_test_measurement):
        assert f"Deleted measurement {default_test_measurement.short_dis}" in html
        assert "alert-info" in html

    def test_database(self, default_test_measurement):
        assert db.session.get(Measurements, default_test_measurement.id) is None
