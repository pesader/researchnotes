# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

from unittest.mock import MagicMock


# sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(1, os.path.abspath(r"../../"))

print(sys.path)


# -- Project information -----------------------------------------------------

project = "Open ResearchNotes"
copyright = "2020-2023, Open Research Developers"
author = "Ch. Deneke"
release = "1.3.1"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_parser",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.githubpages",
    "sphinx.ext.viewcode",
    "sphinx.ext.autosectionlabel",
]

source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

html_logo = "logo.svg"
html_theme_options = {
    "logo_only": False,
    "display_version": True,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# --------------------Autodoc options-------------------------------------

# sys.modules["werkzeug"] = MagicMock()
# sys.modules["flask"] = MagicMock()
# sys.modules["flask-sqlalchemy"] = MagicMock()
# sys.modules["flask-migrate"] = MagicMock()
# sys.modules["flask-flatpages"] = MagicMock()
# sys.modules["flask-wtf"] = MagicMock()
# sys.modules["wtforms"] = MagicMock()
# sys.modules["sqlalchemy"] = MagicMock()
# sys.modules["sqlalchemy.engine"] = MagicMock()
# sys.modules["sqlalchemy.engine.strategies"] = MagicMock()
# sys.modules["sqlalchemy.ext"] = MagicMock()
# sys.modules["sqlalchemy.ext.compiler"] = MagicMock()
# sys.modules["sqlalchemy.schema"] = MagicMock()
# sys.modules["sqlalchemy.sql"] = MagicMock()
# sys.modules["sqlalchemy.sql.base"] = MagicMock()
# sys.modules["sqlalchemy.sql.elements"] = MagicMock()
# sys.modules["sqlalchemy.util"] = MagicMock()
# sys.modules["sqlalchemy.util.compat"] = MagicMock()
# sys.modules["bleach"] = MagicMock()
# # sys.modules["markupsafe"] = MagicMock()

# autodock_mock_imports = [
#     "flask-migrate",
#     "flask-flatpages",
#     "flask-wtf",
#     "pygments",
#     "python-dotenv",
#     "pillow",
#     "wtforms",
#     "sqlalchemy",
#     "bleach",
#     "markupsafe",
# ]

autosectionlabel_prefix_document = True

autodoc_member_order = "bysource"

napoleon_google_docstring = False
napoleon_use_param = False
napoleon_use_ivar = True
