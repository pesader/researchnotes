from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    SelectField,
    TextAreaField,
    validators,
)


# =============================================================================
# Forms related to Instruments
# =============================================================================


class ChooseDefaultTemplatesForm(FlaskForm):
    """Form to choose default templates for an instrument"""

    ess_template = SelectField("Default E/S/S template", coerce=int)  #: :meta hide-value:
    ppm_template = SelectField("Default P/P/M template", coerce=int)  #: :meta hide-value:
    entry_template = SelectField("Default journal entry template", coerce=int)  #: :meta hide-value:
    save = SubmitField("Save default templates")  #: :meta hide-value:


class InstrumentCreate(FlaskForm):
    """Create and update an Instrument record"""

    identifier = StringField(
        "Identifier",
        [validators.DataRequired(), validators.Length(min=4, max=60)],
    )  #: :meta hide-value:
    location_id = SelectField("Location", coerce=int)  #: :meta hide-value:
    description = TextAreaField("Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit instrument")  #: :meta hide-value:


class InstrumentShare(FlaskForm):
    """Share instrument with guest users"""

    user = SelectField("User", coerce=int)  #: :meta hide-value:
    submit = SubmitField("Share instrument with")  #: :meta hide-value:


# =============================================================================
# Forms related to Instrumentation Journal Entries
# =============================================================================


class InstrumentationJournalEntryCreate(FlaskForm):
    """Create or update an Instrumentation Journal Entry"""

    identifier = StringField(
        "Title",
        [validators.DataRequired(), validators.Length(min=4, max=60)],
    )  #: :meta hide-value:
    description = TextAreaField("Description", [validators.DataRequired()])  #: :meta hide-value:
    etype = SelectField("Type", coerce=int)  #: :meta hide-value:
    submit = SubmitField("Submit journal entry")  #: :meta hide-value:
