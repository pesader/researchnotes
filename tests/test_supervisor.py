import unittest

from flask import url_for, g, current_app
from ResearchNotes import create_app, db
from ResearchNotes.database.basics import Locations
from ResearchNotes.database.ess_ppm_report import MeasurementType

import ResearchNotes.database_transactions as dbt
from ResearchNotes.url_security import safe_url_for

# from ResearchNotes.database import Samples, Measurements, Reports
# from ResearchNotes.url_security import safe_url_for


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(testing=True)
        self.app.config["WTF_CSRF_ENABLED"] = False  # no CSRF during tests
        self.appctx = self.app.app_context()
        self.appctx.push()
        # db.create_all()
        self.populate_db()
        # Needed to use context dependent functions like url_for, g, etc.
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def tearDown(self):
        db.drop_all()
        self.appctx.pop()
        self.app = None
        self.appctx = None
        self.client = None

    def populate_db(self):
        db.create_all()
        dbt.create_roles(db)
        dbt.create_user(
            db,
            {
                "name": "test",
                "UserName": "Test1 Supervisor",
                "email": "test.example.com",
                "password": "test",
                "role_id": 2,
                "group_id": 1,
            },
        )

    def login(self):
        self.client.post(
            "/auth/login",
            data={
                "username": "test",
                "password": "test",
            },
        )

    def test_supervisor_view(self):
        self.login()

        response = self.client.get(url_for("samples.groupsamples"))
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Your are not having the right role for this" not in html

        response = self.client.get(url_for("conf.index"))
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Change Password" in html
        assert "New P/P/M Type" in html
        assert "New Location" in html

    def test_mtype(self):
        self.login()

        # Test to see mtype page
        response = self.client.get(url_for("conf.register_type"))
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Existing Measurement Types" in html

        # Test adding mtype
        test_mtype_name = "test mtype"
        response = self.client.post(
            url_for("conf.register_type"), data={"mtype": test_mtype_name}, follow_redirects=True
        )
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Change Password" in html
        assert test_mtype_name in html
        assert "Measurement type created." in html
        assert "alert-success" in html

        # Test measurement type deduplication
        response = self.client.post(
            url_for("conf.register_type"), data={"mtype": test_mtype_name}, follow_redirects=True
        )
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Change Password" not in html
        assert f"MeasurementType {test_mtype_name} is already registered." in html
        assert "alert-warning" in html

        # Test mtype deletion
        m = MeasurementType.query.filter_by(name=test_mtype_name).first()
        url = safe_url_for(m.id, "conf.delete_type", current_app.config["SEC_SESSION_KEY"], g.salt)
        response = self.client.get(url, follow_redirects=True)
        assert response.status_code == 200
        assert MeasurementType.query.filter_by(name=test_mtype_name).count() == 0
        html = response.get_data(as_text=True)
        assert f"{test_mtype_name} was deleted" in html
        assert "alert-success" in html

    def test_location(self):
        self.login()

        # Test to see location types
        response = self.client.get(url_for("conf.register_location"))
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Existing Location" in html

        # Test adding location type
        test_location_name = "test location"
        response = self.client.post(
            url_for("conf.register_location"), data={"mtype": test_location_name}, follow_redirects=True
        )
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Change Password" in html
        assert test_location_name in html
        assert "Location created." in html
        assert "alert-success" in html

        # Test location deduplication
        response = self.client.post(
            url_for("conf.register_location"), data={"mtype": test_location_name}, follow_redirects=True
        )
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Change Password" not in html
        assert f"Location {test_location_name} is already registered." in html
        assert "alert-warning" in html

        # Test location deletion
        l = Locations.query.filter_by(name=test_location_name).first()
        url = safe_url_for(l.id, "conf.delete_location", current_app.config["SEC_SESSION_KEY"], g.salt)
        response = self.client.get(url, follow_redirects=True)
        assert response.status_code == 200
        assert Locations.query.filter_by(name=test_location_name).count() == 0
        html = response.get_data(as_text=True)
        assert f"{test_location_name} was deleted" in html
        assert "alert-success" in html

        # TODO: test deactivate_user(), update(), register(), user_mangement()
