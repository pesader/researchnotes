## Installation and configuration

Run the app against a Python WSGI server of your choice. Do not use the integrated flask server in production.

- [Setup on FreeBSD](freebsd) using uWSGI and nginx 
- [Setup on Linux (Ubuntu)](linux) using Gunicorn and nginix
- [Setup on FreeBSD](freebsd_packages) using packages, Gunicorn, supervisord and nginx

At the current point, I recommend to install OpenResearchNotes into a Python environment and use Gunicorn and supervisord
serving over nginx. Nginx should be configured to use https - one can use self-signed certificates if you are
behind a firewall and limit access just form inside your institution (seems smart). Just Google chrome's password managers
does not like this.

### Configuration of ResearchNotes

As ResearchNotes is a [flask-based](https://flask.palletsprojects.com/) web 
application, you can configure it by setting the environment variable RESEARCHNOTES_CONF 
to a place (default we check in the UPLOAD directory on Windows and in /usr/var/local/etc for Unix),
where a ResearchNotes_conf.py is stored. You might want to change the UPLOAD_PATH 
for the storing of files and the database (make sure it is read/writable by your process). 
We use flask [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/) 
as database ORM. Hence, by configuring SQLALCHEMY_DATABASE_URI in the ResearchNotes_conf.py, 
you can use any database supported. By default, we use SQLite in the upload directory, 
which is okish for small installations.

Also, the create_app function excepts a configure file as parameter, if you do not want to use
the environment variable. To see available variables, look into the Open ResearchNotes app_configuration modul.
It also shows the different default values - most of them make sense but you will have to define an upload directory.

As you will start putting information into a SQL database, the choice of the database
is important as changing will involve migrating your data from one database engine 
to the next. A task not supported by the app.

A typical configuration file for the up (I normally give the path with the environment variable) looks 
something like this on my Windows system

        UPLOAD_PATH = r'd:\ResearchNotes'
        SQLALCHEMY_DATABASE_URI = r"sqlite:///D:\ResearchNotes\ResearchNotes.sqlite"

 
After the installation and setting the flask environment variable FLASK_APP to ResearchNotes,
you should run:

      python -m flask init_db
      python -m flask db init
      python -m flask db upgrade

This will create the initial database with one user registered (admin/admin) as 
well as initialize flask-migrate for later database updates. Remember to change the
admin passwords.

### Config file

For any more advanced installation, you will have to create a config file that be either be called over the
environment variable or given as parameter to the create_app function. The main points are database, logging, 
and search engine. An advanced config file will look something like this:

      UPLOAD_PATH = r'/tank/ResearchNotes/data'
      #SQLALCHEMY_DATABASE_URI = r"sqlite:////tank/ResearchNotes/data/ResearchNotes.sqlite"
      SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://ResearchNotes@127.0.0.1/ResearchNotes'

      DATABASE_MIGRATION = r'/usr/local/www/researchnotes/migrations'

      PERMANENT_SESSION_LIFETIME = 24*24*4000
      PREFERRED_URL_SCHEME = 'https'

      LOG_PATH = r'/usr/local/www/researchnotes/logs'
      LOG_FILENAME = r'rn-logs.log'

      LOG_SYSLOG = True
      LOG_SYSLOG_ADDR = "/var/run/log"

      #MEILISEARCH_URL = 'http://localhost:7700'
      ELASTICSEARCH_URL = 'http://localhost:9200'

Notice, that some configuration is needed to run over https and self-signed certificates. 

### Commandline and other administrative task

If you create a "wsgi.py" file, flask will execute this if called in the directory of the file. This
allows for some administrative tasks to be carried out easily. There are some command line functions defined
beside the ones coming from flask-migrate or flask itself.

These are mainly to dump out the database as well as do some database maintenance.

        clean_doc_database   Clean and correct entries in the Document database.
        dump_database        Dump databases into a .csv file for saving 
        dump_database_json   Dump databases into a json file for saving 	
        load_database_json   Import database from json dump.
        update_search_index  Reindex all relevant entries to the search engine
		
**The load database command will reset the existing database!** So, call with care. It might be a good idea to define
a cron job that calls one of the dump functions once a day - we recommend to the "dump_database_json". If you use a 
SQL backend, a "mysqldump" might additionally be a good idea. 

### Search engine

We have bindings to two search engines: meilisearch and elasticsearch. Elasticsearch is the overkill but works everywhere.
Meilisearch might be better suited as smaller and more for the intended document size. But the program is rather new, and
we experienced problems.

You will have to install one of the two search engine options. Then you need to set one of the configuration variables:

            ELASTICSEARCH_URL = os.environ.get("ELASTICSEARCH_URL")
            MEILISEARCH_URL = os.environ.get("MEILISEARCH_URL")

or include them into your config.py loaded by Open ResearchNotes. Furthermore, you have to install the fitting Python package, e.g.:

            pip install elasticsearch==7.17.3

or

            pip install meilisearch

Elasticsearch has started to change some commands of the search engine in 8.xx, and we are currently still using the last open
source version 7.x. With this, the search bar in the main menu should come alive. Currently, we index long descriptions of ESS, 
PMM and reports as well as identifiers, titles and other text fields.

Search results might vary by engine and by version as we will try fine-tuning the search command.

