"""
Notification modul.

If we do some real time notification, everything should go here. By now, we just did a first testing by
implementing a test view to return a random dict and a test_view using brython to refresh a message
every 10 sec.
"""
import os
import random
import string
import typing

from werkzeug.utils import secure_filename
from werkzeug.wrappers.response import Response

from flask import Blueprint, render_template, current_app, send_from_directory, g

from ResearchNotes.auth import login_required


bp = Blueprint("notifications", __name__, url_prefix="/notifications")


@bp.route("/test")
@login_required
def test() -> typing.Dict[str, str]:
    letters = string.ascii_lowercase
    return {"key": "".join(random.choice(letters) for i in range(10))}


@bp.route("/test_view")
@login_required
def test_view() -> str:
    return render_template("/notifications/test.html", title="test")
