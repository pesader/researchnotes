"""
Database sub module

As the database growth, it will be split into files and submodules to make it easier to handel.

"""
from sqlalchemy import MetaData
from flask_sqlalchemy import SQLAlchemy

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

metadata = MetaData(naming_convention=convention)

db: SQLAlchemy = SQLAlchemy(metadata=metadata)

# This needs to be done to prevent cyclic imports
#
# Importing stuff here will make the classes available as part of the module :-)

# pylint: disable=import-outside-toplevel
# pylint: disable=wrong-import-position
from .basics import User, Role, Locations, Groups
from .ess_ppm_report import (
    MeasurementType,
    Measurements,
    Samples,
    Reports,
    TemplateSamples,
    TemplateMeasurements,
    TemplateReports,
)

from .documents import Documents
from .instrument_journal import (
    Instrument,
    InstrumentationJournalEntry,
    TemplateInstrument,
    TemplateInstrumentationJournalEntry,
    EntryType,
)
