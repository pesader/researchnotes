# -*- coding: utf-8 -*-
"""
Measurements submodule.

Everything for the P/P/M part will go here.
"""

import os
import shutil

# from datetime import datetime
import typing

from werkzeug.wrappers.response import Response

from flask import Blueprint, flash, g, redirect, render_template, url_for, current_app, abort

from ResearchNotes.auth import login_required
import ResearchNotes.database_transactions as dbt
from ResearchNotes.database import (
    db,
    Measurements,
    MeasurementType,
    TemplateMeasurements,
)

from ResearchNotes.form import MeasurementCreateForm, UseTemplate  # , MeasurementOrder
from ResearchNotes.report import _delete_report

# from ResearchNotes.instruments import get_group_instruments
from ResearchNotes.files import uploaddir_path, make_file_list
from ResearchNotes.url_security import token_decode

# Measurement = NewType("Measurement", Measurements)

bp = Blueprint("measurements", __name__, url_prefix="/measurements")


def create_form(tid: int, group_id: int):
    """
    Fills the create form for a new measurement.

    Parameters
    ----------
    tid : int
        Template ID to fill.
    group_id : int
        Group_id of the user (containing all the measurement types to fill).

    Returns
    -------
    WTForm object
        The created form object to be used in the html template.

    """

    tobj = None
    if tid != 0:
        tobj = db.session.get(TemplateMeasurements, tid)

    form = MeasurementCreateForm(obj=tobj)

    # measurement type choices
    form.mtype.choices = [
        (cg.id, cg.name)
        for cg in db.session.execute(
            db.select(MeasurementType).order_by("name").filter_by(group_id=group_id)
        ).scalars()
    ]
    form.mtype.choices.insert(
        0, (db.session.get(MeasurementType, 1).id, db.session.get(MeasurementType, 1).name)
    )

    if tobj is not None:
        if tobj.mtype_id:
            tm_type = db.session.get(MeasurementType, tobj.mtype_id)
            if tm_type:
                default_type = (tobj.mtype_id, tm_type.name)
                if default_type in form.mtype.choices:
                    form.mtype.choices.insert(
                        0, form.mtype.choices.pop(form.mtype.choices.index(default_type))
                    )
                else:
                    form.mtype.choices.insert(0, default_type)
                form.mtype.default = 0

    # instrument choices
    form.instrument_id.choices = [
        (instrument.id, instrument.identifier)
        for instrument in (
            [i for i in g.user.group_member.owned_instruments if i.active]
            + [i for i in g.user.shared_instruments if i.active]
        )
    ]
    form.instrument_id.choices.insert(0, (0, "None/Other"))

    return form


@bp.route("/htmx_fill_template", methods=["PUT"])
@login_required
def htmx_fill_template() -> str:
    """
    Htmx sniplet that will fill the template into the form and send it back.

    Returns
    -------
    str
        Rendered HTML form (with template filled).
    """
    current_app.logger.debug(" htmx : Change template")

    choose_form = UseTemplate()

    # As we do not fill the option into the choose form (templates), we cannot validate the input here
    form = create_form(choose_form.template.data, g.user.group_id)

    current_app.logger.debug(f" htmx : Template is {choose_form.template.data}")

    return render_template("measurements/inner_form.html", form=form)


@bp.route("/<list:ids>/create", methods=("GET", "POST"))
@login_required
def create(ids: typing.List) -> typing.Union[str, Response]:
    """
    Create a new measurement database entry.

    Parameters
    ----------
    ids : List of the sample and template id
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    sid, tid = ids  # sample ID, template ID

    form = create_form(tid, g.user.group_id)

    # Templates to chose from
    choose_form = UseTemplate()

    choose_form.template.choices = [
        (cg.id, cg.tname)
        for cg in db.session.execute(
            db.select(TemplateMeasurements).order_by("tname").filter_by(creator_id=g.user.id)
        ).scalars()
    ]
    choose_form.template.choices.insert(0, (0, "None"))

    # if choose_form.validate_on_submit():
    #     if choose_form.template.data is not None:
    #         return redirect(url_for("measurements.create", ids=[sid, choose_form.template.data]))

    if form.validate_on_submit():
        new_measurement = {
            "creator_id": g.user.id,
            "short_dis": form.short_dis.data,
            "long_dis": form.long_dis.data,
            "sample_id": sid,
            "mtype_id": form.mtype.data,
            "creator": form.creator.data,
        }

        if form.instrument_id.data:
            # print("form.instrument_id.data")
            new_measurement["instrument_id"] = form.instrument_id.data

        mid = dbt.create_measurement(db, new_measurement)
        flash(f"P/P/M { new_measurement['short_dis']} created", "alert-info")
        return redirect(url_for("measurements.measurement_view", mid=mid))

    return render_template(
        "measurements/create.html", form=form, choose_form=choose_form, measurement=None
    )


def get_measurement(mid: int, check_author: bool = True) -> Measurements:
    """
    Retrieve a given measurement from the database checking the permission of the user before.

    Parameters
    ----------
    mid : int
        database id of the measurement.
    check_author : bool, optional
        If true, we check for permission. The default is True.

    Returns
    -------
    measurement : ResearchNotes.Measurement
        Loaded Measurement database entry.

    """
    measurement = db.get_or_404(
        Measurements,
        mid,
        description=f" : get_measurement : {g.user} tried to load measurement {mid} which does not exist",
    )

    if (
        check_author
        and int(measurement.measurement_sample.creator_id) != g.user.id
        and g.user.id not in measurement.measurement_sample.sharedsamplelist
    ):
        abort(
            403,
            description=" : get_measurement : Authorization failure."
            + f" {g.user} tried to load measurement {mid}",
        )
    #
    return measurement


@bp.route("/<int:mid>/update", methods=("GET", "POST"))
@login_required
def update(mid: int) -> typing.Union[str, Response]:
    """
    Update a measurement entry of the database.

    Therefore, we get the measurement and
    fill the form with the required entries.

    Actually, the measurement typ gets lost.

    Parameters
    ----------
    mid : int
        Measurement ID in database.

    Returns
    -------
    str|Flask.Response
        Render update form or redirect to measurement view.

    """
    measurement = get_measurement(mid)
    form = MeasurementCreateForm(obj=measurement)

    # measurement type choices
    form.mtype.choices = [
        (cg.id, cg.name)
        for cg in db.session.execute(
            db.select(MeasurementType).order_by("name").filter_by(group_id=g.user.group_id)
        ).scalars()
        if cg.id != measurement.measurement_type.id
    ]
    form.mtype.choices.insert(
        0,
        (measurement.measurement_type.id, measurement.measurement_type.name),
    )
    if measurement.measurement_type.id != 1:
        form.mtype.choices.append(
            (db.session.get(MeasurementType, 1).id, db.session.get(MeasurementType, 1).name)
        )
    form.mtype.default = [measurement.mtype_id]

    # instrument choices
    form.instrument_id.choices = [
        (instrument.id, instrument.identifier)
        for instrument in (
            [i for i in g.user.group_member.owned_instruments if i.active]
            + [i for i in g.user.shared_instruments if i.active]
        )
    ]
    if measurement.instrument_id is not None:
        form.instrument_id.choices.append((0, "None/Other"))
    else:
        form.instrument_id.choices.insert(0, (0, "None/Other"))

    if form.validate_on_submit():
        updated_measurement_info: dict = {
            "short_dis": form.short_dis.data,
            "long_dis": form.long_dis.data,
            "mtype": form.mtype.data,
            "creator": form.creator.data,
        }
        if form.instrument_id.data:
            # print(f"form.instrument_id.data {form.instrument_id.data}")
            updated_measurement_info["instrument_id"] = form.instrument_id.data

        dbt.update_measurement(db, measurement, updated_measurement_info)
        flash("P/P/M information updated", "alert-info")
        return redirect(url_for("measurements.measurement_view", mid=mid))
    #
    return render_template("measurements/create.html", form=form, measurement=measurement)


@bp.route("/<int:mid>/measurement", methods=("GET", "POST"))
@login_required
def measurement_view(mid: int) -> str:
    """
    Display a measurement and all associated files.

    Parameters
    ----------
    mid : int
        Measurement ID.

    Returns
    -------
    str
        Renders template of the measurement view.

    """
    measurement = get_measurement(mid)

    files = make_file_list(
        uploaddir_path(
            [
                "m",
                measurement.id,
                measurement.sample_id,
                measurement.measurement_sample.identifier,
            ]
        )
    )

    return render_template("measurements/measurement.html", measurement=measurement, files=files)


@bp.route("/all_measurements", methods=("GET", "POST"))
@login_required
def allmeasurements() -> str:
    """
    Show all measurements of created by user included for shared samples.

    Returns
    -------
    str
        Renders all measurement template.

    """
    return render_template("measurements/allmeasurements.html")


def _delete_measurements(measurement: Measurements) -> typing.Optional[Exception]:
    """
    Delete a single measurement in the database as well as all associated with it.

    Parameters
    ----------
    measurement : ResearchNotes.Measurements
        Measurement database record.

    Returns
    -------
    error : None|Exception str
        DESCRIPTION.

    """
    error = None  # type: None

    path = uploaddir_path(
        [
            "m",
            measurement.id,
            measurement.sample_id,
            measurement.measurement_sample.identifier,
        ]
    )

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except shutil.Error as error:
            abort(500, description=f"Failed to delete {path}. Exception {error}")
    #
    with dbt.Transaction(db) as db_session:
        db_session.delete(measurement)
    return error


@bp.route("/<string:token>/delete")
@login_required
def delete(token: str) -> Response:
    """
    Delete measurement.

    Like for the samples, we have to implement a cascade of deletions
    and cross-references. In this case, first we delete all reports associated
    with the measurement, then we delete the measurement itself.

    Parameters
    ----------
    token : str
        Signed string that encodes the id of the measurement to delete

    Returns
    -------
    Flask.Response
        Redirects to the view of the sample that contained the measurement

    """
    mid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)
    measurement = get_measurement(mid)
    sid = measurement.sample_id

    for report in measurement.reports:
        flash(f"Deleting report: {report.title}", "alert-info")
        _delete_report(report)
    flash(f"Deleted measurement {measurement.short_dis}", "alert-info")
    _delete_measurements(measurement)

    return redirect(url_for("samples.sample_view", sid=sid))
