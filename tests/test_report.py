from flask import current_app, g, url_for
import pytest

from ResearchNotes import db
from ResearchNotes.database.ess_ppm_report import Reports
from ResearchNotes.url_security import safe_url_for

pytestmark = pytest.mark.report


class TestReportCreateForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_measurement):
        with app.test_request_context():
            return client.get(url_for("report.create", mid=default_test_measurement.id))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible content
        assert "Title" in html
        assert "Long Description" in html
        assert "Submit Report" in html
        assert "Markdown" in html
        # html tags
        assert 'name="title"' in html
        assert 'name="long_dis"' in html
        assert 'name="submit"' in html


class TestReportCreate:
    new_report_info = {
        "title": "test report title",
        "long_dis": "test report long description",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_measurement):
        with app.test_request_context():
            return client.post(
                url_for("report.create", mid=default_test_measurement.id),
                data={
                    "title": self.new_report_info["title"],
                    "long_dis": self.new_report_info["long_dis"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def new_report(self):
        return db.session.execute(
            db.select(Reports).filter_by(title=self.new_report_info["title"])
        ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Report" in html
        assert "Hide/Show" in html

    def test_dynamic_content(self, html):
        assert self.new_report_info["title"] in html
        assert self.new_report_info["long_dis"] in html

    def test_alert(self, html):
        assert f"Report {self.new_report_info['title']} created" in html
        assert "alert-info" in html

    def test_database(self, new_report):
        assert new_report is not None


class TestReportUpdateForm:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_report):
        with app.test_request_context():
            return client.get(url_for("report.update", rid=default_test_report.id))

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        # visible content
        assert "Title" in html
        assert "Long Description" in html
        assert "Submit Report" in html
        assert "Markdown" in html
        # html tags
        assert 'name="title"' in html
        assert 'name="long_dis"' in html
        assert 'name="submit"' in html

    def test_dynamic_content(self, html, default_test_report):
        assert default_test_report.title in html
        assert default_test_report.long_dis in html


class TestReportUpdate:
    updated_report_info = {
        "title": "updated report title",
        "long_dis": "updated report long description",
    }

    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_report):
        with app.test_request_context():
            return client.post(
                url_for("report.update", rid=default_test_report.id),
                data={
                    "title": self.updated_report_info["title"],
                    "long_dis": self.updated_report_info["long_dis"],
                    "submit": True,
                },
                follow_redirects=True,
            )

    @pytest.fixture(scope="class")
    def updated_report(self):
        return db.session.execute(
            db.select(Reports).filter_by(title=self.updated_report_info["title"])
        ).scalar()

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Report" in html
        assert "Hide/Show" in html

    def test_dynamic_content(self, html):
        assert self.updated_report_info["title"] in html
        assert self.updated_report_info["long_dis"] in html

    def test_alert(self, html):
        assert f"Report information updated"
        assert "alert-info" in html

    def test_database(self, updated_report):
        assert updated_report is not None


class TestReportDelete:
    @pytest.fixture(scope="class", autouse=True)
    def response(self, app, client, default_test_report):
        with app.test_request_context():
            # NOTE: g.salt is only available on certain views
            client.post(
                url_for("report.report_view", rid=default_test_report.id),
                follow_redirects=True,
            )
            url = safe_url_for(
                default_test_report.id, "report.delete", current_app.config["SEC_SESSION_KEY"], g.salt
            )
            return client.get(url, follow_redirects=True)

    def test_success(self, response):
        assert response.status_code == 200

    def test_static_content(self, html):
        assert "Description" in html
        assert "Hide/Show" in html
        assert "inserted/updated" in html

    def test_dynamic_content(self, app, html, default_test_report):
        with app.test_request_context():
            assert (
                url_for(
                    "report.report_view",
                    rid=default_test_report.id,
                )
                not in html
            )

    def test_alert(self, html, default_test_report):
        assert f"Deleted report {default_test_report.title}" in html
        assert "alert-info" in html

    def test_database(self, default_test_report):
        assert db.session.get(Reports, default_test_report.id) is None
