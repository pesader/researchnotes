.. Open ResearchNotes documentation master file, created by
   sphinx-quickstart on Tue Feb 15 09:43:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Open ResearchNote's documentation!
==============================================

Open ResearchNotes is an electronic lab notebook (ELN) to keep track of your experiments/samples/simulations,
procedures/processing/measurements and results. Unlike your handwritten lab book, it is organized by samples - which are
understood of the entity you are investigations. All procedures, measurements and
results are connected to the sample, keeping things in a logical order and not distributed over
various places.

Features
=========

* Organization of the research data by samples (ESS entries), to which measurements (PPM entries) and reports are connected.

* ESS entries have identifiers and description to provide metadata to attached files and to physical samples. We also have extra fields to mark for ESS origin, creator and creation place. We track creation and update times for you. These entries can be ordered and searched to find data or samples information.

* Indexing against a full text search engine (ElasticSearch or Meilisearch) allowing to search the long description and other relevant fields.

* Users are organized by research groups and can have either the role of a student or supervisors. On leaving the group, the student account is converted into a former student account and data shared with the supervisor.

* Easy upload of files. For various file types, a preview function is implemented.

* Long description text for the ESS, PPM and report entries can be formatted and translated to HTML using Markdown. A Markdown toolbar is implemented to help to learn the commands. The Markdown can refer to uploaded images.

* A documentation wiki using Markdown as to format and creation of new documents containing information not directly connected to samples (e.g. experimental protocols, common procedures etc.). Documents are shared and editable on the group bases.

* A Instrumentation Journal that can be used to keep track of setups and instrumentation logs. Basically, you keep a log over your equipment and note information not related to ESS entries.

* ESS entries can be shared  between persons with an account in the software allowing collaboration and collection of research data form different sources in one entry.

* ESS location can be marked and used to track the location of a sample.

* ESS Entries are exportable to markdown files for storage and backup outside the software.

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: User documentation

   help
   install
   howto
   searching
   other

.. toctree::
   :maxdepth: 3
   :caption: Code documentation

   code
   modules




Indices and tables
==================df

* :ref:`genindex`

