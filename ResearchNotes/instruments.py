# -*- coding: utf-8 -*-
"""
Instrument submodule.

Everything regarding the instruments is done here.
"""

import os
import shutil
import copy
import typing

from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    url_for,
    current_app,
    abort,
)

from werkzeug.utils import secure_filename
from werkzeug.wrappers.response import Response

import ResearchNotes.database_transactions as dbt

from ResearchNotes.auth import login_required, role_required
from ResearchNotes.database import (
    Instrument,
    TemplateInstrument,
    TemplateInstrumentationJournalEntry,
    TemplateMeasurements,
    TemplateSamples,
    db,
    User,
    Locations,
    Groups,
)
from ResearchNotes.entry import delete_entry_data

from ResearchNotes.form import ChooseDefaultTemplatesForm, InstrumentCreate, InstrumentShare, UseTemplate
from ResearchNotes.files import uploaddir_path, make_file_list
from ResearchNotes.url_security import token_decode

bp = Blueprint("instruments", __name__, url_prefix="/instruments")


@bp.route("/")
@login_required
def index() -> str:
    """
    Index view for the instrument part.

    Show five most recently updated owned instruments and shared instruments

    Returns
    -------
    str
        Render index template.

    """
    group_instruments = [i for i in g.user.group_member.owned_instruments if i.active]

    shared_instruments = [i for i in g.user.shared_instruments if i.active]

    deactivated_instruments = [i for i in g.user.group_member.owned_instruments if not i.active]

    return render_template(
        "instruments/index.html",
        group_instruments=group_instruments,
        shared_instruments=shared_instruments,
        deactivated_instruments=deactivated_instruments,
    )


@bp.route("/<int:iid>/log_view", methods=("GET", "POST"))
@login_required
def log_view(iid: int) -> str:
    """
    Shows all the log entries for an instrument.

    Parameters
    ----------
        iid : int
            Instrumentation identifier

    Returns
    -------
        str|Response
            Log_view
    """
    instrument = get_instrument(iid)

    return render_template(
        "instruments/log_overview.html",
        instrument=instrument,
    )


def create_create_form(tid: int, group_id: int, template=None):
    """
    Fill the create form for new instrument fom.


    Parameters
    ----------

    tid : int
        Template ID to be used in the create form
    group_id :int
        Group ID of user to select locations.
    template : Instrument
        Data record used to fill form. Default None

    Returns
    -------
    WTForm
        The fiolled WTFOmr object to be rendered later.

    """

    if tid != 0:
        template = db.get_or_404(
            TemplateInstrument,
            tid,
            description=f" instruments.create : {g.user} tried to load sample template {tid}",
        )
    create_instrument_form = InstrumentCreate(obj=template)

    create_instrument_form.location_id.choices = [
        (loc.id, loc.name)
        for loc in db.session.execute(
            db.select(Locations).order_by("name").filter_by(group_id=group_id)
        ).scalars()
    ]

    create_instrument_form.location_id.choices.insert(
        0, (db.session.get(Locations, 1).id, db.session.get(Locations, 1).name)
    )

    return create_instrument_form


@bp.route("/htmx_fill_create_template", methods=["PUT"])
@login_required
def htmx_fill_create_template() -> str:
    """
    Htmx rout that puts a new template data into the create form.

    Returns
    -------
    str
        Filled and rendered form to replace old form in page.
    """

    current_app.logger.debug(": htmx : Fill template into create form")

    choose_template_form = UseTemplate()

    # As we do not fill the option into the choose form (templates), we cannot validate the input here
    # current_app.logger.debug(f": htmx : Form validation: {choose_template_form.validate_on_submit()}")
    form = create_create_form(choose_template_form.template.data, g.user.group_id)
    current_app.logger.debug(f": htmx : Fill template {choose_template_form.template.data}")

    return render_template("instruments/inner_create_form.html", create_instrument_form=form)


@bp.route("/<int:tid>/create", methods=("GET", "POST"))
@login_required
def create(tid: int) -> typing.Union[str, Response]:
    """
    Create new instrument entry.

    Parameters
    ----------
    tid : int
        Template to be used for create dialog (default is 0 - None used)

    Returns
    -------
    str|Response
        Renders create instrument form or instrument view.

    """

    create_instrument_form = create_create_form(tid, g.user.group_id)

    choose_template_form = UseTemplate()
    choose_template_form.template.choices = [
        (t.id, t.tname)
        for t in db.session.execute(db.select(TemplateInstrument).order_by("tname")).scalars()
        if (t.group_id != 1 and t.group_id == g.user.group_id) or (t.creator_id == g.user.id)
    ]
    choose_template_form.template.choices.insert(0, (0, "None"))

    if create_instrument_form.validate_on_submit():
        instrument_info = {
            "identifier": secure_filename(create_instrument_form.identifier.data),
            "description": create_instrument_form.description.data,
            "creator_id": g.user.id,
            "group_id": g.user.group_id,
            "location_id": create_instrument_form.location_id.data,
        }

        i_id = dbt.create_instrument(db, instrument_info)
        flash(f"Created instrument {instrument_info['identifier']}", "alert-success")

        return redirect(url_for("instruments.instrument_view", iid=i_id))

    return render_template(
        "instruments/create.html",
        create_instrument_form=create_instrument_form,
        choose_template_form=choose_template_form,
        instrument=None,
    )


@bp.route("/<int:iid>/update", methods=("GET", "POST"))
@login_required
def update(iid: int) -> typing.Union[str, Response]:
    """
    Update instrument information.

    We also have to rename the instrument directory,
    if it already exits.

    Parameters
    ----------
    iid : Integer
        Instrument ID to update.

    Returns
    -------
    str|Flask.Response
        Renders either form or redirects to the instrument entry.

    """
    instrument = get_instrument(iid)
    old_identifier = copy.copy(instrument.identifier)
    form = create_create_form(0, g.user.group_id, template=instrument)

    # form.location_id.choices = [
    #     (loc.id, loc.name)
    #     for loc in db.session.execute(
    #         db.select(Locations).order_by("name").filter_by(group_id=g.user.group_id)
    #     ).scalars()
    # ]
    #
    # form.location_id.choices.insert(
    #     0, (db.session.get(Locations, 1).id, db.session.get(Locations, 1).name)
    # )

    if form.validate_on_submit():
        dbt.update_instrument(
            db,
            instrument,
            {
                "identifier": secure_filename(form.identifier.data),
                "description": form.description.data,
                "location_id": form.location_id.data,
            },
        )

        if str(old_identifier) != secure_filename(instrument.identifier):
            old_path = uploaddir_path(["i", instrument.id, old_identifier])
            new_path = uploaddir_path(["i", instrument.id, secure_filename(instrument.identifier)])

            if os.path.exists(old_path):
                os.rename(old_path, new_path)
                flash("Instrument path updated", "alert-success")

        flash("Instrument information updated", "alert-success")
        return redirect(url_for("instruments.instrument_view", iid=instrument.id))

    return render_template("instruments/create.html", create_instrument_form=form, instrument=instrument)


@bp.route("/<string:token>/delete")
@role_required(["Supervisor"])
def delete(token: str) -> Response:
    """
    Delete an instrument.

    Delete all data related to an instrument, including all its files and all
    its journal entries (as well as the files of those entries)

    Parameters
    ----------
    token : str
        Encrypted ID of instrument to delete

    Returns
    -------
    Response
        Redirects to instrument view

    """
    iid = token_decode(token, current_app.config["SEC_SESSION_KEY"], g.salt)
    instrument = get_instrument(iid)

    current_app.logger.info(f": delete : Delete instrument {instrument}")

    if instrument.active or len(list(instrument.measurements)) > 0:
        flash(
            "Only deactivated instruments without associated measurements can be deleted", "alert-info"
        )
    else:
        for entry in instrument.journal_entries:
            delete_entry_data(entry)
            flash(f'Journal entry "{entry.identifier}" deleted', "alert-info")

        delete_instrument_data(instrument)
        flash(f'Instrument "{instrument.identifier}" deleted', "alert-info")
        current_app.logger.info(f": delete : Instrument {instrument} and all its data was deleted.")

    return redirect(url_for("instruments.index", iid=iid))


def delete_instrument_data(instrument: Instrument) -> None:
    """
    Delete all data directly associated with an instrument.

    This includes all files and the database object itself, but it does not
    include any data associated with the instrument's journal entries.

    Parameters
    -----------
        instrument: Instrument.
            Instrument database record, whose data is deleted
    Returns
    -------
        None
    """
    # remove all associated files
    path = uploaddir_path(["i", instrument.id, instrument.identifier])

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except shutil.Error as error:
            abort(500, description=f"Failed to delete {path}. Exception {error}")

    # remove database object
    with dbt.Transaction(db) as db_session:
        db_session.delete(instrument)


@bp.route("/<int:iid>/deactivate", methods=("GET", "POST"))
@login_required
@role_required(["Supervisor", "StudentAdmin"])
def deactivate(iid: int) -> Response:
    """
    Deactivate an instrument.

    Parameters
    ----------
    iid : Integer
        Instrument ID to deactivate.

    Returns
    -------
    Flask.Response
        Redirects to instrument view.

    """
    instrument: Instrument = get_instrument(iid)
    if instrument.owner_group.id == g.user.group_id:
        dbt.deactivate_instrument(db, instrument)
        flash(f"Instrument {instrument.identifier} deactivated", "alert-info")
    return redirect(url_for("instruments.index", iid=iid))


@bp.route("/<int:iid>/activate", methods=("GET", "POST"))
@role_required(["Supervisor", "StudentAdmin"])
def activate(iid: int) -> Response:
    """
    Activate an instrument.

    Parameters
    ----------
    iid : Integer
        Instrument ID to activate.

    Returns
    -------
    Flask.Response
        Redirects to instrument view.

    """
    instrument: Instrument = get_instrument(iid)
    if instrument.owner_group.id == g.user.group_id:
        dbt.activate_instrument(db, instrument)
        flash(f"Instrument {instrument.identifier} re-activated", "alert-success")
    return redirect(url_for("instruments.index", iid=iid))


def create_default_template_form(user_id: int, instrument: Instrument):
    """
    Fills in the form to choose the default templates for an instrumetn.

    Parameters
    ----------
    user_id : int
        User ID of the active user
    instrument : Instrument
        Instrument record to chose the default templates for

    Returns
    -------
    WTForm
        Filled WFTForm object that will be rendered in the html later

    """

    choose_default_templates_form = ChooseDefaultTemplatesForm()

    # Default sample template
    choose_default_templates_form.ess_template.choices = [
        (template.id, template.tname)
        for template in db.session.execute(
            db.select(TemplateSamples).order_by("tname").filter_by(creator_id=user_id)
        ).scalars()
    ]

    # template might be set by a different user, so it won't be included in the
    # list if we filter templates by "creator_id=g.user.id". To fix this,
    # append the current default template to the list
    if instrument.default_ess_template is not None:
        default_choice = (
            instrument.default_ess_template.id,
            instrument.default_ess_template.tname,
        )
        if default_choice not in choose_default_templates_form.ess_template.choices:
            choose_default_templates_form.ess_template.choices.append(default_choice)

    if instrument.default_ess_template is not None:  # move current choice to 1st position
        choose_default_templates_form.ess_template.choices.append((0, "None"))
        choose_default_templates_form.ess_template.choices.insert(
            0,
            choose_default_templates_form.ess_template.choices.pop(
                choose_default_templates_form.ess_template.choices.index(
                    (instrument.default_ess_template.id, instrument.default_ess_template.tname)
                )
            ),
        )
    else:
        choose_default_templates_form.ess_template.choices.insert(0, (0, "None"))

    # Default measurement template
    choose_default_templates_form.ppm_template.choices = [
        (template.id, template.tname)
        for template in db.session.execute(
            db.select(TemplateMeasurements).order_by("tname").filter_by(creator_id=user_id)
        ).scalars()
    ]

    if instrument.default_ppm_template is not None:
        default_choice = (
            instrument.default_ppm_template.id,
            instrument.default_ppm_template.tname,
        )
        if default_choice not in choose_default_templates_form.ppm_template.choices:
            choose_default_templates_form.ppm_template.choices.append(default_choice)

    if instrument.default_ppm_template is not None:  # move current choice to 1st position
        choose_default_templates_form.ppm_template.choices.append((0, "None"))
        choose_default_templates_form.ppm_template.choices.insert(
            0,
            choose_default_templates_form.ppm_template.choices.pop(
                choose_default_templates_form.ppm_template.choices.index(
                    (instrument.default_ppm_template.id, instrument.default_ppm_template.tname)
                )
            ),
        )
    else:
        choose_default_templates_form.ppm_template.choices.insert(0, (0, "None"))

    # Default journal entry template
    choose_default_templates_form.entry_template.choices = [
        (entry_template.id, entry_template.tname)
        for entry_template in db.session.execute(
            db.select(TemplateInstrumentationJournalEntry).order_by("tname")
        ).scalars()
        if (entry_template.group_id != 1 and entry_template.group_id == g.user.group_id)
        or (entry_template.creator_id == g.user.id)
    ]
    if instrument.default_entry_template:  # move current choice to 1st position
        choose_default_templates_form.entry_template.choices.append((0, "None"))
        choose_default_templates_form.entry_template.choices.insert(
            0,
            choose_default_templates_form.entry_template.choices.pop(
                choose_default_templates_form.entry_template.choices.index(
                    (instrument.default_entry_template.id, instrument.default_entry_template.tname)
                )
            ),
        )
    else:
        choose_default_templates_form.entry_template.choices.insert(0, (0, "None"))

    return choose_default_templates_form


@bp.route("/<int:iid>/htmx_instrument_template", methods=["PUT"])
@login_required
def htmx_instrument_template(iid: int) -> str:
    """
    Htmx sniplet that will save the chosen default tempalte and rerender the form.

    Parameters
    ----------
    iid : int
    ID of the instrument data record.

    Returns
    -------
    str
    Rendered html form to be swapped in the form.

    """
    current_app.logger.debug(" : htmx : Change template")
    instrument = get_instrument(iid)

    choose_default_templates_form = create_default_template_form(g.user.id, instrument)

    dbt.update_default_instrument_templates(
        db,
        instrument,
        choose_default_templates_form.ess_template.data,
        choose_default_templates_form.ppm_template.data,
        choose_default_templates_form.entry_template.data,
    )

    current_app.logger.debug(
        " samples : htmx : Set new templates ESS: "
        + f"{choose_default_templates_form.ess_template.data},"
        + f" PPM {choose_default_templates_form.ppm_template.data}"
        + f" and JEntry {choose_default_templates_form.entry_template.data}"
    )

    return render_template(
        "instruments/inner_template_form.html",
        choose_default_templates_form=choose_default_templates_form,
        msg="<p class='alert alert-info my-2' _='init wait 5s then remove me'> Updated default template </p>",
    )


@bp.route("/<int:iid>/instrument", methods=("GET", "POST"))
@login_required
def instrument_view(iid: int) -> Response | str:
    """
    Show single instrument entry.

    Parameters
    ----------
    iid : int
        ID of Instrument to show.

    Returns
    -------
    str|Flask.Response
        Flask view of the instrument.

    """
    instrument = get_instrument(iid)

    files = make_file_list(uploaddir_path(["i", instrument.id, instrument.identifier]))

    share_form = InstrumentShare()
    share_form.user.choices = {
        str(group.name): [
            (u.id, u.UserName) for u in group.members if u.id not in instrument.guest_users_list
        ]
        for group in db.session.execute(db.select(Groups).order_by(Groups.name)).scalars()
        if (group.id != g.user.group_id)
    }

    if share_form.validate_on_submit():
        if share_form.submit.data:
            with dbt.Transaction(db):
                user = db.session.get(User, share_form.user.data)
                instrument.guest_users.append(user)
            flash(f"Shared instrument with {user.name}", "alert-success")
        return redirect(url_for("instruments.instrument_view", iid=iid))

    # Default templates
    choose_default_templates_form = create_default_template_form(g.user.id, instrument)

    # if choose_default_templates_form.validate_on_submit():
    #
    #     dbt.update_default_instrument_templates(
    #         db,
    #         instrument,
    #         choose_default_templates_form.ess_template.data,
    #         choose_default_templates_form.ppm_template.data,
    #         choose_default_templates_form.entry_template.data,
    #     )
    #     flash(
    #         "Default templates successfully updated",
    #         "alert-success",
    #     )
    #     return redirect(url_for("instruments.instrument_view", iid=iid))

    return render_template(
        "instruments/instrument.html",
        instrument=instrument,
        share_form=share_form,
        choose_default_templates_form=choose_default_templates_form,
        files=files,
        msg=None,
    )


@bp.route("/<list:ids>/unshare")
@login_required
def unshare(ids) -> Response:
    """
    Unshare an instrument with a user (only possible for group member).

    Parameters
    ----------
    ids : list[int]
        Instrument ID and User ID to unshare.

    Returns
    -------
    Flask.Response
        Flask redirect.

    """
    iid, uid = ids
    instrument = db.get_or_404(
        Instrument,
        iid,
        description=f"instrument.get_instrument: Authorization failure. {g.user} "
        + f"tried to load instrument {iid}",
    )

    if g.user.group_id == instrument.group_id:
        user = db.session.get(User, uid)
        with dbt.Transaction(db):
            instrument.guest_users.remove(user)
        flash(f"Instrument unshared with {user.name}", "alert-info")
    else:
        flash("Only instrument group members can remove shares", "alert-warning")
    return redirect(url_for("instruments.instrument_view", iid=iid))


def get_instrument(iid: int, authenticate: bool = True) -> Instrument:
    """
    Get instrument by ID and check permission to act on it.

    Parameters
    ----------
    iid : Integer
        Instrument ID.
    authenticate : bool, optional
        Whether to check for permission or not. The default is True.

    Returns
    -------
    instrument : ResearchNotes.Instrument
        Entry for Instrument in database.

    """
    instrument = db.get_or_404(
        Instrument,
        iid,
        description=f"instrument.check_active: {g.user} tried to load instrument {iid}"
        + " which does not exists",
    )

    if authenticate:
        if instrument.active:
            if not (
                int(iid) in [int(i.id) for i in g.user.group_member.owned_instruments]
                or int(iid) in [int(i.id) for i in g.user.shared_instruments]
            ):
                abort(
                    403,
                    description=f"instrument.get_instrument: Authorization failure. {g.user} "
                    + f"tried to load instrument {iid}",
                )
        elif g.user.role_member.name == "Supervisor":

            if not (iid in [i.id for i in g.user.group_member.owned_instruments]):
                abort(
                    403,
                    description=f"instrument.get_instrument: Authorization failure. {g.user} "
                    + f"tried to load instrument {iid}",
                )

    return instrument


# --------------------------------------------------------------------------------------------------------
# That function can be solved by a backref user.group_member.owned_instruments
# def get_group_instruments(gid: int, include_deactivated: bool = False) -> typing.List[Instrument]:
#     """
#     Get all instruments owned by a group, given its ID
#
#     Parameters
#     ----------
#     gid : int
#         Group ID
#     include_deactivated :bool
#         Include the deactivated instruments in the list.
#     Returns
#     -------
#     List[Instrument]
#         All instruments owned by the current user's group
#
#     """
#     return [
#         i for i in db.session.get(Groups, gid).owned_instruments if (include_deactivated or i.active)
#     ]


# --------------------------------------------------------------------------------------------------------
# This function is called once. It either should be merged into call or be moved to utils.py, if we
# want to recycle somewhere else
# def in_same_group_as(user: User):
#     """
#
#     Parameters
#     ----------
#     user
#
#     Returns
#     -------
#
#     """
#     return g.user.group_id == user.group_id


# --------------------------------------------------------------------------------------------------------
