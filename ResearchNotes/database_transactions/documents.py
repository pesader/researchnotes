"""
All database transactions for the Documents go here.
"""

from flask_sqlalchemy import SQLAlchemy

# from flask import g  # This should not be needed here and be always a parameter for the function

from ResearchNotes.database import (
    Documents,
)

from .basics import Transaction

# =============================================================================
# documents.py - related to documents management
# =============================================================================


def create_document(database: SQLAlchemy, info_doc: dict, parent_id: int) -> None:
    """
    Create a new document.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    info_doc : dict.
            Dict to contain the document information.

    parent_id:  int
            Document id of the parent document

    Returns
    -------
    None.

    """
    new_doc = Documents(
        label=info_doc["label"],
        title=info_doc["title"],
        body=info_doc["body"],
        group_id=info_doc["group_id"],
        creator_id=info_doc["creator_id"],
        updatetor_id=info_doc["updatetor_id"],
    )

    with Transaction(database) as db_session:
        db_session.add(new_doc)
        Documents.query.get(parent_id).add_child(new_doc)


def update_document(database: SQLAlchemy, document: Documents, doc_data: dict) -> None:
    """
    Update an existing document.

    Parameters
    ----------
    database: FlaskSQLAlchemy class
        Database handler that the Flask APP is using.

    document : Document
        Entry of Document to be updated.

    doc_data: dict
        Information of document to be updated.

    Returns
    -------
    None.

    """
    with Transaction(database):
        document.label = doc_data["label"]
        document.title = doc_data["title"]
        document.body = doc_data["body"]
        document.updatetor_id = doc_data["updator"]
