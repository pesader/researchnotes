The next organization level is the  Procedures/Processing/Measurement (PPM) entry, you add to the ESS. This can include getting a spectrum, processing the sample to make nanostructures or do a run of 
your experiment or simulation with a set of parameters. It is assumed that you carry out your PPM with this sample.

Therefore, you have to create a new PPM entry in the ESS view. This will open a form to ask for information regarding the PPM. Again, the PPM will be created on form submission.

### Add PPM

Open a form to create a new entry. The form will ask for:

**Title:** Measurement title or name.

**Measurement Type:** Drop menu providing the type for the PPM. The supervisor has to create a list of PPM types in his menu.

**Made by** Person or team carrying out the PPM. The PPM will remember the user, who created the entry but this is not necessarily the person(s) actually doing the PPM and providing the data.

**Long Description**

Here actually the information over the PPM goes. This is the closest thing you have to a classical lab book. Note parameters, conditions and any other information relevant. 
Again, use Markdown to make tables, lists or anything else that helps make the information readable or understandable.

You can use defined templates to create the PPM, e.g. if it is something you do on a regular basis and you have a certain set of parameters values that need to be noted. You 
can create templates from existing PPMs or in the template menu.

After submitting, you will see the PPM view/page. Beside giving the information of the form in a formatted way, we have some additional options here:

### Return to ESS

Gets back to the ESS entry

### Edit

Reopens the creation form already filled with the existing information. You can update the information. Practical, if you want to add information during caring out your PPM.

### Template

Creates a PPM template from this measurement. Can be edited in the template menu.

### New report

Creates a report entry.

### Associated Reports

Report belonging to the PPM. You can open them by clicking the report title or editing them in the action menu. The action menu allows for 
deletion of the report (**no undelete is available and information is gone after deletion**).

### Associated Files 

A file menu working like the file menu of the ESS entry. You can download all files, single files, preview them (if a preview is available) or delete the file. 
New files are added by dragging them into the Dropzone. Again, you can add images to the long description using the Markdown command `![]()`. As long as you know the path, you 
can add any of your files to any PPM, ESS, report or document.
