# Searching
We have a full text search engine, which will index most of the relevant fields of the ESS, PPM, report and documents entries.

These fields are searched by a fuzzy search that will try to rank the most relevant results for the four areas indexed. 

We have a binding against ElasticSearch and Meilisearch. Depended from the configuration one or the other search engine is used.

Just type what you want to find into the search bar and press enter.