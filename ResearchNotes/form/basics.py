from flask import request

from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    SubmitField,
    SelectField,
    BooleanField,
    SearchField,
    validators,
)

# from dataclasses import dataclass

from ResearchNotes.database import User, db

# ============================================================================
# Searchform
# ============================================================================


class SearchForm(FlaskForm):
    """Form for the search bar in the main menu."""

    q = SearchField("Search", validators=[validators.DataRequired()])

    def __init__(self, *args, **kwargs):
        if "formdata" not in kwargs:
            kwargs["formdata"] = request.args
        if "meta" not in kwargs:
            kwargs["meta"] = {"csrf": False}
        super().__init__(*args, **kwargs)


# ============================================================================
# User registration and setup related forms
# ============================================================================
class UserForm(FlaskForm):
    """Generic form for users profiles."""

    UserName = StringField(
        "First and last name",
        [
            validators.Regexp("[A-Za-z0-9]", message="Only A-Z, a-z, 0-9 allowed"),
            validators.DataRequired(),
            validators.Length(min=4, max=25),
        ],
    )  #: :meta hide-value:
    email = StringField("Email Address", [validators.Length(min=6, max=35)])
    name = StringField(
        "Username (login)",
        [
            validators.Regexp("[A-Za-z0-9]", message="Only A-Z, a-z, 0-9 allowed"),
            validators.DataRequired(),
            validators.Length(min=2, max=35),
        ],
    )  #: :meta hide-value:

    group_id = SelectField("Group", coerce=int)  #: :meta hide-value:
    role_id = SelectField("Role", coerce=int)  #: :meta hide-value:
    # accept_tos = BooleanField('I accept the TOS', [validators.DataRequired()])
    submit = SubmitField("Submit")  #: :meta hide-value:

    def __init__(self, original_username, original_email, *args, **kwargs):
        """
        Init function.

        The new init has to remember the existing values of "user.name" and "user.email" to
        avoid changing to existing values

        Parameters
        ----------
        original_username : str
            The original username before change.
        original_email : str
            The initial e-mail given.
        *args : Any
            Arguments to passed to super().
        **kwargs : Any
            Keywords passed to super.

        Returns
        -------
        None.

        """
        super().__init__(*args, **kwargs)
        self.original_username = original_username
        self.original_email = original_email

    def validate_name(self, name):
        """
        Validate for doubling of user.name (login handel).

        Parameters
        ----------
        name : str
            Login handel of user.

        Raises
        ------
        validators
            Raised if the login handel already exists.

        Returns
        -------
        None.

        """
        if name.data != self.original_username:
            user = db.session.execute(db.select(User).filter_by(name=name.data)).scalar()
            if user is not None:
                raise validators.ValidationError("Please use a different username.")

    def validate_email(self, email):
        """
        Validate for doubling of user.email.

        Parameters
        ----------
        email : str
            E-Mail to be used by user.

        Raises
        ------
        validators
            Raised, if e-mail is already in database and used by other user.

        Returns
        -------
        None.

        """
        if email.data != self.original_email:
            user = db.session.execute(db.select(User).filter_by(email=email.data)).scalar()
            if user is not None:
                raise validators.ValidationError("Please use a different email address.")


class UserCreateForm(UserForm):
    "Create a user profile"
    password = PasswordField(
        "Password",
        [
            validators.DataRequired(),
            validators.Length(min=8, max=100),
        ],
    )  #: :meta hide-value:


class UserUpdateForm(UserForm):
    "Update a user profile"


class ResetPassword(FlaskForm):
    """Reset user password."""

    password = PasswordField(
        "New Password",
        [
            validators.DataRequired(),
            validators.EqualTo("confirm", message="Passwords must match"),
            validators.Length(min=8, max=100),
        ],
    )  #: :meta hide-value:
    confirm = PasswordField("Repeat Password", id="old")  #: :meta hide-value:

    submit = SubmitField("Reset Password")  #: :meta hide-value:


class ConfigChangePasswdForm(FlaskForm):
    """Change your password in the configuration menu."""

    old = PasswordField("Old password", [validators.DataRequired()])  #: :meta hide-value:
    # email = StringField('Email Address', [validators.Length(min=6, max=35)])
    password = PasswordField(
        "New password",
        [
            validators.DataRequired(),
            # validators.EqualTo("confirm", message="Passwords must match"),
            validators.Length(min=8, max=100),
        ],
    )  #: :meta hide-value:
    # confirm = PasswordField("Repeat Password")  #: :meta hide-value:

    submit = SubmitField("Change Password")  #: :meta hide-value:


# ============================================================================
# Login page form
# ============================================================================


class LoginForm(FlaskForm):
    """Login fom at the entrance page."""

    username = StringField(
        "Username", [validators.DataRequired(), validators.Length(min=3, max=25)]
    )  #: :meta hide-value:
    password = PasswordField("Password", [validators.DataRequired()])  #: :meta hide-value:

    remember = BooleanField("Stay signed in.")  #: :meta hide-value:
    submit = SubmitField("Sign In")  #: :meta hide-value:
