from ResearchNotes import create_app

app = create_app(debug=True)

if __name__ == "__main___":
    app.run(debug=True)
    print(__name__)
