# -*- coding: utf-8 -*-
"""
Here we define all form classes to keep the other files a bit more clean.

They are used by Flask - WTForm to render the input formulas.

We will follow the file structure of the database and keep related forms together. We will have an extra
file for templates as they have a specific create and update menu (and dedicated forms for this).
"""

from .basics import (
    SearchForm,
    UserCreateForm,
    UserUpdateForm,
    LoginForm,
    ConfigChangePasswdForm,
    ResetPassword,
)

from .documents import DocCreateForm

from .ess_ppm_report import (
    SamplesCreateForm,
    SamplesLocation,
    SamplesShare,
    MeasurementCreateForm,
    ReportCreateForm,
    ConfigCreateMType,
)

from .instrument_journal import (
    InstrumentCreate,
    InstrumentShare,
    InstrumentationJournalEntryCreate,
    ChooseDefaultTemplatesForm,
)

from .template import (
    TemplateInstrumentCreateForm,
    TemplateMeasurementCreateForm,
    TemplateSamplesCreateForm,
    TemplateInstrumentationJournalEntryCreateForm,
    UseTemplate,
)
