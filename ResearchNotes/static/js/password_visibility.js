function password_show_hide() {
  var x = document.getElementById("password");
  var y = document.getElementById("old");
  var show_eye = document.getElementById("show_eye");
  if (x.type === "password") {
    x.type = "text";
    y.type = "text"
    show_eye.innerHTML = "Hide";
  } else {
    x.type = "password";
    y.type = "password"
    show_eye.innerHTML = "Show";
  }
}
