title: About
published: 2022-04-25 

Open ResearchNotes 
=============

Open ResearchNotes is an electronic lab notebook (ELN) to keep track of your samples, experiments or simulations,
as well as  procedures/processing/measurements and results. Unlike your handwritten lab book, it is organized 
by samples - which are understood of the entity you are investigated. All procedures, measurements and 
results are connected to the sample keeping things in a logical order and not distributed over
various places.

Sample entries are shareable, so other group members can view them or add measurements of their own to the 
same sample.

A documentation part helps to create and track common procedure or  processes, which can be stored in a wiki like 
format. This can contain manuals for setups, general procedures for sample preparation etc. Basically, it stores
information more general and exchangeable between group members.

- See the [changelog](/pages/changelog) for latest features.
- See [help](/pages/help) for mor details.

*(c) by [Open ResearchNotes Developers](/pages/authors) - 2020, 2021, 2022, 2023*

The software is published under GPLv3 license as formulated in 29 June 2007.

Open ResearchNotes uses:

- Python 
- flask
- flask SQLAlchemy
- flask migrate
- flask FlatPages (including the Markdown python module)
- flask WTForms
- Bootstrap, dropzone.js, simpleMDE.js, htmx, _hyperscript and DataTable.js (front end)

and the packages, they depend on. For os/file activities, we use the standard library's os, 
shutil. By default, **sqlite** is used as database but code is tested also against **mariadb**.
Part of the text is formatable by using the Markdown syntax with tables and WikiLinks enabled.

We have to thank the Flask and various Flask extension authors for making there code available. 
For the backend, we are deeply in depth with the Flask tutorial and the nice tutorial from 
[Miguel Grinberg](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world). 
We are following a lot these examples and techniques with some code snipes from Stackoverflow (thanks to 
these people as well for making it available).

For the front end, we are using several Open Source projects: [bootstrap](https://getbootstrap.com/),
[DropeZone](https://www.dropzone.dev/js/), [simpleMDE](https://github.com/sparksuite/simplemde-markdown-editor) and
[DataTables](https://datatables.net/). WE implement some frontend functionality with [_hyperscript](https://hyperscript.org) 
and [htmx](https://htmx.org). Not to have to write javascript helps a lot. Thank you for making this code available.

## Getting involved

If you like to help, please look at the Contributing file of the package.