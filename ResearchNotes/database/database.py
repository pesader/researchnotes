# -*- coding: utf-8 -*-
"""
Database module to define all database schemes.

The database uses SQLAlchemy as "backend". In this way, we are not restricted
to a specific database engine or have to write SQL code. We use flask-sqlsalchemy to make our life
easier - this raises problems in linting and restricts a bit how we have to do stuff.

All database schemes are defined as a Python dataclass. Hence, they will have
a __init__ method generated for them. Besides, dataclass have other "magic"
methods defined, which are not of much interest. As some fields are
set to default value on creation or update, they can be excluded from
the __init__ method. Linting the code becomes easier than pylint will complain
about missing defined arguments.

A Mixin Class for our Search engine is defined first. Then the database models are defined as needed.

We define database schemes for

- Groups
- Roles
- User
- Samples
- Locations
- Measurements
- MeasurementTypes
- Reports

- Sharedsamples (Many-to-many relationship)

- TemplateSamples
- TemplatesMeasurements
- TemplateReports
- TemplateInstrument
- TemplateInstrumentationJournalEntry

- DocGenerations (many-to-many relationship for our Wiki)

- Documents
"""
#
# from dataclasses import dataclass
# from datetime import datetime
#
# from typing import List, Set, Any  # Needs to be here for compatibility with python 3.8
#
# # from ResearchNotes.search import add_to_index, remove_from_index, query_index, delete_index
#
# from ResearchNotes.database import db
# from ResearchNotes.database.mixins import SearchableMixin

# engine = create_engine(current_app.config['SQLALCHEMY_DATABASE_URI'], convert_unicode=True)
# db_session = scoped_session(sessionmaker(autocommit=False,
#                                          autoflush=False,
#                                          bind=engine))
# Base = declarative_base()
# Base.query = db_session.query_property()
