# Open ResearchNote code remarks

The Open ResearchNote code is written mainly in Python using some Javascript, HTML and CSS for the
frontend.

We use Flask as well as several flask extensions:

- Python 3.10 or better
- flask
- flask SQLAlachemy
- flask migrate
- flask FlatPages (including the markdown python module)
- flask WTForms
- Meilisearch or elasticsearch python library (optional)
- Bootstrap, dropzone.js, simpleMDE and DataTable.js (frontend)

**Open ResearchNotes** is published under [GPLv3 license](https://gitlab.unicamp.br/cdeneke/researchnote/-/blob/main/LICENSE).

To have the search function working, you have to have a meilisearch or elasticsearch instance running and configured. Run
the cli command update_search_index to create an index of existing database entries.

For running the package, you will have to install it and run with a WSGI and web server preferable on a dedicated server 
(see [Installation](install) ).

The code is currently hosted on the GitLab of the University of Campinas:

https://gitlab.unicamp.br/cdeneke/researchnote

We normally try to follow and use the latest version of the listed packages above. If new versions break the existing programm, we 
might need sometime to adapt.

The documentation is build with Sphinx, myst-parser and the read-tee-docs theme.

See CONTRIBUTING, how to participate, e.g. suggesting features, reporting bugs or submitting code. Contributions as well as 
constructive critic and suggestions are welcome. Feel free to open an issue in the issue tracker of the gitlab. Please do not
use the issue tracker for support questions (like install or usage).

Below is the documentation of the Python module.

- Bootstrap is released under [MIT license](https://github.com/twbs/bootstrap/blob/v4.5.3/LICENSE)
- Dropezone.js is released under [MIT license](https://github.com/dropzone/dropzone/blob/main/LICENSE)
- SimpleMDE is released under [MIT license](https://github.com/sparksuite/simplemde-markdown-editor/blob/master/LICENSE)
- DataTables is released under [MIT license](https://datatables.net/license/mit)

- Flask is under [BSD-3-Clause Source License](https://flask.palletsprojects.com/en/2.1.x/license/)
- Flask SQLAlchemy is under its own [license](https://flask-sqlalchemy.palletsprojects.com/en/2.x/license/)
- Flask migrate is released under [MIT license](https://github.com/miguelgrinberg/Flask-Migrate/blob/main/LICENSE)
- Flask WTF is under [BSD-3-Clause Source License](https://flask-wtf.readthedocs.io/en/1.0.x/license/)
- Flask Flatpages is release under own [license](https://github.com/Flask-FlatPages/Flask-FlatPages/blob/master/LICENSE)

See the myproject.toml file for projects requirements and needed packages for building the documentation, run the test etc.
Packages are build with __flit__.