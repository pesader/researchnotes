import pytest
import os
import shutil
from pathlib import Path
from ResearchNotes import create_app, db
import ResearchNotes.database_transactions as dbt

from ResearchNotes.database.basics import (
    Groups,
    Role,
    User,
    Locations,
)

from ResearchNotes.database.ess_ppm_report import (
    MeasurementType,
)

resources = Path(__file__).parent / "resources"


@pytest.fixture(scope="class")
def app():
    app = create_app(testing=True)
    app.config["UPLOAD_PATH"] = resources

    with app.app_context():
        db.drop_all()
        db.create_all()

        dbt.create_roles(db)
        dbt.create_admin(db)

    return app


@pytest.fixture(scope="class")
def clean_app():
    app = create_app(testing=True)
    return app


@pytest.fixture(scope="class")
def runner(app):
    return app.test_cli_runner()


def test_dump_database(runner):
    if os.path.exists(os.path.join(resources, "database_dump")):
        shutil.rmtree(os.path.join(resources, "database_dump"), ignore_errors=True)

    result = runner.invoke(args="dump_database_json")

    assert "Dumping the database to json." in result.output
    assert os.path.exists(os.path.join(resources, "database_dump"))
    assert os.path.exists(os.path.join(resources, "database_dump", "user.json"))
    assert os.path.exists(os.path.join(resources, "database_dump", "samples.json"))

    if os.path.exists(os.path.join(resources, "database_dump")):
        shutil.rmtree(os.path.join(resources, "database_dump"), ignore_errors=True)


def test_init_db(clean_app):
    with clean_app.app_context():
        result = clean_app.test_cli_runner().invoke(args="init_db")

        assert "Creating databases" in result.output

        admin = User.query.get(1)
        assert admin is not None
        assert admin.name == "admin"
        assert admin.UserName == "Joe Admin"

        mtype = MeasurementType.query.get(1)
        assert mtype.name == "Other"

        group = Groups.query.get(1)
        assert group.name == "None"

        location = Locations.query.get(1)
        assert location.name == "Not defined"

        role = Role.query.get(1)
        assert role.name == "admin"

        role = Role.query.get(2)
        assert role.name == "Supervisor"

        role = Role.query.get(3)
        assert role.name == "Student"

        role = Role.query.get(4)
        assert role.name == "ExStudent"

        role = Role.query.get(5)
        assert role.name == "StudentAdmin"
