title: Authors
published: 2022-04-25 

## Open ResearchNotes Developers

Currently, the following people contributed to the code:

- Christoph Deneke, IFGW, Unicamp(Maintainer)
- Pedro Sader Azevedo, FECC, Unicamp