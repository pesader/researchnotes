# -*- coding: utf-8 -*-
"""
Module to take command line instructions. We later register them as click commands.

This module should not contain any views or calls to web functions. For some reason,
functions are not found by sphinx autodoc - most likely has to do with the cli.command
decorator.
"""

# import csv
import datetime
import os
import typing
import re
import click
import dataclasses

from flask import current_app, Blueprint

# from flask.cli import with_appcontext
# from flask.json import dump, load

from ResearchNotes.database import (
    db,
    User,
    Groups,
    Samples,
    MeasurementType,
    Measurements,
    Role,
    Locations,
    Reports,
    Documents,
    TemplateSamples,
    TemplateMeasurements,
    Instrument,
    InstrumentationJournalEntry,
    TemplateInstrument,
    TemplateInstrumentationJournalEntry,
    EntryType,
)

# from ResearchNotes.search import add_to_index, query_index

import ResearchNotes.database_transactions as dbt


# bp = Blueprint("main", __name__, url_prefix="")

# =============================================================================
# Init database commands
# =============================================================================

bp = Blueprint("cli_commands", __name__)


@bp.cli.command("init_db")
def init_db():
    """
    Create the databases needed.

    Init the database by deleting exiting tables, creating all tables and
    filling in the minimum amount of fields needed (like users, roles, Measurement type none etc.)

    Returns
    -------
    None.

    """
    click.echo("Creating databases ...")

    db.drop_all()
    db.create_all()

    dbt.create_roles(db)
    dbt.create_admin(db)
    # dbt.create_index(db)

    click.echo("Initialized the database.")


# ===============================================================================
# Search related stuff
# ===============================================================================


@bp.cli.command("update_search_index")
def update_search_index():
    """Reindex all relevant entries to the search engine"""

    Samples.reindex()
    Measurements.reindex()
    Reports.reindex()
    Documents.reindex()


# =============================================================================
# Function to database maintenance
# =============================================================================


@bp.cli.command("database_maintenance")
def database_maintenance():
    """
    Clean and correct entries in the Document database.

    We check, if doc is child of itself. We should do an orphan check (all documents need to be linked to
    other document except the 'index' document).

    Returns
    -------
    None.

    """
    click.echo("Checking Document database ...")
    docs = db.session.execute(db.select(Documents)).scalars()

    click.echo("    Checking for self-reference ...")
    for doc in docs:
        if doc.is_child(doc):
            click.echo(f"{doc} is its own child! Fixing")
            with dbt.Transaction(db):
                doc.remove_child(doc)
    #
    click.echo("     Checking for orphans ...")
    for doc in docs:
        parent_dict = [(p.id, p.label) for p in doc.parent_docs]
        if len(parent_dict) == 0:
            click.echo(f"{doc} is orphan")
            if doc.label.endswith("_index"):
                click.echo("        But it is an index page, therefor ok.")
            else:
                pass
        if doc.id == 1 and doc.label == "index" and doc.group_id == 1:
            click.echo(f"           {doc} is the old index page - it will be removed.")
            with dbt.Transaction(db) as db_session:
                db_session.delete(doc)

    click.echo("Checking if all roles exist ...")

    role_name = ["admin", "Supervisor", "Student", "ExStudent", "StudentAdmin"]

    for role in role_name:
        found = db.session.execute(db.select(Role).filter_by(name=role)).scalar()
        if found is not None:
            click.echo(f"   Found {found.name} with ID: {found.id}")
        else:
            click.echo(f"   Did not find role: {role}. Creating it.")
            with dbt.Transaction(db) as db_session:
                db_session.add(Role(name=role, description="Missing role by database_maintenance"))

        if role == "admin":
            if found.members.scalar() is None:
                click.echo("    Did not find an administrator. Create one.")
                dbt.create_admin(db)
            else:
                click.echo("    Found at minimum one active administrator.")

    click.echo(
        "Checking if needed entries for location, measurement type, journal entry type as ell as EES and PPM template exist ..."
    )

    required_entry = db.session.get(Locations, 1)
    if (required_entry.name != "Not defined") or (required_entry is None):
        click.echo(f"   Required Location entry does not exist. Recreating")

    required_entry = db.session.get(MeasurementType, 1)
    if (required_entry.name != "None") or (required_entry is None):
        click.echo(f"   Required MeasurementType entry does not exist. Recreating")


# =============================================================================
# Function to dump database
# =============================================================================
#
#
# @bp.cli.command("dump_database")
# # @with_appcontext
# def dump_database():
#     """
#     Dump databases into a csv file for saving and/or reimporting.
#
#
#     We only dump the important stuff - so, some information will get lost
#
#     Returns
#     -------
#     None.
#
#     """
#     current_app.logger.info(": Dumping databases in to csv")
#     click.echo("Dumping the database to csv.")
#
#     dump_path = os.path.join(current_app.config["UPLOAD_PATH"], "database_dump")
#
#     if not os.path.exists(dump_path):
#         os.makedirs(dump_path)
#     # database = Groups.query.all()
#     # file = os.path.join(dump_path, "group.csv")
#     with open(os.path.join(dump_path, "group.csv"), "w", newline="", encoding="utf-8") as csvfile:
#
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(["id", "name"])
#         for group in Groups.query.all():
#             writer.writerow([group.id, group.name])
#     #
#     # database = Role.query.all()
#     # file = os.path.join(dump_path, "role.csv")
#     with open(os.path.join(dump_path, "role.csv"), "w", newline="", encoding="utf-8") as csvfile:
#
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(["id", "name", "description"])
#         for role in Role.query.all():
#             writer.writerow([role.id, role.name, role.description])
#     #
#     # database = Locations.query.all()
#     # file = os.path.join(dump_path, "locations.csv")
#     with open(os.path.join(dump_path, "locations.csv"), "w", newline="", encoding="utf-8") as csvfile:
#
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(["id", "name"])
#         for loc in Locations.query.all():
#             writer.writerow([loc.id, loc.name, loc.group_id])
#     #
#     # database = MeasurementType.query.all()
#     # file = os.path.join(dump_path, "measurementtype.csv")
#     with open(
#         os.path.join(dump_path, "measurementtype.csv"),
#         "w",
#         newline="",
#         encoding="utf-8",
#     ) as csvfile:
#
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(["id", "name"])
#         for mtype in MeasurementType.query.all():
#             writer.writerow([mtype.id, mtype.name, mtype.group_id])
#     #
#     # database = User.query.all()
#     # file = os.path.join(dump_path, "user.csv")
#     with open(os.path.join(dump_path, "user.csv"), "w", newline="", encoding="utf-8") as csvfile:
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(
#             [
#                 "id",
#                 "name",
#                 "UserName",
#                 "email",
#                 "password",
#                 "active",
#                 "confirmed_at",
#                 "last_login_at",
#                 "current_login_at",
#                 "last_login_ip",
#                 "current_login_ip",
#                 "login_count",
#                 "group_id",
#                 "role_id",
#                 "Shared_sample_list",
#             ]
#         )
#         for user in User.query.all():
#             writer.writerow(
#                 [
#                     user.id,
#                     user.name,
#                     user.UserName,
#                     user.email,
#                     user.password,
#                     user.active,
#                     user.confirmed_at,
#                     user.last_login_at,
#                     user.current_login_at,
#                     user.last_login_ip,
#                     user.current_login_ip,
#                     user.login_count,
#                     user.group_id,
#                     user.role_id,
#                     user.sharedsamplelist,
#                 ]
#             )
#     #
#     # database = Samples.query.all()
#     # file = os.path.join(dump_path, "samples.csv")
#     with open(os.path.join(dump_path, "samples.csv"), "w", newline="", encoding="utf-8") as csvfile:
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(
#             [
#                 "id",
#                 "identifier",
#                 "origin",
#                 "short_dis",
#                 "long_dis",
#                 "creator",
#                 "created",
#                 "updated",
#                 "creator_id",
#                 "group_id",
#                 "location_id",
#             ]
#         )
#         for sample in Samples.query.all():
#             writer.writerow(
#                 [
#                     sample.id,
#                     sample.identifier,
#                     sample.origin,
#                     sample.short_dis,
#                     sample.long_dis,
#                     sample.creator,
#                     sample.created,
#                     sample.updated,
#                     sample.creator_id,
#                     sample.group_id,
#                     sample.location_id,
#                 ]
#             )
#     #
#     # database = Measurements.query.all()
#     # file = os.path.join(dump_path, "measurements.csv")
#     with open(os.path.join(dump_path, "measurements.csv"), "w", newline="", encoding="utf-8") as csvfile:
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(
#             [
#                 "id",
#                 "short_dis ",
#                 "long_dis",
#                 "creator",
#                 "created",
#                 "updated",
#                 "mtype_id",
#                 "sample_id",
#                 "creator_id",
#             ]
#         )
#         for meas in Measurements.query.all():
#             writer.writerow(
#                 [
#                     meas.id,
#                     meas.short_dis,
#                     meas.long_dis,
#                     meas.creator,
#                     meas.created,
#                     meas.updated,
#                     meas.mtype_id,
#                     meas.sample_id,
#                     meas.creator_id,
#                 ]
#             )
#     #
#     # database = Reports.query.all()
#     # file = os.path.join(dump_path, "reports.csv")
#     with open(os.path.join(dump_path, "reports.csv"), "w", newline="", encoding="utf-8") as csvfile:
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(
#             [
#                 "id",
#                 "title ",
#                 "long_dis",
#                 "creator",
#                 "created",
#                 "updated",
#                 "sample_id",
#                 "creator_id",
#                 "measurement_id",
#             ]
#         )
#         for rep in Reports.query.all():
#             writer.writerow(
#                 [
#                     rep.id,
#                     rep.title,
#                     rep.long_dis,
#                     rep.creator,
#                     rep.created,
#                     rep.updated,
#                     rep.sample_id,
#                     rep.creator_id,
#                     rep.measurement_id,
#                 ]
#             )
#     #
#     # database = Documents.query.all()
#     # file = os.path.join(dump_path, "documents.csv")
#     with open(os.path.join(dump_path, "documents.csv"), "w", newline="", encoding="utf-8") as csvfile:
#         writer = csv.writer(csvfile, delimiter="\t", quotechar='"', doublequote=True)
#         writer.writerow(
#             [
#                 "id",
#                 "label",
#                 "title",
#                 "body",
#                 "created",
#                 "updated",
#                 "group_id",
#                 "creator_id",
#                 "updatetor_id",
#                 "children",
#             ]
#         )
#         for doc in Documents.query.all():
#             writer.writerow(
#                 [
#                     doc.id,
#                     doc.label,
#                     doc.title,
#                     doc.body,
#                     doc.created,
#                     doc.updated,
#                     doc.group_id,
#                     doc.creator_id,
#                     doc.updatetor_id,
#                     doc.childrenlist,
#                 ]
#             )


@bp.cli.command("dump_database_json")
# @with_appcontext
def dump_database_json():
    """
    Dump databases into a json file for saving and/or reimporting.

    We only dump the important stuff - so, some information will get lost

    Returns
    -------
    None.

    """
    current_app.logger.info(": Dumping databases in to json")
    click.echo("Dumping the database to json.")

    dump_path = os.path.join(current_app.config["UPLOAD_PATH"], "database_dump")
    if not os.path.exists(dump_path):
        os.makedirs(dump_path)

    file_list = [
        "group.json",
        "measurementtype.json",
        "role.json",
        "locations.json",
        "user.json",
        "samples.json",
        "measurements.json",
        "reports.json",
        "documents.json",
        "temp_samples.json",
        "temp_measurements.json",
        "instrument.json",
        "InstrumentationJournalEntry.json",
        "TemplateInstrument.json",
        "TemplateInstrumentationJournalEntry.json",
        "EntryType.json",
    ]
    dbmodel_list = [
        Groups,
        MeasurementType,
        Role,
        Locations,
        User,
        Samples,
        Measurements,
        Reports,
        Documents,
        TemplateSamples,
        TemplateMeasurements,
        Instrument,
        InstrumentationJournalEntry,
        TemplateInstrument,
        TemplateInstrumentationJournalEntry,
        EntryType,
    ]

    with click.progressbar(zip(file_list, dbmodel_list)) as p_bar:
        for f_name, m_name in p_bar:

            with open(os.path.join(dump_path, f_name), "w", encoding="utf-8") as file:

                to_dump = [
                    dataclasses.asdict(obj) for obj in db.session.execute(db.select(m_name)).scalars()
                ]
                current_app.json.dump(to_dump, file)

    shared_sample_list = [
        {str(sample.id): sample.sharedsamplelist}
        for sample in db.session.execute(db.select(Samples)).scalars()
        if sample.is_shared
    ]
    with open(os.path.join(dump_path, "shared_sample.json"), "w", encoding="utf-8") as file:
        click.echo("Dumping shared samples")
        current_app.json.dump(shared_sample_list, file)

    doc_child_list = [
        {str(doc.id): list(doc.childrenlist)}
        for doc in db.session.execute(db.select(Documents)).scalars()
        if len(doc.childrenlist) > 0
    ]
    with open(os.path.join(dump_path, "doc_children.json"), "w", encoding="utf-8") as file:
        click.echo("Dumping document relations")
        current_app.json.dump(doc_child_list, file)


def check_datetime(obj: str) -> typing.Union[str, datetime.datetime]:
    """
    Convert a string to a datetime object. If fail (ValueError) return the string.

    Parameters
    ----------
    obj : str
        String to be converted should follow the fingerprint: "%a, %d %b %Y %H:%M:%S %Z". This is the
        current format of a json dump

    Returns
    -------
    str|datetime
        If succeed, return the datetime o=ject otherwise the original string
    """
    try:
        r_obj = datetime.datetime.strptime(obj, "%a, %d %b %Y %H:%M:%S %Z")
    except ValueError:
        r_obj = obj
    return r_obj


@bp.cli.command("load_database_json")
def load_database_json():
    """
    Import database from json dump.

    Will look for a json dump of the database and reimport it. Will delete all existing entries in
    database.

    Returns
    -------
    None.

    """
    current_app.logger.info(": Loading databases from json")
    click.echo(
        "Loading the database from json. This will reinitialize the database and"
        + " all existing data will be overwritten!"
    )
    if click.confirm("Are you sure?"):
        dump_path = os.path.join(current_app.config["UPLOAD_PATH"], "database_dump")

        db.drop_all()
        db.create_all()

        file_list = [
            "group.json",
            "measurementtype.json",
            "role.json",
            "locations.json",
            "user.json",
            "samples.json",
            "measurements.json",
            "reports.json",
            "documents.json",
            "temp_samples.json",
            "temp_measurements.json",
            "instrument.json",
            "EntryType.json",
            "InstrumentationJournalEntry.json",
            "TemplateInstrument.json",
            "TemplateInstrumentationJournalEntry.json",
        ]

        dbmodel_list = [
            Groups,
            MeasurementType,
            Role,
            Locations,
            User,
            Samples,
            Measurements,
            Reports,
            Documents,
            TemplateSamples,
            TemplateMeasurements,
            Instrument,
            EntryType,
            InstrumentationJournalEntry,
            TemplateInstrument,
            TemplateInstrumentationJournalEntry,
        ]

        with click.progressbar(zip(file_list, dbmodel_list)) as p_bar:
            for j_file, db_model in p_bar:
                with open(os.path.join(dump_path, j_file), "r", encoding="utf-8") as file:
                    entries = current_app.json.load(file)
                for entry in entries:
                    for key, value in entry.items():
                        if isinstance(value, str) and re.search(" GMT", value):
                            entry[key] = check_datetime(value)

                    with dbt.Transaction(db) as db_session:
                        db_session.add(db_model(**entry))

        with open(os.path.join(dump_path, "shared_sample.json"), "r", encoding="utf-8") as file:
            shared_sample_list = current_app.json.load(file)
        for shared_sample in shared_sample_list:
            for key in shared_sample:
                sample = db.session.get(Samples, int(key))
                for uid in shared_sample[key]:
                    with dbt.Transaction(db):
                        user = db.session.get(User, uid)
                        sample.sharedsamples.append(user)

        with open(os.path.join(dump_path, "doc_children.json"), "r", encoding="utf-8") as file:
            doc_child_list = current_app.json.load(file)
        for doc_child in doc_child_list:
            for key in doc_child:
                doc_p = db.session.get(Documents, int(key))
                for did in doc_child[key]:
                    with dbt.Transaction(db):
                        doc_c = db.session.get(Documents, did)
                        doc_p.add_child(doc_c)
