---
orphan: true
---

### Installing on a Linux machine (XUbuntu with a local account)

Install python

      apt install python3 python3-venv python-dev^

Install nginix and supervisor 

       sudo apt install supervisor nginx

Create and python enviroment and install Researchnotes, pysqlite3 and Gunicorn
      
       python3 -m venv ResearchNotes
       source ResearchNotes/bin/activate
       pip install ResearchNotes-1.xx.xx-py3-none-any.whl 
       pip install gunicorn
       pip install pysqlite3

Setup environment

      export FLASK_APP=ResearchNotes 
      export RESEARCHNOTES_CONF=ResearchNotes/config.py

Setup a wsgi.py

```
from ResearchNotes import create_app

app = create_app()

if __name__ == "__main__":
    app.run(host='0.0.0.0') 
```  

Make a config.py

       UPLOAD_PATH = r'/home/christoph/ResearchNotes/data'
       SQLALCHEMY_DATABASE_URI = r"sqlite:////home/christoph/ResearchNotes/data/ResearchNotes.sqlite"
	   LOG_PATH = r"/home/christoph/ResearchNotes/"

No new database was created or prepared for migration. The existing database and files from Windows (and FreeBSD) install were copied to the /data directory


Start gunicorn to listen on a local port with --preload (sqlite3 seems to have problems without this)

    gunicorn -w 4 --threads 5 --preload -b localhost:8000 wsgi:app

nginix acitive side (goes to /etc/nginix/sites-enabled)

```
#server {
    # listen on port 80 (http)
#    listen 80;
#    server wintermute.ifi.unicamp.br;
#    location / {
        # redirect any requests to the same URL but on https
#        return 301 http://$host$request_uri;
#    }
#}
server {
    # listen on port 443 (https)
    listen 80;
    server_name wintermute.ifi.unicamp.br;

    # location of the self-signed SSL certificate
    #ssl_certificate /home/ubuntu/microblog/certs/cert.pem;
    #ssl_certificate_key /home/ubuntu/microblog/certs/key.pem;

    # write access and error logs to /var/log
    access_log /var/log/nginx/researchnotes_access.log;
    error_log /var/log/nginx/researchnotes_error.log;
    #Increase uplaod size to 100 MB as we have to get measurements uploaded.
    client_max_body_size 100M;

    # Use secure headers to avoid XSS and many other things
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Robots-Tag none;
    add_header X-Download-Options noopen;
    add_header X-Permitted-Cross-Domain-Policies none;
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header Referrer-Policy "no-referrer";
    add_header Content-Security-Policy "script-src 'self' 'unsafe-inline' 'unsafe-eval'; frame-src 'self'; object-src 'self'";

    location / {
        # forward application requests to the gunicorn server
        proxy_pass http://localhost:8000;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /static {
        # handle static files directly, without forwarding to the application
        alias /home/christoph/ResearchNotes/lib/python3.9/site-packages/ResearchNotes/static/;
        expires 30d;
    }
}
```




supoervisor conf (goes to /etc/supervisor/conf.d/

     [program:researchnotes]
     environment= RESEARCHNOTES_CONF=/home/christoph/ResearchNotes/config.py
     command=/home/christoph/ResearchNotes/bin/gunicorn -w 4 --threads 5 --preload -b localhost:8000 wsgi:app
     directory=/home/christoph/ResearchNotes
     user=christoph
     autostart=true
     autorestart=true
     stopasgroup=true
     killasgroup=true