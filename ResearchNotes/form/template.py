from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    SelectField,
    TextAreaField,
    validators,
)

# =============================================================================
# Forms related to Templates
# =============================================================================


class UseTemplate(FlaskForm):
    """Helper form to allow for the use of a Template."""

    template = SelectField("Template", coerce=int, validate_choice=False)  #: :meta hide-value:
    use = SubmitField("Use Template")  #: :meta hide-value:


class TemplateSamplesCreateForm(FlaskForm):
    """Create an E/S/S tem,plate or update one. Either from scratch of from existing E/S/S."""

    tname = StringField(
        "Template Name",
        [validators.DataRequired(), validators.Length(min=4, max=50)],
    )  #: :meta hide-value:
    identifier = StringField(
        "Identifier",
        [validators.DataRequired(), validators.Length(min=1, max=25)],
    )  #: :meta hide-value:
    origin = StringField(
        "Origin", [validators.DataRequired(), validators.Length(min=3, max=25)]
    )  #: :meta hide-value:
    short_dis = StringField(
        "Short Description",
        [validators.DataRequired(), validators.Length(min=1, max=50)],
    )  #: :meta hide-value:
    long_dis = TextAreaField("Long Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit E/S/S Template")  #: :meta hide-value:


class TemplateMeasurementCreateForm(FlaskForm):
    """Create a P/M/M tem,plate or update one. Either from scratch of from existing E/S/S."""

    tname = StringField(
        "Template Name",
        [validators.DataRequired(), validators.Length(min=4, max=50)],
    )  #: :meta hide-value:
    short_dis = StringField(
        "Title", [validators.DataRequired(), validators.Length(min=1, max=50)]
    )  #: :meta hide-value:
    mtype = SelectField("P/P/M Type", coerce=int)  #: :meta hide-value:
    instrument_id = SelectField("Instrument", coerce=int, validate_choice=False)  #: :meta hide-value:
    creator = StringField(
        "Made by", [validators.DataRequired(), validators.Length(min=1, max=25)]
    )  #: :meta hide-value:
    long_dis = TextAreaField("Long Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit P/P/M")  #: :meta hide-value:


class TemplateInstrumentCreateForm(FlaskForm):
    """
    Create or update an Instrument template
    Can be used to create templates from an existing instrument or from scratch
    """

    tname = StringField(
        "Template Name",
        [validators.DataRequired(), validators.Length(min=4, max=50)],
    )  #: :meta hide-value:
    identifier = StringField(
        "Identifier",
        [validators.DataRequired(), validators.Length(min=4, max=50)],
    )  #: :meta hide-value:
    # location_id = SelectField("Location", coerce=int)  #: :meta hide-value:
    description = TextAreaField("Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit instrument")  #: :meta hide-value:


class TemplateInstrumentationJournalEntryCreateForm(FlaskForm):
    """
    Create or update an Instrumentation Journal Entry template
    Can be used to create templates from an existing entry or from scratch
    """

    tname = StringField(
        "Template Name",
        [validators.DataRequired(), validators.Length(min=4, max=50)],
    )  #: :meta hide-value:
    identifier = StringField(
        "Identifier",
        [validators.DataRequired(), validators.Length(min=4, max=50)],
    )  #: :meta hide-value:
    etype = SelectField("Type", coerce=int)  #: :meta hide-value:
    description = TextAreaField("Description", [validators.DataRequired()])  #: :meta hide-value:
    submit = SubmitField("Submit instrumentation journal entry template")  #: :meta hide-value:
